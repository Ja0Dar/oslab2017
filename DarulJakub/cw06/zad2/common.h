//
// Created by owner on 16.04.17.
//

#ifndef ZAD1_COMMON_H
#include <unistd.h>
#include <stdio.h>
#include <mqueue.h>


#define MAX_CLIENT_NUMBER 32
#define SMALL_BUF 64
#define MEDIUM_BUF 256
#define BIG_BUFF 1024

#define SERVER_JOB_QUEUE_NAME "/mqueue_1"

#define MSG_REGISTER 1
#define MSG_ECHO 2
#define MSG_TO_UPPER 3
#define MSG_TIME 4
#define MSG_EXIT 5
#define MSG_PASS_CLIENT_ID 6


typedef struct {
    pid_t pid;
    char mqueue_name[MEDIUM_BUF];
}msg_registerbuf_t;

typedef struct {
    int client_id;
    char mtext[BIG_BUFF];
}msg_textbuf_t;

//Todo - remove because not used
typedef struct{
    int  client_id;
}msg_idbuf_t;

typedef struct {
    char mqueue_name [MEDIUM_BUF];
    pid_t pid;
    int mqueue_fd;
}client_t;



mqd_t create_default_queue(const char *queue_name, size_t maxmsg, size_t max_msg_size);
//key_t get_private_queue_key_from_home();
void send_idmsg(mqd_t mqdes, int mtype, int client_id);
void send_textmsg(const char *text, mqd_t mqdes, int mtype,int client_id);
void printAndQuit(const char *str, ...);



#define ZAD1_COMMON_H

#endif //ZAD1_COMMON_H
