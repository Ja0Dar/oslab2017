#include <stdio.h>
#include "common.h"
#include <stdlib.h>
#include <signal.h>
#include <memory.h>
#include <time.h>
#include <sys/time.h>
//Serwer po uruchomieniu tworzy nową kolejkę komunikatów systemu V. Za pomocą tej kolejki klienci będą wysyłać komunikaty do serwera. Wysyłane zlecenia mają zawierać rodzaj zlecenia jako rodzaj komunikatu oraz informację od którego klienta zostały wysłane (PID procesu klienta), w odpowiedzi rodzajem komunikatu ma być informacja identyfikująca czekającego na nią klienta.

mqd_t server_job_mqdes;
client_t clients[MAX_CLIENT_NUMBER];
int number_of_clients = 0;


void register_client(const msg_registerbuf_t *buf);

void respond_echo(const msg_textbuf_t *text_message);

void respond_to_upper(msg_textbuf_t *text_buf);

char to_upper(char small_letter);

void close_queues();

void respond_time(int client_id);

void exit_if_mqueue_is_empty(mqd_t mqdes);

int main() {
    signal(SIGINT, exit);


    server_job_mqdes = create_default_queue(SERVER_JOB_QUEUE_NAME, 10, BIG_BUFF);

    char buffer[BIG_BUFF + 1];
    ssize_t response_len;
    int mtype;

    atexit(close_queues);

    //buffers
    msg_registerbuf_t reg;
    msg_textbuf_t textbuf;
    int idbuf;

    int allow_exit = 0;
    int forever = 1;
    while (forever) {
        if (allow_exit)
            exit_if_mqueue_is_empty(server_job_mqdes);


        response_len = mq_receive(server_job_mqdes, buffer, BIG_BUFF, NULL);
        if (response_len < 0) {
            printAndQuit("Response_len<0\n");
        }
//        buffer[response_len] = '\0';//just in case TODO - check if necessary and remove
        mtype = buffer[0];
        switch (mtype) {
            case MSG_REGISTER:
                memcpy(&reg, buffer + 1, sizeof(reg));
                register_client(&reg);
                break;

            case MSG_ECHO:
                memcpy(&textbuf, buffer + 1, sizeof(textbuf));
                respond_echo(&textbuf);
                break;
            case MSG_TO_UPPER:
                memcpy(&textbuf, buffer + 1, sizeof(textbuf));
                respond_to_upper(&textbuf);
                break;
            case MSG_TIME:

                idbuf = buffer[1];
                respond_time(idbuf);
                break;
            case MSG_EXIT:
                printf("Received exit message\n");
                allow_exit = 1;
                break;

            default:
                printf("Not implemented\n");
        }
        fflush(stdout);


    }


}

void exit_if_mqueue_is_empty(mqd_t mqdes) {

    struct mq_attr attr;
    mq_getattr(mqdes, &attr);
    if (attr.mq_curmsgs == 0)
        exit(0);


}

void register_client(const msg_registerbuf_t *buf) {
    int new_client_id = number_of_clients++;
    printf("Saving to clients. Id: %d, number of clients : %d\n", new_client_id, number_of_clients);
    fflush(stdout);
    clients[new_client_id].pid = buf->pid;
    strcpy(clients[new_client_id].mqueue_name, buf->mqueue_name);
    clients[new_client_id].mqueue_fd = mq_open(buf->mqueue_name, O_WRONLY);
    if (clients[new_client_id].mqueue_fd < 0)
        printAndQuit("Cannot open client queue");
    send_idmsg(clients[new_client_id].mqueue_fd, MSG_PASS_CLIENT_ID, new_client_id);
    printf("Sent client_id. mqueue_fd : %d, queue_name: %s \n", clients[new_client_id].mqueue_fd,
           clients[new_client_id].mqueue_name);
    fflush(stdout);

}

void respond_echo(const msg_textbuf_t *text_message) {
    int cli_id = text_message->client_id;
    send_textmsg(text_message->mtext, clients[cli_id].mqueue_fd, MSG_ECHO, -1);


}

void respond_to_upper(msg_textbuf_t *text_buf) {
    char response[BIG_BUFF];
    int i = -1;
    do {
        i++;
        response[i] = to_upper(text_buf->mtext[i]);
    } while (i < BIG_BUFF - 1 && text_buf->mtext[i] != '\0');


    int client_id = text_buf->client_id;
    send_textmsg(response, clients[client_id].mqueue_fd, MSG_TO_UPPER, -1);


}

char to_upper(char small_letter) {
    if (small_letter >= 'a' && small_letter <= 'z')
        return small_letter - ('a' - 'A');
    return small_letter;
}

void respond_time(int client_id) {
//http://stackoverflow.com/questions/3673226/how-to-print-time-in-format-2009-08-10-181754-811
    time_t timer;
    char buffer[26];
    struct tm *tm_info;
    time(&timer);
    tm_info = localtime(&timer);
    strftime(buffer, 26, "%Y-%m-%d %H:%M:%S", tm_info);

    //
    send_textmsg(buffer, clients[client_id].mqueue_fd, MSG_TIME, -1);
}

void close_queues() {
//    sleep(2);
    mq_close(server_job_mqdes);
    mq_unlink(SERVER_JOB_QUEUE_NAME);

    for (int i = 0; i < number_of_clients; ++i) {
        mq_close(clients[i].mqueue_fd);

    }

    printf("All queues closed\n");

}


