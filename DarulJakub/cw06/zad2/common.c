// Created by owner on 16.04.17.
#include <memory.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <stdarg.h>
#include <sys/msg.h>
#include "common.h"

mqd_t create_default_queue(const char *queue_name, size_t maxmsg, size_t max_msg_size) {
    //atributes
    struct mq_attr attr;
    attr.mq_flags = 0;
    attr.mq_maxmsg = maxmsg;
    attr.mq_msgsize = max_msg_size;
    attr.mq_curmsgs = 0;

    mqd_t mqd = mq_open(queue_name, O_CREAT | O_RDONLY, 0644, &attr);
    if (mqd < 0)
        printAndQuit("Cannot create queue");
    return mqd;
}
void send_textmsg(const char *text, mqd_t mqdes, int mtype,int client_id){
    msg_textbuf_t response;
    response.client_id=client_id;
    strcpy(response.mtext,text);

    char char_response[BIG_BUFF];

    char_response[0]=mtype;
    memcpy(char_response+1,&response,sizeof(response));


    if (mq_send(mqdes,char_response,BIG_BUFF,0)<0)
        printAndQuit("Server cannot confirm registration, message not sent\n");


}

void send_idmsg(mqd_t mqdes, int mtype, int client_id){

    char char_response[BIG_BUFF];
    char_response[0]=(char )mtype;
//    memcpy(char_response+1,&client_id,sizeof(client_id));
    char_response[1]=client_id;//client_id;
//    char_response[2]='\0';

    if (mq_send(mqdes,char_response,BIG_BUFF,0)<0)
        printAndQuit("Server cannot confirm registration, message not sent\n");
    return;
}

key_t get_private_queue_key_from_home(){

    char *home_path;
    home_path = getenv("HOME");
    return ftok(home_path, getpid());
}

void printAndQuit(const char *str, ...) {
    va_list args;
    va_start (args, str);
    vfprintf(stderr, str, args);
    va_end (args);

    fprintf(stderr, "\n%s\n", strerror(errno));
    exit(1);
}