#include "common.h"
#include <stdlib.h>
#include <memory.h>
#include <signal.h>


void prompt(int server_job_mqdes, int client_id);

void close_queues_client();

void send_register_message(mqd_t server_job_mqdes, char *priv_mq_name);

int get_client_id_from_server_response();

#define  PRIV_MQ_NAME_BASE "/private_queue"
char priv_mq_name[MEDIUM_BUF];
mqd_t server_job_mqdes, response_queue_mqdes;

int main() {
    signal(SIGINT, exit);
    atexit(close_queues_client);
    //Generate unique queue name :
    sprintf(priv_mq_name, PRIV_MQ_NAME_BASE "_%d", getpid());


    server_job_mqdes = mq_open(SERVER_JOB_QUEUE_NAME, O_WRONLY);
    if (server_job_mqdes < 0) {
        printAndQuit("Cannot open server job queue\n");
    }

    response_queue_mqdes = create_default_queue(priv_mq_name, 10, BIG_BUFF);

    send_register_message(server_job_mqdes, priv_mq_name);

    int client_id = get_client_id_from_server_response();
    printf("Received id from server %d\n", client_id);

    char buffer[BIG_BUFF + 1];
    ssize_t response_len;
    msg_textbuf_t textbuf;
    int allow_exit = 0;
    while (!allow_exit) {
        prompt(server_job_mqdes, client_id);

        response_len = mq_receive(response_queue_mqdes, buffer, BIG_BUFF, 0);
        if (response_len < 0)
            printAndQuit("mq_receive error \n");

        memcpy(&textbuf, buffer + 1, BIG_BUFF);
        printf("Server response: %s\n", textbuf.mtext);

        sleep(1);
    }
}

int get_client_id_from_server_response() {
    char buffer[BIG_BUFF + 1];
    //recieve
    ssize_t response_len;
    response_len = mq_receive(response_queue_mqdes, buffer, BIG_BUFF, NULL);
    if (response_len < 0)
        printAndQuit("Cannot receive server response with id");
    if (buffer[0] != MSG_PASS_CLIENT_ID)
        printAndQuit("Wrong msg type. Expected MSG_PASS_CLIENT_ID\n");

    return buffer[1];
}

void send_register_message(mqd_t server_job_mqdes, char *priv_mq_name) {
    char buffer[BIG_BUFF + 1];
    msg_registerbuf_t reg;
    strcpy(reg.mqueue_name, priv_mq_name);
    reg.pid = getpid();
    memcpy(buffer + 1, &reg, sizeof(reg));
    buffer[0] = MSG_REGISTER;
    if (mq_send(server_job_mqdes, buffer, BIG_BUFF, 0) < 0) {
        printAndQuit("Cannot send registration request message\n");
    }
}


void prompt(int server_job_mqdes, int client_id) {
    printf("Choose message type [Int]\n"
                   "ECHO : %d\n"
                   "TO_UPPER : %d\n"
                   "TIME : %d\n"
                   "EXIT : %d\n", MSG_ECHO, MSG_TO_UPPER, MSG_TIME, MSG_EXIT);
    int mtype;
    if (scanf("%d", &mtype) != 1) {
        printf("Please specify correct message type\n");
        return;
    }
    if (mtype == MSG_ECHO || mtype == MSG_TO_UPPER) {

        char buf[BIG_BUFF] = "\n";
        int buf_size = BIG_BUFF;
        printf("type text to send to server:\n");
        fflush(stdout);
        while (buf[0] == '\n')
            fgets(buf, buf_size, stdin);
        buf[strlen(buf) - 1] = '\0';// cuts that \n

        if (buf_size > BIG_BUFF)
            printAndQuit("Too long message\n");

        send_textmsg(buf, server_job_mqdes, mtype, client_id);


    } else if (mtype == MSG_EXIT || mtype == MSG_TIME) {
        send_idmsg(server_job_mqdes, mtype, client_id);
        if (mtype == MSG_EXIT) {
            printf("Sent EXIT, so there is no reason to wait. Exiting.\n");
            exit(0);
        }
    } else {
        printf("Wrong type");
        prompt(server_job_mqdes,client_id);

    }
}

void close_queues_client() {
    mq_close(server_job_mqdes);
    mq_close(response_queue_mqdes);
    mq_unlink(priv_mq_name);
    printf("Client queues are closed and/or deleted.\n");
}
