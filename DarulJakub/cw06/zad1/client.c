#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <memory.h>
#include <signal.h>
#include "common.h"


int private_queue_id;
int client_id;

void prompt(int server_queue_id, int client_id);

void send_register_message_to_server(int private_queue_key, int server_job_queue_id);

void close_private_queue();

int get_client_id_from_server_response();

int main() {
    signal(SIGINT, exit);

    key_t private_queue_key = get_private_queue_key_from_home();

    private_queue_id = msgget(private_queue_key, IPC_CREAT | 0666);
    if (private_queue_id < 0)
        printAndQuit("Couldn't create Queue");

    atexit(close_private_queue);


    printf("Queue_id : %d, key: %d\n", private_queue_id, private_queue_key);

    int server_job_queue_id;
    if ((server_job_queue_id = msgget(SERVER_JOB_QUEUE_KEY, 0666)) < 0)
        printAndQuit("Client couldn't open client-discover queue");

    send_register_message_to_server(private_queue_key, server_job_queue_id);
    client_id = get_client_id_from_server_response();


    msg_textbuf_t text_message;
    int flags = IPC_NOWAIT;
    int forever = 1;
    while (forever) {

        if ((msgrcv(private_queue_id, &text_message, sizeof(text_message) - sizeof(long), MSG_ECHO, flags) >
             0) ||
            (msgrcv(private_queue_id, &text_message, sizeof(text_message) - sizeof(long), MSG_TO_UPPER, flags) >
             0) ||
            (msgrcv(private_queue_id, &text_message, sizeof(text_message) - sizeof(long), MSG_TIME, flags) >
             0)) {
            printf("Server response : '%s' \n", text_message.mtext);
            fflush(stdout);

        } else {
            prompt(server_job_queue_id, client_id);
        }

        sleep(1);
    }

}

int get_client_id_from_server_response() {
    msg_idbuf_t idbuf;
    int flags = IPC_NOWAIT;

    while (msgrcv(private_queue_id, &idbuf, sizeof(idbuf) - sizeof(long), MSG_PASS_CLIENT_ID,
                  flags) < 0) {
        sleep(1);
    }

    printf("Client registered. Client_id :%d\n", idbuf.client_id);
    fflush(stdout);
    return idbuf.client_id;
}

void prompt(int server_queue_id, int client_id) {
    printf("Choose message type [Int]\n"
                   "ECHO : %d\n"
                   "TO_UPPER : %d\n"
                   "TIME : %d\n"
                   "EXIT : %d\n", MSG_ECHO, MSG_TO_UPPER, MSG_TIME, MSG_EXIT);
    int mtype;
    if (scanf("%d", &mtype) != 1) {
        printf("Please specify correct message type\n");
        return;
    }
    if (mtype == MSG_ECHO || mtype == MSG_TO_UPPER) {

        char buf[BIG_BUFF]="\n";
        int buf_size=BIG_BUFF;
        printf("type text to send to server:\n");
        fflush(stdout);
        while (buf[0]=='\n')
            fgets(buf,buf_size,stdin);
        buf[strlen(buf)-1]='\0';// cuts that \n

        if (buf_size>BIG_BUFF)
            printAndQuit("Too long message\n");

        send_textmsg(buf, server_queue_id, mtype, client_id);


    } else if (mtype == MSG_EXIT || mtype == MSG_TIME) {
        send_idmsg(server_queue_id, mtype, client_id);
        if(mtype==MSG_EXIT){
            printf("Sent EXIT, so there is no reason to wait. Exiting.\n");
            exit(0);
        }
    } else {
        printf("Wrong type");
    }
}

void close_private_queue() {

    if (msgctl(private_queue_id, IPC_RMID, 0) < 0) {
        printAndQuit("Cannot remove queue\n");
    }
    printf("Queue removed\n");

}

void send_register_message_to_server(int private_queue_key, int server_job_queue_id) {
    msg_registerbuf_t buf;
    buf.key = private_queue_key;
    buf.pid = getpid();
    buf.mtype = MSG_REGISTER;

    int send = msgsnd(server_job_queue_id, &buf, sizeof(buf) - sizeof(long), IPC_NOWAIT);
    if (send < 0)
        printAndQuit("Client cannot send initial message to server\n");

}
