//
// Created by owner on 16.04.17.
//

#ifndef ZAD1_COMMON_H
#include <unistd.h>
#include <stdio.h>


#define MAX_CLIENT_NUMBER 32
#define SMALL_BUF 64
#define MEDIUM_BUF 256
#define BIG_BUFF 1024


#define SERVER_JOB_QUEUE_KEY 1234

#define MSG_REGISTER 1
#define MSG_ECHO 2
#define MSG_TO_UPPER 3
#define MSG_TIME 4
#define MSG_EXIT 5
#define MSG_PASS_CLIENT_ID 6


typedef struct {
    long mtype;
    pid_t pid;
    key_t key;
}msg_registerbuf_t;

typedef struct {
    long mtype;
    int client_id;
    char mtext[BIG_BUFF];
}msg_textbuf_t;

typedef struct{
    long mtype;
    int  client_id;
}msg_idbuf_t;

typedef struct {
//    int client_id;
    key_t queue_key;
    pid_t pid;
    int queue_id;
}client_t;



key_t get_private_queue_key_from_home();
void send_idmsg(int queue_id, long mtype, int client_id);
void send_textmsg(const char *text, int queue_id, long mtype,int client_id);
void printAndQuit(const char *str, ...);



#define ZAD1_COMMON_H

#endif //ZAD1_COMMON_H
