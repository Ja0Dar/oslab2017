#include <sys/msg.h>

#include <stdio.h>
#include "common.h"
#include <stdlib.h>
#include <time.h>
#include <signal.h>

//Serwer po uruchomieniu tworzy nową kolejkę komunikatów systemu V. Za pomocą tej kolejki klienci będą wysyłać komunikaty do serwera. Wysyłane zlecenia mają zawierać rodzaj zlecenia jako rodzaj komunikatu oraz informację od którego klienta zostały wysłane (PID procesu klienta), w odpowiedzi rodzajem komunikatu ma być informacja identyfikująca czekającego na nią klienta.

client_t clients[MAX_CLIENT_NUMBER];
int number_of_clients = 0;


void register_client(const msg_registerbuf_t *buf);

void respond_echo(const msg_textbuf_t *text_message);

void respond_to_upper(msg_textbuf_t *text_buf);

char to_upper(char small_letter);

void close_queue();

void respond_time(msg_idbuf_t *id_message);

int jobs_queue_id;

int main() {
    signal(SIGINT, exit);

    if ((jobs_queue_id = msgget(SERVER_JOB_QUEUE_KEY, IPC_CREAT | 0666)) < 0)
        printAndQuit("Couldn't create Queue");

    atexit(close_queue);

    msg_registerbuf_t register_message;
    msg_textbuf_t text_message;
    msg_idbuf_t idbuf;


    int flags = IPC_NOWAIT;

    int exit_when_empty = 0;

    while (1) {
        if (msgrcv(jobs_queue_id, &register_message, sizeof(register_message) - sizeof(long), MSG_REGISTER,
                   flags) > 0) {
            printf("Received register request\n");
            fflush(stdout);
            register_client(&register_message);

        } else if (msgrcv(jobs_queue_id, &text_message, sizeof(text_message) - sizeof(long), MSG_ECHO, flags) > 0) {
            respond_echo(&text_message);
        } else if (msgrcv(jobs_queue_id, &text_message, sizeof(text_message) - sizeof(long), MSG_TO_UPPER, flags) > 0) {
            respond_to_upper(&text_message);
        } else if (msgrcv(jobs_queue_id, &idbuf, sizeof(idbuf) - sizeof(long), MSG_TIME, flags) > 0) {
            respond_time(&idbuf);
        } else if (msgrcv(jobs_queue_id, &idbuf, sizeof(idbuf) - sizeof(long), MSG_EXIT, flags) > 0) {
            printf("Exit job received\n");
            exit_when_empty = 1;
        } else {
            if (exit_when_empty)
                exit(0);
        }

        sleep(1);


    }


}

void respond_time(msg_idbuf_t *id_message) {
//http://stackoverflow.com/questions/3673226/how-to-print-time-in-format-2009-08-10-181754-811
    time_t timer;
    char buffer[26];
    struct tm *tm_info;
    time(&timer);
    tm_info = localtime(&timer);
    strftime(buffer, 26, "%Y-%m-%d %H:%M:%S", tm_info);

    //
    send_textmsg(buffer, clients[id_message->client_id].queue_id, MSG_TIME, -1);
}

void respond_to_upper(msg_textbuf_t *text_buf) {
    char response[BIG_BUFF];
    int i = -1;
    do {
        i++;
        response[i] = to_upper(text_buf->mtext[i]);
    } while (i < BIG_BUFF - 1 && text_buf->mtext[i] != '\0');


    int client_id = text_buf->client_id;
    send_textmsg(response, clients[client_id].queue_id, MSG_TO_UPPER, -1);


}

char to_upper(char small_letter) {
    if (small_letter >= 'a' && small_letter <= 'z')
        return small_letter - ('a' - 'A');
    return small_letter;
}


void respond_echo(const msg_textbuf_t *text_message) {
    int cli_id = text_message->client_id;
    send_textmsg(text_message->mtext, clients[cli_id].queue_id, MSG_ECHO, -1);


}


void register_client(const msg_registerbuf_t *buf) {
    int i = number_of_clients++;
    printf("Saving to clients.  id: %i . Number of   clients : %d\n", i, number_of_clients);
    fflush(stdout);
    clients[i].pid = buf->pid;
    clients[i].queue_key = buf->key;
    clients[i].queue_id = msgget(buf->key, 0666);
    if (clients[i].queue_id < 0)
        printAndQuit("Cannot open client queue");

    msg_idbuf_t idbuf;
    idbuf.mtype = MSG_PASS_CLIENT_ID;
    idbuf.client_id = i;

    printf("Sending msg to client. queue_id : %d, key: %d \n", clients[i].queue_id, clients[i].queue_key);
    fflush(stdout);
    if (msgsnd(clients[i].queue_id, &idbuf, sizeof(idbuf) - sizeof(long), IPC_NOWAIT) <
        0)
        printAndQuit("Server cannot confirm registration, message not sent\n");
    printf("Sent\n");
    fflush(stdout);

}

void close_queue() {

    if (msgctl(jobs_queue_id, IPC_RMID, 0) < 0) {
        printAndQuit("Cannot remove server job queue");
    }
    printf("\nQueue 'server_job_queue' removed\n");
    fflush(stdout);


}


