// Created by owner on 16.04.17.
//#include <libnet.h>
#include <memory.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <stdarg.h>
#include <sys/msg.h>
#include "common.h"

void send_textmsg(const char *text, int queue_id, long mtype,int client_id){
    msg_textbuf_t textbuf;
    textbuf.client_id=client_id;
    textbuf.mtype=mtype;
    strcpy(textbuf.mtext,text);
    if (msgsnd(queue_id,&textbuf,sizeof(textbuf)-sizeof(long),IPC_NOWAIT)<0)
        printAndQuit("Cannot send text message.\n");
}

void send_idmsg(int queue_id, long mtype, int client_id){

    msg_idbuf_t idbuf;
    idbuf.mtype=mtype;
    idbuf.client_id=client_id;
    if (msgsnd(queue_id,&idbuf,sizeof(idbuf)-sizeof(long),IPC_NOWAIT)<0)
        printAndQuit("Cannot send id message.\n");
}

key_t get_private_queue_key_from_home(){

    char *home_path;
    home_path = getenv("HOME");
    return ftok(home_path, getpid());
}

void printAndQuit(const char *str, ...) {
    va_list args;
    va_start (args, str);
    vfprintf(stderr, str, args);
    va_end (args);

    fprintf(stderr, "\n%s\n", strerror(errno));
    exit(1);
}