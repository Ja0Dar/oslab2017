#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>
//Program powinien również obsługiwać sygnał SIGINT. Po jego odebraniu program wypisuje komunikat "Odebrano sygnał SIGINT" i kończy działanie. W kodzie programu, do przechwycenia sygnałów użyj zarówno funkcji signal, jak i sigaction (np. SIGINT odbierz za pomocą signal, a SIGTSTP za pomocą sigaction).

int up=1;


void stpHandle(int x){
    up=-up;
    printf("\n");
}

void intHandle(int x){
    printf("\nOdebrano sygnał SIGINT\n");
    exit(1);
}

int main(){
    signal(SIGTSTP, stpHandle);

//	sigaction()
    struct sigaction act;
    act.sa_flags=0;
    act.sa_handler=intHandle;
    sigemptyset(&act.sa_mask);
    sigaddset(&act.sa_mask, SIGINT);

    sigaction(SIGINT,&act,NULL);

    int i='A'-1;
    int forever=1;
    while (forever){
        up>0 ? i++ : i-- ;
        if(i<'A')
            i='Z';
        else if (i>'Z') {
            i='A';
        }

        usleep(200000);
        fflush(stdout);// flush buffer

        printf("%c",(char)i);
    }

    return 0;
}
