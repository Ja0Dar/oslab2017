#include <signal.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

int notGo=1;

void notGo_to_G(int sig){
    notGo=0;
}
int main(){

//            Zachowanie dzieci powinno być następujące. Każde dziecko najpierw symuluje pracę (uśnie na 0-10                     sekund). Następnie wysyła sygnał SIGUSR1 prośby o przejście, a po uzyskaniu pozwolenia losuje jeden z               32 sygnałów czasu rzeczywistego  (SIGRTMIN,SIGRTMAX), wysyła go do rodzica i kończy działanie, zwracając wypisaną różnicę czasu             między wysłaniem prośby a wysłaniem sygnału jako kod                   zakończenia procesu.

    srand((unsigned int) getpid());   // should only be called once
    sleep(1+rand()%9);


    //TIME START
    clock_t request_time;
    time(&request_time);

    signal(SIGUSR1,notGo_to_G);
    union sigval val;
    sigqueue(getppid(), SIGUSR1, val);
//    pause();
//    _exit(1);
    while (notGo)
        sleep(1);


    int r = rand()%32;

    kill(getppid(),SIGRTMIN+r);

    clock_t response_time;
    time(&response_time);
//    _exit(EXIT_FAILURE);
    exit((int) (response_time - request_time));

}
