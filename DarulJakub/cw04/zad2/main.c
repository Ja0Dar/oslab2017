#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <stdlib.h>
#include <stdarg.h>
#include <sys/wait.h>
#include <errno.h>

#define _XOPEN_SOURCE 700

//Napisz program, który tworzy N potomków i oczekuje na ich prośby na przejście (sygnały SIGUSR1). Po uzyskaniu K próśb, proces powinien pozwolić kontynuować działanie wszystkim procesom, które poprosiły o przejście (wysłać im sygnał pozwolenia na rozpoczęcie pracy) i niezwłocznie akceptować każdą kolejną prośbę. Program powinien wypisać listę wszystkich otrzymanych sygnałów wraz numerem PID potomka oraz kodem zakończenia procesu (w przypadku otrzymania sygnału zakończenia pracy potomka).
//
//Program kończy działanie po zakończeniu pracy ostatniego potomka albo po otrzymaniu sygnału SIGINT (w tym wypadku należy zakończyć wszystkie działające procesy potomne).
//
//N i M są argumentami programu.
//

void printAndQuit(const char *str, ...);


void parse_args(int argc, char *const *argv);

int K, k;
int N, n;
int *pid_prison;
int *all_children_pids;

int witnessed_child_exits = 0;

void handle_child_request_to_go(int sig, siginfo_t *info, void *v) {
    int child_pid = info->si_pid;
    if (sig == SIGUSR1)
        printf("SIGUSR1 from %d ", child_pid);
    else
        printf("not SIGUSR1 from %d ", child_pid);
    if (k < K) {
        pid_prison[k++] = child_pid;
        //hold childPid captive
        printf("holding him captive\n");
        return;
    }
    if (pid_prison != NULL) {
        printf("Kth +1 request- releasing all and ");
        for (int i = 0; i < K; ++i)
            kill(pid_prison[i], SIGUSR1);
        //TODO Causes segfault.
//        free(pid_prison);
        pid_prison = NULL;  //Jailbreak
    }
    printf("letting him go \n");
    kill(child_pid, SIGUSR1);

}

void print_child_time_and_rt_signal(int sig, siginfo_t *info, void *v) {
    int childPid = info->si_pid;
    int status = 0;
    int finished_pid = waitpid(childPid, &status, 0);

    if (finished_pid == childPid)
        printf("Pid: %d, RT sig : %d  Time : %d\n", childPid, sig - SIGRTMIN, WEXITSTATUS(status));
    else if (finished_pid < 0) {
        fprintf(stderr, "Waitpid error\n");
    } else {
        printf("Waiting for %d, found %d\n", childPid, finished_pid);
    }
    if (++witnessed_child_exits == N)
        exit(EXIT_SUCCESS);

}

void send_sigint_to_all_children_and_exit(int sig) {
    for (int i = 0; i < N; ++i) {
        if (all_children_pids[i] < 0)
            break;
        printf("Sending SIGINT to %d\n", all_children_pids[i]);
        kill(all_children_pids[i], SIGINT);

    }

    exit(EXIT_SUCCESS);
}


int main(int argc, char **argv) {
//    Parse Arguments

    parse_args(argc, argv);
    k = 0;
    all_children_pids = (int *) malloc((size_t) N);
    for (int i = 0; i < N; ++i)all_children_pids[i] = -1;
    //All pids before Kth request(SIGUSR1) go to pid_prison
    pid_prison = (int *) malloc((size_t) K);


    //HANDE SIGUSR1
    struct sigaction usr1_action;
    usr1_action.sa_flags = SA_SIGINFO;
    sigemptyset(&usr1_action.sa_mask);
//    for(int i=SIGRTMIN;i<=SIGRTMAX;i++)sigaddset(&usr1_action.sa_mask,i); // masking return while recieving
    usr1_action.sa_sigaction = handle_child_request_to_go;
    sigaction(SIGUSR1, &usr1_action, NULL);



    // Loopfor SIGRT :
    for (int sig = SIGRTMIN; sig <= SIGRTMAX; sig++) {
        struct sigaction tmpAct;
        tmpAct.sa_flags = SA_SIGINFO;
        sigemptyset(&tmpAct.sa_mask);
        sigaddset(&tmpAct.sa_mask, SIGUSR1);
        tmpAct.sa_sigaction = print_child_time_and_rt_signal;
        sigaction(sig, &tmpAct, NULL);
    }

    signal(SIGINT, send_sigint_to_all_children_and_exit);

    int pid;
    for (n = 0; n < N; n++) {
        if ((pid = fork()) < 0) {
            printAndQuit("Fork error");
        } else if (pid == 0) {
//            Zachowanie dzieci powinno być następujące. Każde dziecko najpierw symuluje pracę (uśnie na 0-10                     sekund). Następnie wysyła sygnał SIGUSR1 prośby o przejście, a po uzyskaniu pozwolenia losuje jeden z               32 sygnałów czasu rzeczywistego  (SIGRTMIN,SIGRTMAX), wysyła go do rodzica i kończy działanie, zwracając wypisaną różnicę czasu             między wysłaniem prośby a wysłaniem sygnału jako kod                   zakończenia procesu.

            execl("./child.out", "child.out", NULL);

            printf("This shouldnt happen\n");
            _exit(0);
        } else {
//            parent stuff
            all_children_pids[n] = pid;
        }

    }
    while (1)
        sleep(3);
    //this thing causes logout :/
//    int status=0;
//    for (int i=0;i<N;i++){
//        if(waitpid(all_children_pids[i],&status,0)<0){
//            perror("Waitpid fail");
//            exit(EXIT_FAILURE);
//        } else if (WIFEXITED(status)){
//            printf("Pid %d worked %d sec\n",all_children_pids[i],WEXITSTATUS(status));
//
//        } else{
//            printf("Pid %d exited but not through exit(int) or _exit(int)\n",all_children_pids[i]);
//        }
//    }
    return 0;
}

void parse_args(int argc, char *const *argv) {
    if (argc != 3) {
        printAndQuit(
                "Wrong arg number  should be 2 : [N] - total number of child processes  and\n [K] - number of processes for which permission to go will be delayed.\n \n");
    }

    if (sscanf(argv[1], "%d", &N) != 1)
        printAndQuit("Couldn't read N\n");
    if (sscanf(argv[2], "%d", &K) != 1)
        printAndQuit("Couldn't read K");
    if (K >= N)
        printAndQuit("K should be smaller than N");
}


void printAndQuit(const char *str, ...) {
    va_list args;
    va_start (args, str);
    vfprintf(stderr, str, args);
    va_end (args);
    exit(1);
}
