//Napisz program który tworzy proces potomny i wysyła do niego L sygnałów SIGUSR1, a następnie sygnał zakończenia wysyłania SIGUSR2. Potomek po
// otrzymaniu sygnałów SIGUSR1 od rodzica zaczyna je odsyłać do procesu macierzystego, a po otrzymaniu SIGUSR2 kończy pracę.
//
//Proces macierzysty w zależności od argumentu Type (1,2,3) programu wysyła sygnały na trzy różne sposoby:
//
//        SIGUSR1, SIGUSR2 za pomocą funkcji kill
//SIGUSR1, SIGUSR2 za pomocą funkcji sigqueue
//        wybrane 2 sygnały czasu rzeczywistego za pomocą kill
//Program powinien wypisywać informacje o:
//
//        liczbie wysłanych sygnałów do potomka
//        liczbie odebranych sygnałów przez potomka
//liczbie odebranych sygnałów od potomka
//        Program kończy działanie po zakończeniu pracy potomka albo po otrzymaniu sygnału SIGINT (w tym wypadku należy zakończyć proces potomny).
//
//L i Type są argumentami programu.
//
//(*) Zamiast podstawowego wariantu program tworzy w pętli N procesów potomnych, a następnie wysyła sygnały do losowo wybranego potomka (do każdego z nich w inny sposób) Przeanalizuj wyniki z wykorzystaniem funkcji sleep pomiędzy losowaniem kolejnego potomka, wartość funkcji sleep jest ostatnim argumentem programu, domyślna wartość T=0 oznacza jego brak. Argumenty programu N, L, T=0.

#include <stdio.h>
#include <unistd.h>
#include <stdarg.h>
#include <stdlib.h>
#include <signal.h>

#define SMALL_BUF_SIZE 32
#define PIPE_READ 0
#define PIPE_WRITE 1

void printAndQuit(const char *str,...);
void parseArgs(int argc, char **pString, int *pInt, int *pInt1);
void childStuff(int pInt[2]);
void printStats();


int parent_child_pipe[2];
int returned;
int pid;
///CHILD
int recieved;
int type, L;
///Parent Callbacks
void incrementReturned(int sig){ returned++; }

void  kill_children_and_die(int sig){
    kill(pid,SIGINT);
    printStats();
    exit(EXIT_SUCCESS);
}


void printStats() {
    char buf[SMALL_BUF_SIZE];
    read(parent_child_pipe[PIPE_READ],buf,SMALL_BUF_SIZE);
    printf("Sent : %d Recieved: %s Returned : %d \n",L,buf,returned);


}
///MAIN



int main(int argc, char** argv){
    parseArgs(argc,argv,&L,&type);

    pipe(parent_child_pipe);

    if((pid=fork())<0)
        printAndQuit("Fork error");
    else if (pid==0)
        childStuff(NULL);

//        execl("./child.out","child.out",NULL);


    signal(SIGINT, kill_children_and_die);

    //child  needs some time to open
    usleep(200*1000);

    returned=0;

    union sigval val={0}; // placeholder
    switch(type){
        case 1:
            signal(SIGUSR1, incrementReturned);
            for(int i=0;i<L;i++) kill(pid,SIGUSR1);
            usleep(200*1000);
            kill(pid,SIGUSR2);
            break;
        case 2:
            signal(SIGUSR1, incrementReturned);
            for(int i=0;i<L;i++) sigqueue(pid,SIGUSR1,val);
            usleep(200*1000);
            sigqueue(pid,SIGUSR2,val);
            break;
        case 3:
            signal(SIGRTMIN, incrementReturned);
            for(int i=0;i<L;i++) kill(pid,SIGRTMIN);
            usleep(200*1000);
            kill(pid,SIGRTMAX);// For exit()

            break;
        default:
            printAndQuit("Switch error.");
    }
    printStats();
    return 0;
}


///CHILD
void  die(int sig){
    char buf[SMALL_BUF_SIZE];
    sprintf(buf,"%d",recieved);
    write(parent_child_pipe[PIPE_WRITE],buf,SMALL_BUF_SIZE);
    exit(EXIT_SUCCESS);
}

void return_sig(int sig){
    recieved++;
    kill(getppid(),sig);
}
void childStuff(int pInt[2]) {
    recieved=0;
    printf("Child has been born\n");
    signal(SIGUSR1,return_sig);

    signal(SIGRTMIN,return_sig);


    signal(SIGRTMAX,die);
    signal(SIGUSR2,die);
    while (1);
}


///DETAILS


void parseArgs(int argc, char **argv, int *L, int *type) {
    if (argc!=3) {
        printAndQuit("Wrong arg number  should be 2 : [Type: 1|2|3] [L] \n");
    }
    sscanf(argv[1],"%d",type);
    sscanf(argv[2],"%d",L);

    if(!(*type==1 || *type==2 || *type==3 )) {
        printAndQuit("Wrong type %d. Type = [1|2|3]",*type);
    }

    if (*L<=0) {
        printAndQuit("L should be greater than 0, not %d",*L);
    }


}

void printAndQuit(const char *str,...) {
    va_list args;
    va_start (args, str);
    vfprintf(stderr,str,args);
    va_end (args);
    exit(1);
}