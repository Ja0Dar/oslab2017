//Napisz program który tworzy proces potomny i wysyła do niego L sygnałów SIGUSR1, a następnie sygnał zakończenia wysyłania SIGUSR2. Potomek po
// otrzymaniu sygnałów SIGUSR1 od rodzica zaczyna je odsyłać do procesu macierzystego, a po otrzymaniu SIGUSR2 kończy pracę.
//
//Proces macierzysty w zależności od argumentu Type (1,2,3) programu wysyła sygnały na trzy różne sposoby:
//
//        SIGUSR1, SIGUSR2 za pomocą funkcji kill
//SIGUSR1, SIGUSR2 za pomocą funkcji sigqueue
//        wybrane 2 sygnały czasu rzeczywistego za pomocą kill
//Program powinien wypisywać informacje o:
//
//        liczbie wysłanych sygnałów do potomka
//        liczbie odebranych sygnałów przez potomka
//liczbie odebranych sygnałów od potomka
//        Program kończy działanie po zakończeniu pracy potomka albo po otrzymaniu sygnału SIGINT (w tym wypadku należy zakończyć proces potomny).
//
//L i Type są argumentami programu.
//
//(*) Zamiast podstawowego wariantu program tworzy w pętli N procesów potomnych, a następnie wysyła sygnały do losowo wybranego potomka (do każdego z nich w inny sposób) Przeanalizuj wyniki z wykorzystaniem funkcji sleep pomiędzy losowaniem kolejnego potomka, wartość funkcji sleep jest ostatnim argumentem programu, domyślna wartość T=0 oznacza jego brak. Argumenty programu N, L, T=0.

#include <stdio.h>
#include <unistd.h>
#include <stdarg.h>
#include <stdlib.h>
#include <signal.h>
#include <time.h>
#include <sys/wait.h>

#define SMALL_BUF_SIZE 32
#define PIPE_READ 0
#define PIPE_WRITE 1

struct child_info {
    int parent_child_pipe[2];
    int returned;
    int pid;
    int sent;
    int sendType;
};

enum {
    REAL_TIME = 3, KILL = 1, SIGQUEUE = 2,NOT_SENT=0
};

void incrementReturned(int sig, siginfo_t *info, void *v);
void kill_children_and_die(int sig);
void die(int sig);
void return_sig(int sig);

void printAndQuit(const char *str, ...);
void parseArgs(int argc, char **argv, int *N, int *L, int *T);
void childStuff(int pInt[2]);
void createChildren();
void printStats(struct child_info *info);
void sendSignals(struct child_info *info, int type, int l);

///Used by parent
struct child_info *child_infos;
int N;



///USED by child - after fork
int recieved;
int *childPipe;

int main(int argc, char **argv) {
    int L,T;
    parseArgs(argc, argv, &N, &L, &T);
    child_infos = calloc((size_t) N, sizeof(struct child_info));

    createChildren();

    signal(SIGINT, kill_children_and_die);

    //children  need some time to open
    sleep(3);


    struct sigaction act1;
    act1.sa_flags = SA_SIGINFO;
    sigemptyset(&act1.sa_mask);
    ///Add masks if needed
    act1.sa_sigaction = incrementReturned;
    sigaction(SIGUSR1, &act1, NULL);
    sigaction(SIGRTMIN, &act1, NULL);

    srand(time(NULL));
    int type, rand_ind;
    for (int j = 0; j < (N * 3) / 4; ++j) {
        if (T > 0)
            sleep((unsigned int) T);
        rand_ind = rand() % N;
        type = 1+(rand() % 3);//KILL, SIGQUEUE, REAL_TIME
        if (child_infos[rand_ind].sent <= 0)
            sendSignals(&child_infos[rand_ind], type, L);
    }

    sleep(3);
    kill_children_and_die(1);

}


///Parent Callbacks
void incrementReturned(int sig, siginfo_t *info, void *v) {
    int childPid = info->si_pid;
    int i = 0;

    while (i < N && childPid != child_infos[i].pid)i++;
    if (i == N)
        printAndQuit("Couldn't find corrent pid in incrementReturned");


    child_infos[i].returned += 1;
}

void kill_children_and_die(int sig) {

//    union sigval val = {0}; // placeholder
    for (int i = 0; i < N; ++i) {
        /*
         switch (child_infos[i].sendType){
            case KILL:
                kill(child_infos[i].pid, SIGUSR2);
                 waitpid(child_infos[i].pid,NULL,0);
                break;
            case SIGQUEUE:
                sigqueue(child_infos[i].pid,SIGUSR2,val);
                 waitpid(child_infos[i].pid,NULL,0);
                break;
            case REAL_TIME:
                kill(child_infos[i].pid,SIGRTMAX);
                 waitpid(child_infos[i].pid,NULL,0);
                wait(NULL);
                break;
            case NOT_SENT:
                kill(child_infos[i].pid, SIGUSR2);
                 waitpid(child_infos[i].pid,NULL,0);
            break;
            default:
                printf("Something went wrong in kill_children_and_die ");
        }*/
        kill(child_infos[i].pid,SIGUSR2);
        waitpid(child_infos[i].pid,NULL,0);

        if (child_infos[i].sent > 0){// && child_infos[i].sendType==NOT_SENT){
            printStats(&child_infos[i]);
        }

    }


    exit(EXIT_SUCCESS);
}


///CHILD callbacs
void die(int sig) {
//    printf("Recieved %d\n",recieved);
    char buf[SMALL_BUF_SIZE];
    sprintf(buf, "%d", recieved);
    write(childPipe[PIPE_WRITE], buf, SMALL_BUF_SIZE);
    exit(EXIT_SUCCESS);
}


void return_sig(int sig){
    recieved++;
    kill(getppid(), sig);
}






void sendSignals(struct child_info *info, int type, int l) {
    int pid = info->pid;
    info->sent = l;
    union sigval val = {0}; // placeholder
    info->sendType = type;
    switch (type) {
        case KILL:
            for (int i = 0; i < l; i++) kill(pid, SIGUSR1);
//            usleep(200*1000);
//            kill(pid, SIGUSR2);
            break;
        case SIGQUEUE:
            for (int i = 0; i < l; i++) sigqueue(pid, SIGUSR1, val);
//            usleep(200*1000);
//            sigqueue(pid, SIGUSR2, val);
            break;
        case REAL_TIME:
            for (int i = 0; i < l; i++) kill(pid, SIGRTMIN);
//            usleep(200*1000);
//            kill(pid, SIGRTMAX);// For exit()

            break;
        default:
            printAndQuit("Switch error.");

    }
}

void createChildren() {
    int pid;
    for (int i = 0; i < N; i++) {
        pipe(child_infos[i].parent_child_pipe);
        if ((pid = fork()) < 0)
            printAndQuit("Fork error");
        else if (pid == 0)
            childStuff(child_infos[i].parent_child_pipe);
        child_infos[i].sendType=NOT_SENT;
        child_infos[i].pid = pid;
        child_infos[i].returned = 0;

    }

}




void childStuff(int pipe[2]) {
    childPipe = pipe;
    recieved = 0;
    printf("Child has been born %d\n",getpid());


//   /*
    struct sigaction act_USR1,act_USR2,act_RTMIN,act_RTMAX;
    act_USR1.sa_flags = SA_SIGINFO;
    act_USR2.sa_flags = SA_SIGINFO;
    act_RTMIN.sa_flags = SA_SIGINFO;
    act_RTMAX.sa_flags = SA_SIGINFO;

    sigemptyset(&act_USR1.sa_mask);
    sigemptyset(&act_USR2.sa_mask);
    sigemptyset(&act_RTMIN.sa_mask);
    sigemptyset(&act_RTMAX.sa_mask);

//    sigaddset(&act_USR1.sa_mask,SIGUSR2);
//    sigaddset(&act_RTMIN.sa_mask,SIGRTMAX);
    sigaddset(&act_USR2.sa_mask,SIGUSR1);
    sigaddset(&act_RTMAX.sa_mask,SIGRTMIN);

    act_USR1.sa_handler=return_sig;
    act_RTMIN.sa_handler=return_sig;

    act_RTMAX.sa_handler=die;
    act_USR2.sa_handler=die;

    sigaction(SIGUSR1,&act_USR1,NULL);
    sigaction(SIGRTMIN,&act_RTMIN,NULL);

    sigaction(SIGUSR2,&act_USR2,NULL);
    sigaction(SIGRTMAX,&act_RTMAX,NULL);

/*
    signal(SIGUSR1,return_sig);

    signal(SIGRTMIN,return_sig);


    signal(SIGRTMAX,die);
    signal(SIGUSR2,die);
    ///Add masks if needed

*/
    while (1);
}

///DETAILS


void printAndQuit(const char *str, ...) {
    va_list args;
    va_start (args, str);
    vfprintf(stderr, str, args);
    va_end (args);
    exit(1);
}




void parseArgs(int argc, char **argv, int *N, int *L, int *T) {
    if (!(argc == 3 || argc == 4)) {
        printAndQuit("Wrong arg number  should be 2-3 : N  L   [T] \n");
    }
    sscanf(argv[1], "%d", N);
    sscanf(argv[2], "%d", L);
    *T = 0;
    if (argc == 4)
        sscanf(argv[3], "%d", T);

    if (*N < 0) {
        printAndQuit("Wrong N %d. Should be >=0", *N);
    }

    if (*T < 0) {
        printAndQuit("Wrong T %d. Should be >=0", *T);
    }

    if (*L <= 0) {
        printAndQuit("L should be greater than 0, not %d", *L);
    }
}

void printStats(struct child_info *info) {
    char buf[SMALL_BUF_SIZE];
    read(info->parent_child_pipe[PIPE_READ], buf, SMALL_BUF_SIZE);
    printf("%d  | Sent : %d Recieved: %s Returned : %d ",info->pid, info->sent, buf, info->returned);
//    if(atoi(buf)<info->returned)
//        printf("SOME KIND OF ERROR");
    switch (info->sendType) {
        case KILL:
            printf("KILL\n");
            break;
        case SIGQUEUE:
            printf("SIGQUEUE\n");
            break;
        case REAL_TIME:
            printf("REAL_TIME\n");
            break;
        default:
            printf("UNKNOWN sendType\n");
    }


}
