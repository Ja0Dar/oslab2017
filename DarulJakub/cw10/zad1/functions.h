#ifndef FUNCTIONS_H

#include <semaphore.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <pthread.h>
#define  BUF_SMALL 256
#define  BUF_MED 512
#define BUF_BIG 1024
#define MAX_CLIENTS 64
enum operation {
    ADD, SUBTRACT, MULTIPLY, DIVIDE
};

enum acceptation {ACCEPTED=137,REFUSED};
struct expression {
    double a;
    enum operation op;
    double b;
} typedef expression_t;

struct client {
    int isAvailable;
    int hasName;
    int socketfd;
    char name[BUF_MED];
} typedef client_t;

void printAndQuit(const char *str, ...);

enum operation prompt_for_op();

expression_t prompt_for_expr();

double prompt_for_double(const char *prompt);

#define FUNCTIONS_H
#endif
