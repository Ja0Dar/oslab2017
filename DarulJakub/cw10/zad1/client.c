
#include <sys/un.h>
#include <sys/socket.h>
#include <signal.h>
#include "functions.h"
#include "socket_functions.h"

enum connection_mode {
    UNIX = 0, TCP = 1
};

void prompt_client(char *fname);

double evaluate_expression(expression_t expr);

void parse_args_client(int argc, char **argv, char cli_name[BUF_MED], enum connection_mode *conn_mode,
                       char serv_name[BUF_MED], int *portno);

int main(int argc, char **argv) {
    char client_name[BUF_MED];
    enum connection_mode con_mod;
    char serv_name[BUF_MED];
    int portno;
    parse_args_client(argc, argv, client_name, &con_mod, serv_name, &portno);

    int sockfd, st;


    // setup connection
    if (con_mod == UNIX) {
        sockfd = socket(AF_UNIX, SOCK_STREAM, 0);
        if (sockfd < 0)
            printAndQuit("Unix socket error");

        struct sockaddr_un addr;
        strcpy(addr.sun_path, serv_name);
        addr.sun_family = AF_UNIX;

        st = connect(sockfd, (struct sockaddr *) &addr, sizeof(addr));
    } else {
        sockfd = socket(AF_INET, SOCK_STREAM, 0);
        if (sockfd < 0)
            printAndQuit("TCP socket error");
        struct sockaddr_in addr;
        struct hostent *serv = gethostbyname(serv_name);
        if (serv == NULL) {
            printAndQuit("Wrong server name");
        }
        setup_serv_addr(&addr, serv, portno);

        st = connect(sockfd, (struct sockaddr *) &addr, sizeof(addr));
    }


    signal(SIGINT, exit);
    signal(SIGTERM, exit);
    void clean() {
        shutdown(sockfd, SHUT_RDWR);
        close(sockfd);
        printf("Socket closed\n");
    }
    atexit(clean);


    if (st < 0) {
        close(sockfd);
        printAndQuit("Cannot connect to server");
    }

    if (send_str_to_socket(client_name, strlen(client_name), sockfd) < 0)
        printAndQuit("Cannot send str through socket\n");

    int isAccepted;
    save_int_from_socket(&isAccepted, sockfd);

    if (isAccepted==REFUSED){
        printAndQuit("Server has client with the same name. Connection refused\n");
    } else if (isAccepted==ACCEPTED){
        printf("Connection accepted\n");
        fflush(stdout);
    } else{
        printAndQuit("Wrong response");
    }

    int forver = 1;
    expression_t expr;
    double res;
    while (forver) {
        read(sockfd, &expr, sizeof(expr));

        res = evaluate_expression(expr);

        send_double_through_socket(res, sockfd);

    }


}

void parse_args_client(int argc, char **argv, char cli_name[BUF_MED], enum connection_mode *conn_mode,
                       char serv_name[BUF_MED], int *portno) {
    if (argc != 4 && argc != 5) {
        prompt_client(argv[0]);
        exit(1);
    }


    strcpy(cli_name, argv[1]);
    strcpy(serv_name, argv[3]);
    if (sscanf(argv[2], "%d", conn_mode) != 1 || !(*conn_mode == UNIX || *conn_mode == TCP)) {
        prompt_client(argv[0]);
        exit(1);
    }
    if (argc == 5) {
        *portno = -1;
        if (sscanf(argv[4], "%d", portno) != 1 || *portno <= 0) {
            prompt_client(argv[0]);
            exit(1);
        }
    }
}


void prompt_client(char *fname) {
    printf("usage: %s client_name conn_type serv_name [portno]\n"
                   "conn_type: 0 - UNIX -local sockets\n"
                   "conn_type: 1 - TCP\n"
                   "if conn_type = 1 you should provide portno\n", fname);
}

double evaluate_expression(expression_t expr) {
    switch (expr.op) {
        case ADD:
            return expr.a + expr.b;
        case SUBTRACT:
            return expr.a - expr.b;
        case MULTIPLY:
            return expr.a * expr.b;
        case DIVIDE:// for b=0  returns inf? :D
            return expr.a / expr.b;
        default:
            printAndQuit("Wrong operation in evaluate_expression\n");
    }
    return 0;
}
