//
// Created by jacko on 14.05.17.
//

#ifndef SOCKET_FUNCTIONS_H
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include "functions.h"
#define _XOPEN_SOURCE 700
#define UNIX_PATH_MAX 108
#define UNIX_SOCKET_NAME "unix_socket"
enum connection_type {CON_CLI=8,CON_HR};
int save_int_from_socket(int *msg, int socketfd);
int send_int_through_socket(int msg, int socketfd);
void setup_serv_addr(struct sockaddr_in *serv_addr, struct hostent *serv_hostent, int portno);
int send_str_to_socket(const char *msg, size_t len, int socketfd) ;
int save_str_from_socekt(char *msg, int socketfd) ;
int send_double_through_socket(double msg, int socketfd) ;
int save_double_from_socket(double *msg, int socketfd) ;

//dependent on functions.h
int save_expression_from_socket(expression_t *msg, int socketfd) ;
int send_expression_through_socket(expression_t msg, int socketfd) ;
#define SOCKET_FUNCTIONS_H

#endif //SOCKET_FUNCTIONS_H
