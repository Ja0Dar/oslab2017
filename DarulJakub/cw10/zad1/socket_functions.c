
#include <stdio.h>
#include "socket_functions.h"
#include "functions.h"

void setup_serv_addr(struct sockaddr_in *serv_addr, struct hostent *serv_hostent, int portno) {

    bzero((char *) serv_addr, sizeof(struct sockaddr_in));
    serv_addr->sin_family = AF_INET;
    bcopy((char *) serv_hostent->h_addr, (char *) &serv_addr->sin_addr.s_addr,
          serv_hostent->h_length);
    serv_addr->sin_port = htons(portno);

}


int send_int_through_socket(int msg, int socketfd) {
    if (write(socketfd, &msg, sizeof(msg)) < sizeof(msg)) {
        fprintf(stderr, "Cannot send con type to server\n");
        return -1;
    }
    return 0;
}


int save_int_from_socket(int *msg, int socketfd) {
    if (read(socketfd, msg, sizeof(*msg)) < sizeof(*msg)) {
        fprintf(stderr, "Cannot get con type from client\n");
        return -1;
    }
    return 0;
}

int send_double_through_socket(double msg, int socketfd) {
    if (write(socketfd, &msg, sizeof(msg)) < sizeof(msg)) {
        fprintf(stderr, "Cannot send con type to server\n");
        return -1;
    }
    return 0;
}

int save_double_from_socket(double *msg, int socketfd) {
    if (read(socketfd, msg, sizeof(*msg)) < sizeof(*msg)) {
        fprintf(stderr, "Cannot get con type from client\n");
        return -1;
    }
    return 0;
}
int send_expression_through_socket(expression_t msg, int socketfd) {
    if (write(socketfd, &msg, sizeof(msg)) < sizeof(msg)) {
        fprintf(stderr, "Cannot send con type to server\n");
        return -1;
    }
    return 0;
}


int save_expression_from_socket(expression_t *msg, int socketfd) {
    if (read(socketfd, msg, sizeof(*msg)) < sizeof(*msg)) {
        fprintf(stderr, "Cannot get con type from client\n");
        return -1;
    }
    return 0;
}
int send_str_to_socket(const char *msg, size_t len, int socketfd) {
//    fprintf(stderr,"sending %s\n",msg);
    if (write(socketfd, msg, len) < 0) {
        fprintf(stderr, "Cannot send con type to server\n");
        return -1;
    }

    return 0;
}
///
/// \param msg  - should be allocated,  of BUF_MED size
/// \param socketfd
/// \return
int save_str_from_socekt(char *msg, int socketfd) {
//    fprintf(stderr,"receiving %s\n",msg);
    int res = (int) (read(socketfd, msg, BUF_MED));
    if (res < 0) fprintf(stderr, "Cannot get con type from client\n");
    return res;
}
