cmake_minimum_required(VERSION 3.6)
project(cw10)

set(CMAKE_C_STANDARD 99)

set(SOURCE_FILES server.c functions.c functions.h socket_functions.c socket_functions.h)
add_executable(server.out ${SOURCE_FILES})
TARGET_LINK_LIBRARIES(server.out pthread rt)

set(SOURCE_FILES client.c functions.c functions.h socket_functions.c socket_functions.h)
add_executable(client.out ${SOURCE_FILES})
TARGET_LINK_LIBRARIES(client.out pthread rt)

#set(ZADB zadb.c functions.c functions.h)
#add_executable(zadb.out ${ZADB})
#TARGET_LINK_LIBRARIES(zadb.out pthread rt)