
#include <sys/socket.h>
#include <stdio.h>
#include <netinet/in.h>
#include <sys/un.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include "socket_functions.h"
#include "functions.h"
#include <sys/epoll.h>
#include <fcntl.h>
#include <stdbool.h>


client_t clients[MAX_CLIENTS];
sem_t sem_clients;
int cli_count = 0;

void close_client(int cli_id);

int get_client_index_from_name(const char * name) ;
void* operation_dispacher(void*arg);
int get_client_index_from_sockfd(int fd);

void parse_args(int argc, char **argv, char *unix_socket_fd, int *portno);

int setup_and_bind_unix_socket(const char *unix_socket_name);

int setup_and_bind_inet_socket(int portno);

int make_socket_non_blocking(int sfd);

void print_prompt(char *fname);

int main(int argc, char **argv) {
    sem_init(&sem_clients, 0, 1);// init (sem_ptr, 0 - for threads, 1 - initial value);


// Args
    char unix_socket_name[UNIX_PATH_MAX];
    int portno;
    parse_args(argc, argv, unix_socket_name, &portno);


    int socket_unix = setup_and_bind_unix_socket(unix_socket_name);
    if (socket_unix < 0){
        unlink(unix_socket_name);
        printAndQuit("Cannot setup unix socket\n");
    }
    listen(socket_unix, 20);

    //setup tcp socket
    int socket_in = setup_and_bind_inet_socket(portno);
    if (socket_in < 0)printAndQuit("Cannot setup inet socket\n");
    listen(socket_in, 20);

//todo addr af inet cleanup
    void cleanup_socket() {
        shutdown(socket_unix, SHUT_RDWR);
        close(socket_unix);
        unlink(unix_socket_name);
        shutdown(socket_in, SHUT_RDWR);
        close(socket_in);
        printf("Cleanup done\n ");
    }

    signal(SIGINT, exit);
    signal(SIGTERM, exit); //Clion

    atexit(cleanup_socket);


    int epoll_fd = epoll_create1(0);
    struct epoll_event tmp_event;
    tmp_event.events = EPOLLIN | EPOLLOUT | EPOLLRDHUP; // let -  tmp_event przy niepustym buferze
    tmp_event.data.fd = socket_unix;
    epoll_ctl(epoll_fd, EPOLL_CTL_ADD, socket_unix, &tmp_event);

    tmp_event.data.fd = socket_in;
    epoll_ctl(epoll_fd, EPOLL_CTL_ADD, socket_in, &tmp_event);


    int incoming_fd;
    struct epoll_event *incoming_events = calloc(60, sizeof(struct epoll_event));
    int number_of_incoming_events;



    pthread_t prompter;
    pthread_create(&prompter,NULL,operation_dispacher,NULL);



#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wmissing-noreturn"
    while (1) {
        //Get buch of events
        number_of_incoming_events = epoll_wait(epoll_fd, incoming_events, 60, -1);

        //process al of them
        for (int i = 0; i < number_of_incoming_events; ++i) {
            incoming_fd = incoming_events[i].data.fd;
            if (!incoming_events[i].events & EPOLLIN || incoming_events[i].events & EPOLLRDHUP ||
                incoming_events[i].events & EPOLLHUP) {

                printf("%d just have closed\n", incoming_fd);
                int cli_id = get_client_index_from_sockfd(incoming_fd);
                close_client(cli_id);
                continue;

            } else if (incoming_fd == socket_unix || incoming_fd == socket_in) {
                /* We have a notification on the listening socket, which
                means one or more incoming connections. */

                //  So we will add incoming socket to epoll
                printf("Registering client\n");
                fflush(stdout);

                while (1) {
                    struct sockaddr incoming_un_addr;
                    socklen_t socklen;
                    int new_fd;

                    socklen = sizeof incoming_un_addr;
                    new_fd = accept(incoming_fd, &incoming_un_addr, &socklen);

                    if (new_fd < 0) {
                        if (errno == EAGAIN || errno == EWOULDBLOCK) {
                            //all incoming parsed
                            break;
                        } else {
                            printAndQuit("Accept error\n");
                        }
                    }


                    // make new_fd non blocking and add it to epoll:
                    if (make_socket_non_blocking(new_fd < 0))
                        printAndQuit("Cannot make socket nonblocking");

                    tmp_event.data.fd = new_fd;
                    tmp_event.events = EPOLLIN | EPOLLET | EPOLLRDHUP;
                    if (epoll_ctl(epoll_fd, EPOLL_CTL_ADD, new_fd, &tmp_event) < 0)
                        printAndQuit("Cannot epollctl\n");

                    //add to clients
                    sem_wait(&sem_clients);
                    clients[cli_count].isAvailable = 1;
                    clients[cli_count].hasName = 0;
                    clients[cli_count].socketfd = new_fd;
                    cli_count++;
                    sem_post(&sem_clients);
                    printf("Added %d to epollfd \n", new_fd);
                    fflush(stdout);

                }
            } else {
                printf("receiving message from client\n");

                int cli_ind = get_client_index_from_sockfd(incoming_fd);
                if (cli_ind < 0)
                    printAndQuit("NO WAY! client not in database\n");

                //Tcp client has hang up
                if (recv(incoming_fd, NULL, 10, MSG_PEEK) == 0) {
                    printf("Client %d %s has closed\n", incoming_fd, clients[cli_ind].name);
                    close_client(cli_ind);
                    continue;
                }



                if (!clients[cli_ind].hasName) {
                    char name[BUF_MED];
                    save_str_from_socekt(name, incoming_fd);
                    int other_client_with_the_same_name=get_client_index_from_name(name);
                    if (other_client_with_the_same_name >= 0){
                        send_int_through_socket(REFUSED,incoming_fd);
                        // client shuts down communication on his end

                        continue;
                    }
                    send_int_through_socket(ACCEPTED,incoming_fd);
                    strcpy(clients[cli_ind].name,name);
                    clients[cli_ind].hasName = 1;
                    printf("Client  %d sent his name: %s\n", incoming_fd, clients[cli_ind].name);
                } else if(!clients[cli_ind].isAvailable) {
                    fprintf(stderr, "Undefined action - what has happened? client : %s fd: %d, available: %d\n",
                            clients[cli_ind].name, clients[cli_ind].socketfd, clients[i].isAvailable);
                }else{
                    double res;

                    save_double_from_socket(&res,incoming_fd);
                    printf("\nClient %s returns %f\n",clients[cli_ind].name,res);
                }


            }


        }

    }
#pragma clang diagnostic pop


    return 0;
}

void parse_args(int argc, char **argv, char *unix_socket_fd, int *portno) {
    if (argc != 3) {
        print_prompt(argv[0]);
        exit(1);
    }

    strcpy(unix_socket_fd, argv[1]);

    if (sscanf(argv[2], "%d", portno) != 1) {
        printf("Wrong portno \n");
        exit(1);
    }

    if (*portno <= 0) {
        printf("Portno should be positive int\n");
        exit(1);
    }


}


void print_prompt(char *fname) {
    printf("Usage: %s linux_socket_name port_nr \n", fname);
}



int make_socket_non_blocking(int sfd) {
//    source:
//    https://banu.com/blog/2/how-to-use-epoll-a-complete-example-in-c/
    int flags, s;

    flags = fcntl(sfd, F_GETFL, 0);
    if (flags == -1) {
        perror("fcntl");
        return -1;
    }

    flags |= O_NONBLOCK;
    s = fcntl(sfd, F_SETFL, flags);
    if (s == -1) {
        perror("fcntl");
        return -1;
    }

    return 0;
}

///
/// \param fd  - socket fd
/// \return  // index of client in global clients or -1 if there is no such fd in clients
int get_client_index_from_sockfd(int fd) {
    sem_wait(&sem_clients);
    for (int i = 0; i < cli_count; ++i) {
        if (clients[i].socketfd == fd){
            sem_post(&sem_clients);
            return i;
        }
    }
    sem_post(&sem_clients);
    return -1;
}

int get_client_index_from_name(const char * name) {
    sem_wait(&sem_clients);
    for (int i = 0; i < cli_count; ++i) {
        if (clients[i].hasName && strcmp(clients[i].name,name)==0){
            sem_post(&sem_clients);
            return i;
        }
    }
    sem_post(&sem_clients);
    return -1;
}
int setup_and_bind_unix_socket(const char *unix_socket_name) {

    int socket_unix;
    socket_unix = socket(AF_UNIX, SOCK_STREAM, 0);
    if (socket_unix < 0 || make_socket_non_blocking(socket_unix) < 0) {
        fprintf(stderr, "Cannot setup  unix socket \n");
        return -1;
    }

    struct sockaddr_un unix_addr;
    strcpy(unix_addr.sun_path, unix_socket_name);
    unix_addr.sun_family = AF_UNIX;

    if (bind(socket_unix, (struct sockaddr *) &unix_addr, sizeof(unix_addr)) < 0) {
        fprintf(stderr, "Cannot bind socket unix\n");
        return -1;
    }

    return socket_unix;
}

int setup_and_bind_inet_socket(int portno) {

    int socket_in = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_in < 0 || make_socket_non_blocking(socket_in) < 0) {
        fprintf(stderr, "Cannot setup inet socket\n");
        return -1;
    }

    struct sockaddr_in inet_addr;
    bzero((char *) &inet_addr, sizeof(inet_addr));

    inet_addr.sin_family = AF_INET;
    inet_addr.sin_addr.s_addr = INADDR_ANY;
    inet_addr.sin_port = htons(portno); // convert to network byte order

    //bind serv_addr to socket
    if (bind(socket_in, (struct sockaddr *) &inet_addr, sizeof(inet_addr)) < 0) {
        fprintf(stderr, "Cannot bind in socket\n");
        return -1;
    }
    return socket_in;

}

void close_client(int cli_id) {
    //necessity? or not
    sem_wait(&sem_clients);
    clients[cli_id].isAvailable = 0;
    sem_post(&sem_clients);
    close(clients[cli_id].socketfd);
}

void* operation_dispacher(void* arg){
    expression_t exr;
    int choosen_client_ind;
    int forever=1;
    while (forever){
        exr=prompt_for_expr();

        sem_wait(&sem_clients);
        choosen_client_ind= rand()%cli_count;
        int start_ind=choosen_client_ind;

        do{
            choosen_client_ind+=1;
            if (choosen_client_ind>=cli_count)
                choosen_client_ind=choosen_client_ind%cli_count;
            if (choosen_client_ind==start_ind){
                break;
            }
        }while (!clients[choosen_client_ind].isAvailable);


        if(!clients[choosen_client_ind].isAvailable)
            fprintf(stderr,"No client online\n");
        //
        send_expression_through_socket(exr,clients[choosen_client_ind].socketfd);
        printf("\nSent to %s\n",clients[choosen_client_ind].name);

        sem_post(&sem_clients);


    }


}

