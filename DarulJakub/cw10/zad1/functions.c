//
// Created by darul on 5/25/17.
//

#include <stdbool.h>
#include "functions.h"

void printAndQuit(const char *str, ...) {
    va_list args;
    va_start (args, str);
    vfprintf(stderr, str, args);
    va_end (args);

    fprintf(stderr, "\n%s\n", strerror(errno));
    exit(1);
}

expression_t prompt_for_expr() {
    expression_t res;
    res.a = prompt_for_double("\nType a :\n");
    res.op = prompt_for_op();
    res.b = prompt_for_double("\nType b:\n");
    return res;
}

double prompt_for_double(const char *prompt) {
    double res;
    char *buf = malloc(BUF_SMALL);
    size_t bufsize = BUF_SMALL;
/*
    do {
        printf("%s", prompt);
        getline(&buf, &bufsize, stdin);
    } while (sscanf(buf, "%lf\n", &res) != 1);*/
    printf("%s\n", prompt);
    fflush(stdout);
    while (scanf("%lf", &res) != 1);
    free(buf);

    return res;

}

enum operation prompt_for_op() {
    char op_char;
    char *buf = malloc(10);
    size_t bufsize = 10;
    enum operation res;
//    res=ADD;
    char prompt[] = "\nType operation. [ + | - | / | * ]\n";
    bool good_op = false;
    printf("\n%s\n", prompt);
    fflush(stdout);
    while (!good_op) {
        /*do {
            printf("%s\n", prompt);
            getline(&buf, &bufsize, stdin);
        } while (sscanf(buf, "%c\n", &op_char) != 1);
*/
        while (scanf("%c\n", &op_char) != 1);

        good_op = true;
        switch (op_char) {
            case '+':
                res = ADD;
                break;
            case '-':
                res = SUBTRACT;
                break;
            case '/':
                res = DIVIDE;
                break;
            case '*':
                res = MULTIPLY;
                break;
            default:
                good_op = false;
                if (op_char != 10)
                    printf("%c, with code %d is not valid  operator\n", op_char, (int) op_char);
        }
    }

    free(buf);
    return res;


}


