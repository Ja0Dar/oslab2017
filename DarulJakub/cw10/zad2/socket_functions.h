//
// Created by jacko on 14.05.17.
//

#ifndef SOCKET_FUNCTIONS_H

#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include "functions.h"

#define _XOPEN_SOURCE 700
#define UNIX_PATH_MAX 108
#define SERV_UNIX_SOCKET_NAME "unix_socket"


enum connection_mode {
    UNIX = 0, INET = 1
};

enum msg_type {
    //MSG with body
     MSG_EXPR=10, MSG_DOUBLE,MSG_NAME,   // 10  because i want easier debugging - no zeros
    //MSG without bodies
    MSG_PING, MSG_ACCEPT, MSG_REJECT

};
union{
    char unix_name[UNIX_PATH_MAX];
    struct {
        char serv_name[128];
        int portno; // not changed byteorder
    } inet_info;
}typedef sender_info_t;


struct message{
    enum msg_type type;
    enum connection_mode conn_mode;
    sender_info_t sender_info;
    char buf[BUF_SMALL];
}typedef message_t;



void setup_serv_addr(struct sockaddr_in *serv_addr, struct hostent *serv_hostent, int portno);

#define SOCKET_FUNCTIONS_H

#endif //SOCKET_FUNCTIONS_H
