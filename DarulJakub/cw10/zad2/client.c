
#include <sys/un.h>
#include <sys/socket.h>
#include <signal.h>
#include "functions.h"
#include "socket_functions.h"


void prompt_client(char *fname);

int setup_unix_sockets( char *client_name, char *serv_name);

int setup_inet_sockets(char *serv_name, int portno);

double evaluate_expression(expression_t expr);

void parse_args_client(int argc, char **argv, char cli_name[BUF_MED], enum connection_mode *conn_mode,
                       char serv_name[BUF_MED], int *portno);


void process_expr(const message_t *msg);

int sock_to_server, sock_from_server;
sender_info_t client_sender_info;

int main(int argc, char **argv) {
    char client_name[BUF_MED];
    enum connection_mode con_mod;
    char serv_name[BUF_MED];
    int portno;
    parse_args_client(argc, argv, client_name, &con_mod, serv_name, &portno);


    int st = 0;
    /* setup :
     * client_sender_info
     * sock_to_server
     * sock_from_server*/

    if (con_mod == UNIX)
        st = setup_unix_sockets( client_name, serv_name);
    else
        st = setup_inet_sockets(serv_name, portno);


    signal(SIGINT, exit);
    signal(SIGTERM, exit);
    void clean() {

        shutdown(sock_from_server, SHUT_RDWR);
        close(sock_from_server);
        if (con_mod == UNIX)
            unlink(client_sender_info.unix_name);

        shutdown(sock_to_server, SHUT_RDWR);
        close(sock_to_server);
        printf("Socket closed\n");
    }
    atexit(clean);
    //delayed exit allows me to use teardown
    if (st < 0)
        printAndQuit("Cannot setup serv connectons");




// send name to serv
    message_t msg;
    msg.type = MSG_NAME;
    msg.conn_mode = con_mod;
    msg.sender_info = client_sender_info; // setup while setting up sockets
    strcpy(msg.buf, client_name);
    send(sock_to_server, &msg, sizeof(msg), 0);

    //recieve response
    recv(sock_from_server, &msg, sizeof(msg), 0);

    if (msg.type == MSG_ACCEPT)
        printf("Accepted\n");
    else if (msg.type == MSG_REJECT)
        printAndQuit("Server rejected. sorry\n");
    else
        printAndQuit("Wrong message . not accept nor reject\n");

    fflush(stdout);
    int forver = 1;
    expression_t expr;
    message_t ping_resp;
    ping_resp.sender_info = client_sender_info;
    ping_resp.conn_mode = con_mod;
    ping_resp.type = MSG_PING;
    while (forver) {
        recv(sock_from_server, &msg, sizeof(msg), 0);

        switch (msg.type) {
            case MSG_EXPR:
                printf("Received expression to evaluate\n");
                process_expr(&msg);
                break;
            case MSG_PING:
                send(sock_to_server, &ping_resp, sizeof(ping_resp), 0);
                break;
            default:
                printAndQuit("Wrong msg type");
        }
    }
}

int setup_unix_sockets( char *client_name, char *serv_name) {

    sprintf(client_sender_info.unix_name, "%s_%ld", client_name, (long) getpid());


    sock_to_server = socket(AF_UNIX, SOCK_DGRAM, 0);
    sock_from_server = socket(AF_UNIX, SOCK_DGRAM, 0);

    if (sock_to_server < 0 || sock_from_server < 0) {
        printAndQuit("Unix socket error");
    }

    //sock_to_Serv
    struct sockaddr_un addr;
    strcpy(addr.sun_path, serv_name);
    addr.sun_family = AF_UNIX;
    if (connect(sock_to_server, (struct sockaddr *) &addr, sizeof(addr)) < 0)
        return -1;

    //sock_form _serv
    struct sockaddr_un recv_addr;
    recv_addr.sun_family = AF_UNIX;
    strcpy(recv_addr.sun_path,client_sender_info.unix_name);

    bind(sock_from_server, (struct sockaddr *) &recv_addr, sizeof(recv_addr));
    return 0;
}

int setup_inet_sockets(char *serv_name, int portno) {

    strcpy(client_sender_info.inet_info.serv_name, "localhost");
    client_sender_info.inet_info.portno = portno + 1;

    sock_to_server = socket(AF_INET, SOCK_DGRAM, 0);
    sock_from_server = socket(AF_INET, SOCK_DGRAM, 0);
    if (sock_to_server < 0 || sock_from_server < 0) {
        printAndQuit("INET socket error");
    }

    //server addr
    struct sockaddr_in addr;
    struct hostent *serv = gethostbyname(serv_name);
    if (serv == NULL) {
        printAndQuit("Wrong server name");
    }
    setup_serv_addr(&addr, serv, portno);


    if (connect(sock_to_server, (struct sockaddr *) &addr, sizeof(addr)) < 0)
        return -1;


    //client_addr

    struct sockaddr_in cli_addr;
    bzero((char *) &cli_addr, sizeof(cli_addr));

    cli_addr.sin_family = AF_INET;
    cli_addr.sin_addr.s_addr = INADDR_ANY;
    cli_addr.sin_port = htons(client_sender_info.inet_info.portno); // convert to network byte order

    //bind serv_addr to socket
    while (bind(sock_from_server, (struct sockaddr *) &cli_addr, sizeof(cli_addr)) < 0) {
        client_sender_info.inet_info.portno += 1;
        cli_addr.sin_port = htons(client_sender_info.inet_info.portno); // convert to network byte order
        fprintf(stderr, "Cannot bind in socket, trying next port\n");
    }


    return 0;
}


void process_expr(const message_t *msg) {
    expression_t *expr;
    double result;

    expr = (expression_t *) msg->buf;

    result = evaluate_expression(*expr);

    message_t response;
    response.conn_mode = msg->conn_mode;
    response.type = MSG_DOUBLE;
    response.sender_info = client_sender_info;

    memcpy(response.buf, &result, sizeof(result));


    send(sock_to_server, &response, sizeof(response), 0);

}

void parse_args_client(int argc, char **argv, char cli_name[BUF_MED], enum connection_mode *conn_mode,
                       char serv_name[BUF_MED], int *portno) {
    if (argc != 4 && argc != 5) {
        prompt_client(argv[0]);
        exit(1);
    }


    strcpy(cli_name, argv[1]);
    strcpy(serv_name, argv[3]);
    if (sscanf(argv[2], "%d", conn_mode) != 1 || !(*conn_mode == UNIX || *conn_mode == INET)) {
        prompt_client(argv[0]);
        exit(1);
    }
    if (argc == 5) {
        *portno = -1;
        if (sscanf(argv[4], "%d", portno) != 1 || *portno <= 0) {
            prompt_client(argv[0]);
            exit(1);
        }
    }
}


void prompt_client(char *fname) {
    printf("usage: %s client_name conn_type serv_name [portno]\n"
                   "conn_type: 0 - UNIX -local sockets\n"
                   "conn_type: 1 - INET\n"
                   "if conn_type = 1 you should provide portno\n", fname);
}

double evaluate_expression(expression_t expr) {
    switch (expr.op) {
        case ADD:
            return expr.a + expr.b;
        case SUBTRACT:
            return expr.a - expr.b;
        case MULTIPLY:
            return expr.a * expr.b;
        case DIVIDE:// for b=0  returns inf? :D
            return expr.a / expr.b;
        default:
            printAndQuit("Wrong operation in evaluate_expression\n");
    }
    return 0;
}
