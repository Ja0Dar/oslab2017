
#include <sys/socket.h>
#include <stdio.h>
#include <netinet/in.h>
#include <sys/un.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include "socket_functions.h"
#include "functions.h"
#include <sys/epoll.h>
#include <fcntl.h>
#include <stdbool.h>

struct client {
    int isAvailable;
    char name[BUF_MED];
    time_t last_pinged;
    enum connection_mode conn_mode;
    sender_info_t client_adress_info;
} typedef client_t;


volatile client_t volatile clients[MAX_CLIENTS];
sem_t sem_clients;
int cli_count = 0;
int sock_output_unix;
int sock_output_inet;

void * pinger(void * arg);
void close_client(int cli_id);

int add_client(const char *name, enum connection_mode con_mod, sender_info_t sender_info);

int get_client_index_from_name(const char *name);

int send_msg_to(message_t *msg, enum connection_mode con_mod, sender_info_t recipient_info);

void *operation_dispacher(void *arg);


void parse_args(int argc, char **argv, char *unix_socket_fd, int *portno);

int setup_and_bind_unix_socket(const char *unix_socket_name);

int get_client_index_from_sender_info(enum connection_mode conn_mode, sender_info_t sender_info);

int setup_and_bind_inet_socket(int portno);

int make_socket_non_blocking(int sfd);

void print_prompt(char *fname);

void process_adding_client(const message_t *msg);

void print_result_from_msg(const message_t *msg);

void process_ping(message_t *pMessage);

int main(int argc, char **argv) {
    sem_init(&sem_clients, 0, 1);// init (sem_ptr, 0 - for threads, 1 - initial value);


// Args
    char unix_socket_name[UNIX_PATH_MAX];
    int portno;
    parse_args(argc, argv, unix_socket_name, &portno);


    unlink(unix_socket_name);
    int socket_unix = setup_and_bind_unix_socket(unix_socket_name);
    if (socket_unix < 0) {
        printAndQuit("Cannot setup unix socket\n");
    }

    //setup tcp socket
    int socket_in = setup_and_bind_inet_socket(portno);
    if (socket_in < 0)printAndQuit("Cannot setup inet socket\n");

    //sentup sock for sending to clients
    sock_output_inet = socket(AF_INET, SOCK_DGRAM, 0);
    sock_output_unix = socket(AF_UNIX, SOCK_DGRAM, 0);
//todo addr af inet cleanup
    void cleanup_socket() {
        shutdown(sock_output_unix, SHUT_RDWR);
        close(sock_output_unix);
        shutdown(sock_output_inet, SHUT_RDWR);
        close(sock_output_inet);
        shutdown(socket_unix, SHUT_RDWR);
        close(socket_unix);
        unlink(unix_socket_name);
        shutdown(socket_in, SHUT_RDWR);
        close(socket_in);
        printf("Cleanup done\n ");
    }

    signal(SIGINT, exit);
    signal(SIGTERM, exit); //Clion

    atexit(cleanup_socket);


    int epoll_fd = epoll_create1(0);
    struct epoll_event tmp_event;
    tmp_event.events = EPOLLIN; // let -  tmp_event przy niepustym buferze
    tmp_event.data.fd = socket_unix;
    epoll_ctl(epoll_fd, EPOLL_CTL_ADD, socket_unix, &tmp_event);

    tmp_event.data.fd = socket_in;
    epoll_ctl(epoll_fd, EPOLL_CTL_ADD, socket_in, &tmp_event);


    int incoming_fd;
    struct epoll_event *incoming_events = calloc(60, sizeof(struct epoll_event));
    int number_of_incoming_events;


    pthread_t prompter, pinger_pthread;
    pthread_create(&prompter, NULL, operation_dispacher, NULL);
    pthread_create(&pinger_pthread,NULL,pinger,NULL);

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wmissing-noreturn"
    while (1) {
        //Get buch of events
        number_of_incoming_events = epoll_wait(epoll_fd, incoming_events, 60, -1);

        //process al of them
        for (int i = 0; i < number_of_incoming_events; ++i) {
            incoming_fd = incoming_events[i].data.fd;
            if (/*!incoming_events[i].events & EPOLLIN||*/ incoming_events[i].events & EPOLLRDHUP ||
                                                           incoming_events[i].events & EPOLLHUP) {
                // why this is
                //remove clients if necesarry
                continue;

            } else if (incoming_fd == socket_unix || incoming_fd == socket_in) {
                // We have a notification on the listening socket, which means one or more incoming connections.
                //  So we will add incoming socket to epoll
                message_t msg;

                struct sockaddr addr;
                socklen_t len;
                recvfrom(incoming_fd, &msg, sizeof(msg), 0, &addr, &len);


                switch (msg.type) {
                    case MSG_NAME:
                        process_adding_client(&msg);
                        printf("MSG name\n");
                        break;
                    case MSG_DOUBLE:
                        printf("MSG double\n");
                        print_result_from_msg(&msg);
                        break;
                    case MSG_PING:
                        process_ping(&msg);
//                        printf("MSG ping\n");

                        break;
                    default:
                        fprintf(stderr, "Wrong msg type%d\n", msg.type);

                }
                fflush(stdout);


            } else {
                fprintf(stderr, "receiving message from client\n");


            }


        }

    }
#pragma clang diagnostic pop


    return 0;
}

void process_ping(message_t *pMessage) {
    int cli_ind= get_client_index_from_sender_info(pMessage->conn_mode,pMessage->sender_info);

    sem_wait(&sem_clients);
    time(&(clients[cli_ind].last_pinged));
    sem_post(&sem_clients);
}

void print_result_from_msg(const message_t *msg) {
    int cli_ind = get_client_index_from_sender_info(msg->conn_mode, msg->sender_info);
    if (cli_ind < 0) {
        printAndQuit("cannot find client in  array\n");
    }

    double *res = (double *) msg->buf;
    printf("Client %s responded with %f\n", clients[cli_ind].name, *res);
    fflush(stdout);// easier debugging :)
}

int send_msg_to(message_t *msg, enum connection_mode con_mod, sender_info_t recipient_info) {
    if (con_mod == UNIX) {

        struct sockaddr_un addr;
        strcpy(addr.sun_path, recipient_info.unix_name);
        addr.sun_family = AF_UNIX;
//        printf("Sending message to %s\n", recipient_info.unix_name);
        sendto(sock_output_unix, msg, sizeof(message_t), 0, (struct sockaddr *) &addr, sizeof(addr));


    } else {
        struct sockaddr_in addr;
        struct hostent *serv = gethostbyname(recipient_info.inet_info.serv_name);
        if (serv == NULL) {
            printAndQuit("Wrong server name");
        }
        setup_serv_addr(&addr, serv, recipient_info.inet_info.portno);
//        printf("Sending message to %s : %d\n", recipient_info.inet_info.serv_name, recipient_info.inet_info.portno);

        sendto(sock_output_inet, msg, sizeof(message_t), 0, (struct sockaddr *) &addr, sizeof(addr));
    }


}


void process_adding_client(const message_t *msg) {
    char *name = msg->buf;
    int st = add_client(msg->buf, msg->conn_mode, msg->sender_info);

    message_t response;

    if (st < 0) {
        printf("Cannot add client %s\n", name);
        response.type = MSG_REJECT;
    } else {
        printf("Added client %s\n", name);
        response.type = MSG_ACCEPT;
    }
    fflush(stdout);
    send_msg_to(&response, msg->conn_mode, msg->sender_info);

}

void parse_args(int argc, char **argv, char *unix_socket_fd, int *portno) {
    if (argc != 3) {
        print_prompt(argv[0]);
        exit(1);
    }

    strcpy(unix_socket_fd, argv[1]);

    if (sscanf(argv[2], "%d", portno) != 1) {
        printf("Wrong portno \n");
        exit(1);
    }

    if (*portno <= 0) {
        printf("Portno should be positive int\n");
        exit(1);
    }


}


void print_prompt(char *fname) {
    printf("Usage: %s linux_socket_name port_nr \n", fname);
}


//todo - rm
int make_socket_non_blocking(int sfd) {
//    source:
//    https://banu.com/blog/2/how-to-use-epoll-a-complete-example-in-c/
    int flags, s;

    flags = fcntl(sfd, F_GETFL, 0);
    if (flags == -1) {
        perror("fcntl");
        return -1;
    }

    flags |= O_NONBLOCK;
    s = fcntl(sfd, F_SETFL, flags);
    if (s == -1) {
        perror("fcntl");
        return -1;
    }

    return 0;
}

int add_client(const char *name, enum connection_mode con_mod, sender_info_t sender_info) {
    if (get_client_index_from_name(name) >= 0) {
        fprintf(stderr, "Client name is there\n");
        return -1;
    }
    if (get_client_index_from_sender_info(con_mod, sender_info) >= 0) {
        fprintf(stderr, "sender info is here\n");
        return -1;
    }

    volatile struct client c;
    time(&c.last_pinged);
    c.isAvailable = 1;
    strcpy(c.name, name);
    c.client_adress_info = sender_info;
    c.conn_mode = con_mod;

    sem_wait(&sem_clients);
    int res = cli_count;
    clients[cli_count++] = c;
    sem_post(&sem_clients);
    return res;
}

int get_client_index_from_sender_info(enum connection_mode conn_mode, sender_info_t sender_info) {

    sem_wait(&sem_clients);
    if (conn_mode == UNIX) {
        for (int i = 0; i < cli_count; ++i) {
            if (clients[i].isAvailable && clients[i].conn_mode == UNIX &&
                strcmp(clients[i].client_adress_info.unix_name, sender_info.unix_name) == 0) {
                sem_post(&sem_clients);
                return i;
            }
        }
    } else if (conn_mode == INET) {
        for (int i = 0; i < cli_count; ++i) {
            if (clients[i].isAvailable && clients[i].conn_mode == INET &&
                strcmp(clients[i].client_adress_info.inet_info.serv_name, sender_info.inet_info.serv_name) == 0 &&
                clients[i].client_adress_info.inet_info.portno == sender_info.inet_info.portno) {
                sem_post(&sem_clients);
                return i;
            }
        }
    }

    sem_post(&sem_clients);
    return -1;
}

int get_client_index_from_name(const char *name) {
    sem_wait(&sem_clients);
    for (int i = 0; i < cli_count; ++i) {
        if (clients[i].isAvailable && strcmp(clients[i].name, name) == 0) {
            sem_post(&sem_clients);
            return i;
        }
    }
    sem_post(&sem_clients);
    return -1;
}

int setup_and_bind_unix_socket(const char *unix_socket_name) {

    int socket_unix;
    socket_unix = socket(AF_UNIX, SOCK_DGRAM, 0);
    if (socket_unix < 0 || make_socket_non_blocking(socket_unix) < 0) {
        fprintf(stderr, "Cannot setup  unix socket \n");
        return -1;
    }

    struct sockaddr_un unix_addr;
    strcpy(unix_addr.sun_path, unix_socket_name);
    unix_addr.sun_family = AF_UNIX;

    if (bind(socket_unix, (struct sockaddr *) &unix_addr, sizeof(unix_addr)) < 0) {
        fprintf(stderr, "Cannot bind socket unix\n");
        return -1;
    }

    return socket_unix;
}

int setup_and_bind_inet_socket(int portno) {

    int socket_in = socket(AF_INET, SOCK_DGRAM, 0);
    if (socket_in < 0 || make_socket_non_blocking(socket_in) < 0) {
        fprintf(stderr, "Cannot setup inet socket\n");
        return -1;
    }

    struct sockaddr_in inet_addr;
    bzero((char *) &inet_addr, sizeof(inet_addr));

    inet_addr.sin_family = AF_INET;
    inet_addr.sin_addr.s_addr = INADDR_ANY;
    inet_addr.sin_port = htons(portno); // convert to network byte order

    //bind serv_addr to socket
    if (bind(socket_in, (struct sockaddr *) &inet_addr, sizeof(inet_addr)) < 0) {
        fprintf(stderr, "Cannot bind in socket\n");
        return -1;
    }
    return socket_in;

}

void close_client(int cli_id) {
    //necessity? or not
    sem_wait(&sem_clients);
    clients[cli_id].isAvailable = 0;
    sem_post(&sem_clients);
//    close(clients[cli_id].socketfd);
}

void *operation_dispacher(void *arg) {
    expression_t exr;
    int choosen_client_ind;
    int forever = 1;
    while (forever) {
        printf("\n print 1+2, 3-4, 23.3*3, 32/32 or else\n");
        exr = prompt_for_expr();

        sem_wait(&sem_clients);
        if (cli_count==0){
            fprintf(stderr,"No clients available");
            sem_post(&sem_clients);
            continue;
        }
        choosen_client_ind = rand() % cli_count;
        int start_ind = choosen_client_ind;

        do {
            choosen_client_ind += 1;
            if (choosen_client_ind >= cli_count)
                choosen_client_ind = choosen_client_ind % cli_count;
            if (choosen_client_ind == start_ind) {
                break;
            }
        } while (!clients[choosen_client_ind].isAvailable);


        if (!clients[choosen_client_ind].isAvailable){
            fprintf(stderr, "No client online\n");
            sem_post(&sem_clients);
            continue;
        }
        //Sending expression

        message_t result;
        bzero(&result.sender_info, sizeof(sender_info_t));
        result.conn_mode = clients[choosen_client_ind].conn_mode;
        result.type = MSG_EXPR;
        memcpy(result.buf, &exr, sizeof(exr));
        send_msg_to(&result, clients[choosen_client_ind].conn_mode, clients[choosen_client_ind].client_adress_info);

        sem_post(&sem_clients);


        printf("\nSent to %s\n", clients[choosen_client_ind].name);


    }

}

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wmissing-noreturn"
void * pinger(void * arg){
    time_t  timediff;
    time_t now;
    message_t msg;
    msg.type=MSG_PING;

    while (1){
        sleep(1);
        sem_wait(&sem_clients);
        time(&now);
        for (int i = 0; i < cli_count; ++i) {
            if(!clients[i].isAvailable)
                continue;
            timediff= now-clients[i].last_pinged;
//            printf("now : %ld  then : clients[%d].lstping=%ld Timediff: %ld\n",now,i,clients[i].last_pinged,timediff);
            if(timediff>2){
                clients[i].isAvailable=0;
                printf("Client %s stopped responding\n",clients[i].name);
            }
            //ping client:
            msg.conn_mode=clients[i].conn_mode;
            send_msg_to(&msg,msg.conn_mode,clients[i].client_adress_info);
            //main thread will update times on clients when they re-ping
        }

        sem_post(&sem_clients);
    }


}
#pragma clang diagnostic pop

