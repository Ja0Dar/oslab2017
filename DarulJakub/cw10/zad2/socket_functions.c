
#include <stdio.h>
#include "socket_functions.h"
#include "functions.h"

void setup_serv_addr(struct sockaddr_in *serv_addr, struct hostent *serv_hostent, int portno) {

    bzero((char *) serv_addr, sizeof(struct sockaddr_in));
    serv_addr->sin_family = AF_INET;
    bcopy((char *) serv_hostent->h_addr, (char *) &serv_addr->sin_addr.s_addr,
          serv_hostent->h_length);
    serv_addr->sin_port = htons(portno);

}
