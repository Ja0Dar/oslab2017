#include <memory.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <stdarg.h>
#include <sys/sem.h>
#include <sys/time.h>
#include <sys/ipc.h>
#include <sys/types.h>
#include <time.h>
#include <sys/shm.h>
#include "common.h"

/// PRINTS
void printAndQuit(const char *str, ...) {
    va_list args;
    va_start (args, str);
    vfprintf(stderr, str, args);
    va_end (args);

    fprintf(stderr, "\n%s\n", strerror(errno));
    exit(1);
}

void printWithTime(const char *format,...){
    static struct timespec ts;

    clock_gettime(CLOCK_MONOTONIC,&ts);

    printf("%li.%li ::: ", ts.tv_sec, ts.tv_nsec);
    va_list args;
    va_start (args, format);
    vprintf(format, args);
    va_end (args);
}

void print_time() {
    struct timespec ts;
    clock_gettime(CLOCK_MONOTONIC, &ts);
    printf("%li.%li", ts.tv_sec, ts.tv_nsec);
    fflush(stdout);

}

void print_client(const Client_t client){
    printf("{pid=%i  sem_no=%d}",client.pid,client.sem_no);
}
///SEMAPHORES
int semaphore_give(int semid, unsigned short sem_no) {
    struct sembuf sb;
    sb.sem_flg = 0;
    sb.sem_op = -1;
    sb.sem_num = sem_no;
    return semop(semid, &sb, 1);
}

int semaphore_take_with_opts(int semid, unsigned short sem_no, short wait_flags,struct timespec* ts) {
    struct sembuf sb[2];
    sb[0].sem_flg = wait_flags;
    sb[1].sem_flg = IPC_NOWAIT; // TODO - ok?11
    sb[0].sem_op = 0;
    sb[1].sem_op = 1;
    sb[0].sem_num = sb[1].sem_num = sem_no;

#ifdef  __USE_GNU
    return semtimedop(semid, sb, 2,ts);
#endif

    if(ts!=NULL)
        printAndQuit("_USE_GNU not defined, so you cannot use timeout");

    return semop(semid,sb,2);
}
int semaphore_wait_with_opts(int semid, unsigned short sem_no, short wait_flags) {
    struct sembuf sb;
    sb.sem_flg = wait_flags;
    sb.sem_op = 0;
    sb.sem_num = sem_no;
    return semop(semid,&sb,1);
}

int get_semaphore_group_id(key_t key){
    return semget(key,0,GET_IPC_DEFAULT_FLAGS);
}

int create_semaphore_group(key_t key, int number_of_semaphores){
    int res= semget(key,number_of_semaphores,CREATE_IPC_DEFAULT_FLAGS);
    if (res<0){
        res=get_semaphore_group_id(key);
        semctl(res,-1,IPC_RMID);
        res= semget(key,number_of_semaphores,CREATE_IPC_DEFAULT_FLAGS);
    }
    return res;
}

int set_default_semaphore_values(int sem_id, int number_of_semaphores,short value){
    for(int i=0;i<number_of_semaphores;i++){
        if(semctl(sem_id,i,SETVAL,value)<0){
            return -1;
        }
    }
    return 0;
}

///BARBER_SHOP
barber_shop_t setup_barber_shop_shm_interface(void *shm_addr, int number_of_chairs) {
    barber_shop_t res = get_barber_shop_interface_for_shm(shm_addr);

    *(res.current_queue_length) = *(res.first_person_index) = 0;
    *(res.max_queue_length) = number_of_chairs;
    return res;
}

barber_shop_t get_barber_shop_interface_for_shm(void *shm_addr) {
    barber_shop_t res;
    res.shm_addr = shm_addr;
    res.first_person_index = shm_addr;
    int * addr=shm_addr;
    res.current_queue_length = addr + 1 * sizeof(int);
    res.max_queue_length = addr + 2 * sizeof(int);
    res.barber_seat = (Client_t*) addr + 3 * sizeof(int);
    res.queue = res.barber_seat + sizeof(Client_t);
    return res;
}


void print_barber_shop(const barber_shop_t *const barber_shop) {
    printf("=======================================\nbarber_shop_t: \n");
    printf("shm_addr= %p\n",barber_shop->shm_addr);
    printf("first_person_index= %d\n",*(barber_shop->first_person_index));
    printf("current_queue_length= %d\n",*(barber_shop->current_queue_length));
    printf("max_queue_length= %d\n",*(barber_shop->max_queue_length));
    printf("barber_seat: ");print_client(*(barber_shop->barber_seat)); printf("\n");
    printf("Queue: \n");
    for (int i = *(barber_shop->first_person_index); i < *(barber_shop->first_person_index)+ *(barber_shop->current_queue_length); ++i)
        print_client(barber_shop->queue[i % *(barber_shop->max_queue_length)]); printf("\n");
    printf("=================\n");

}


/// SHM
void * get_and_attach_shm_for_barber_shop() {
    int shm_id = shmget(SHM_KEY, 0, GET_IPC_DEFAULT_FLAGS);
    if (shm_id < 0)
        return (void *)-1;
    return attach_shm(shm_id);
}

int create_barber_shop_shm(size_t size){
    int res= shmget(SHM_KEY,size,CREATE_IPC_DEFAULT_FLAGS);
    if (res<0){
            res = shmget(SHM_KEY, 0,GET_IPC_DEFAULT_FLAGS);
            shmctl(res, IPC_RMID, 0);
            res = shmget(SHM_KEY, size, CREATE_IPC_DEFAULT_FLAGS);
        }
    return res;
}


void * attach_shm(int shm_id){
    return shmat(shm_id,NULL,0);
}


///BARBER SHOP "METHODS"
void move_first_client_to_barber_chair(barber_shop_t *barber_shop) {
    *(barber_shop->barber_seat) = barber_shop->queue[*(barber_shop->first_person_index)];
    *(barber_shop->first_person_index) = (*(barber_shop->first_person_index) + 1) % *(barber_shop->max_queue_length);
    *(barber_shop->current_queue_length) -= 1;
}

int waiting_room_is_empty(const barber_shop_t *bs) {
    return *(bs->current_queue_length) == 0;
}

int waiting_room_is_full(const barber_shop_t *bs) {
    return *(bs->current_queue_length) >= *(bs->max_queue_length);
}

void seat_in_waiting_room(Client_t client, barber_shop_t *barber_shop) {
    if (waiting_room_is_full(barber_shop))
        printAndQuit("Waiting room is full you should check that earlier");
    //seat client
    int i=*(barber_shop->first_person_index)+*(barber_shop->current_queue_length);
    barber_shop->queue[i % *(barber_shop->max_queue_length)] = client;
    //increase queue_len
    *(barber_shop->current_queue_length) +=1;

}

void *waiting_room_queue_end_addr(barber_shop_t wr) {
    return ((int *)wr.shm_addr) + NUMBER_OF_INTS_IN_SHM_BEFORE_CLIENT_ARRAY * sizeof(int) +
           (*(wr.max_queue_length)) * sizeof(Client_t) + sizeof(Client_t);
}


