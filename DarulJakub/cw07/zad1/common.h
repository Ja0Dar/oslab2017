#ifndef ZAD1_COMMON_H

#include <unistd.h>
#include <stdio.h>

#define _GNU_SOURCE
#define SMALL_BUF 64
#define MEDIUM_BUF 256
#define BIG_BUFF 1024

#define CREATE_IPC_DEFAULT_FLAGS IPC_CREAT | IPC_EXCL | 0600
#define GET_IPC_DEFAULT_FLAGS 0600


#define SHM_KEY 1234
#define SERVER_SEM_KEY 256*256
#define CLIENTS_SEM_KEY 16*16*5

#define BARBER_SEM_NO 0
#define WAITING_ROOM_SEM_NO 1
#define IS_FACTORY_IN_PREPARATION_BOOL_SEM_NO 2  // is factory ready?
#define BARBER_SLEEPS_SEM_NO 3
#define FACTORY_SEM 4 // i am factory
#define NO_OF_SERVER_SEMAPHORES 5


#define semaphore_take(semid, sem_no) \
    semaphore_take_with_opts(semid,sem_no,0,NULL)
#define semaphore_take_now(semid, sem_no) \
    semaphore_take_with_opts(semid,sem_no,IPC_NOWAIT,NULL)
#define semaphore_take_with_timeout(semid, sem_no, timeout) \
    semaphore_take_with_opts(semid,sem_no,0,timeout)

#define semaphore_wait(semid, sem_no) \
    semaphore_wait_with_opts(semid,sem_no,0)

#define semaphore_check(semid, sem_no) \
    semaphore_wait_with_opts(semid,sem_no,IPC_NOWAIT)
#define NUMBER_OF_INTS_IN_SHM_BEFORE_CLIENT_ARRAY 4

struct Client_t {
    pid_t pid;
    short sem_no;
} typedef Client_t;

struct barber_shop_t {
    void *shm_addr; // shm[0] - just for convenience
    int *first_person_index;  // shm[0]
    int *current_queue_length;//shm[1]
    int *max_queue_length;//shm[2]
    Client_t *barber_seat;
    Client_t *queue;//shm[3]
//    int * queue_end_addr;//shm[size of shm];
} typedef barber_shop_t;

void printAndQuit(const char *str, ...);
void printWithTime(const char *format,...);
int create_semaphore_group(key_t key, int number_of_semaphores);
int get_semaphore_group_id(key_t key);
int set_default_semaphore_values(int sem_id, int number_of_semaphores,short value);

void print_client(const Client_t client);

void *attach_shm(int shm_id);

int semaphore_take_with_opts(int semid, unsigned short sem_no, short wait_flags, struct timespec *ts);

int semaphore_wait_with_opts(int semid, unsigned short sem_no, short wait_flags);

barber_shop_t get_barber_shop_interface_for_shm(void *shm_addr);
int create_barber_shop_shm(size_t size);

barber_shop_t setup_barber_shop_shm_interface(void *shm_addr, int number_of_chairs);

int waiting_room_is_full(const barber_shop_t *bs);

void seat_in_waiting_room(Client_t client, barber_shop_t *barber_shop);

void move_first_client_to_barber_chair(barber_shop_t *);

int waiting_room_is_empty(const barber_shop_t *bs);

int semaphore_give(int semid, unsigned short sem_no);

void print_time();

void print_barber_shop(const barber_shop_t *const barber_shop);

void *get_and_attach_shm_for_barber_shop();

#define ZAD1_COMMON_H

#endif //ZAD1_COMMON_H
