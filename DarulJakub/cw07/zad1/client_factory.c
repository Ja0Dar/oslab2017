#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdlib.h>
#include <sys/sem.h>
#include <sys/wait.h>
#include "common.h"

void parse_args(int argc, char **argv, int *client_no, int *cut_no);

void cleanup_parent();

void cleanup_child();

void act_like_client(int client_id, int wanted_cuts, int server_sem_id, int client_sem_id);

int wake_up_barber_and_give_waiting_room();

int client_semaphors_id, server_sem_id;
void *shm_addr;

int main(int argc, char **argv) {
    int number_of_clients;
    int number_of_cuts;
    parse_args(argc, argv, &number_of_clients, &number_of_cuts);


    //get server semaphors;
    server_sem_id = get_semaphore_group_id(SERVER_SEM_KEY);
    if (server_sem_id < 0)
        printAndQuit("Cannot get server semaphors\n");

    signal(SIGINT, exit);
    signal(SIGTERM, exit);



    //Create client semaphors
    if ((client_semaphors_id = create_semaphore_group(CLIENTS_SEM_KEY, number_of_clients)) < 0)
        printAndQuit("Cannot create client semaphores\n");


    // all clients are free
    if (set_default_semaphore_values(client_semaphors_id, number_of_clients, 0) < 0)
        printAndQuit("Cannot set semaphore default values");

    semaphore_give(server_sem_id, IS_FACTORY_IN_PREPARATION_BOOL_SEM_NO);


    semaphore_take(server_sem_id, FACTORY_SEM);

    pid_t pid;
    for (int i = 0; i < number_of_clients; i++) {
        pid = fork();
        if (pid < 0)
            printAndQuit("Cannot fork\n");
        else if (pid == 0)
            act_like_client(i, number_of_cuts, server_sem_id, client_semaphors_id);

    }
    // TODO - move it back and figure a way to not run it when child dies
    atexit(cleanup_parent);

    printf("Waiting for children\n");
    while (wait(NULL) > 0);
    exit(0);
}

void act_like_client(int client_id, int wanted_cuts, int server_sem_id, int client_sem_id) {
    atexit(cleanup_child);

    pid_t mypid = getpid();

    shm_addr = get_and_attach_shm_for_barber_shop();

    barber_shop_t barber_shop = get_barber_shop_interface_for_shm(shm_addr);


    Client_t this_client;
    this_client.pid = mypid;
    this_client.sem_no = (short) client_id;

    if (semaphore_take(client_sem_id, client_id) < 0)printAndQuit("Client %d cannot take his semaphore\n", mypid);

    while (wanted_cuts > 0) {
        semaphore_take(server_sem_id, WAITING_ROOM_SEM_NO);


        if (!waiting_room_is_full(&barber_shop)) {

            // sit directly in barbers chair
            if (waiting_room_is_empty(&barber_shop) && (semaphore_take_now(server_sem_id, BARBER_SEM_NO) > -1)) {

                *(barber_shop.barber_seat) = this_client;
                printWithTime("{%d,%d} WAKES UP barber.\n", mypid, client_id);

                wanted_cuts -= 1;
                if (wake_up_barber_and_give_waiting_room() < 0)printAndQuit("Cannot wake up barber");

                //
                if (semaphore_take(client_sem_id, client_id) < 0)
                    perror("Cannot return from barber");
                printWithTime("Client {%d,%d} returned from barber\n", mypid, client_id);
                continue;
            }


            /// if waiting room isnt full and barber is awake /waiting room not empty :
            {
                printWithTime("Client {%d,%d} sits in waiting room\n", mypid, client_id);

                seat_in_waiting_room(this_client, &barber_shop);
                semaphore_give(server_sem_id, WAITING_ROOM_SEM_NO);

                wanted_cuts -= 1;
                //wait until barber cuts your hair
                if (semaphore_take(client_sem_id, client_id) < 0)
                    perror("Cannot return from barber");
                printWithTime("Client {%d,%d} returned from barber\n", mypid, client_id);
            }
        } else {
            semaphore_give(server_sem_id, WAITING_ROOM_SEM_NO);
            printWithTime("waiting room full - {%d,%d} leaves\n", mypid, client_id);

        }
    }
    printf("{%d,%d} dies\n", mypid, client_id);
    exit(0);

}

int wake_up_barber_and_give_waiting_room() {
    static struct sembuf sb[3] = {{BARBER_SLEEPS_SEM_NO, -1, IPC_NOWAIT},
                                  {BARBER_SEM_NO,        -1, IPC_NOWAIT},
                                  {WAITING_ROOM_SEM_NO,  -1, IPC_NOWAIT}};//{num, op, flag}
    return semop(server_sem_id, sb, 3);
}


void parse_args(int argc, char **argv, int *client_no, int *cut_no) {

    if (argc != 3)
        printAndQuit("Required 2 arguments : [number of clients]  [number of cuts]\n");

    if (sscanf(argv[1], "%d", client_no) != 1 || *client_no <= 0)
        printAndQuit("Number of clients should be int, greater than 0.\n");

    if (sscanf(argv[2], "%d", cut_no) != 1 || *cut_no <= 0)
        printAndQuit("Number of cuts should be int, greater than 0.\n");

}

void cleanup_parent() {
    if (semaphore_take(server_sem_id, IS_FACTORY_IN_PREPARATION_BOOL_SEM_NO) < 0)
        perror("Couldn't take semaphore FACTORY_IN_PREPARATION");
    print_time();
    printf("\n");
    semaphore_give(server_sem_id, FACTORY_SEM);
    if (semctl(client_semaphors_id, -1, IPC_RMID) < 0)
        perror("Cannot remove client semaphores.\n");
    else
        printf("Client factory semaphores removed.\n");
}

void cleanup_child() {

    if (shmdt(shm_addr) < 0)
        perror("Cannot detach shm");
//    else
//        printf("Detached shm\n");
}
