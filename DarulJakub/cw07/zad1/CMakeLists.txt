#cmake_minimum_required(VERSION 3.6)
#project(zad1)
#
#set(CMAKE_C_STANDARD 99)
#
#set(SOURCE_FILES main.c)
#add_executable(zad1 ${SOURCE_FILES})


cmake_minimum_required (VERSION 3.5)
project (zad1)

set (CMAKE_COMPILER_IS_GNUC)
set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wall -pedantic")

include_directories (
        "${PROJECT_SOURCE_DIR}"
)

set (SERVER_files barber.c common.c common.h)
add_executable (server.out ${SERVER_files})

set (CLIENT_files client_factory.c common.c common.h)
add_executable (client.out ${CLIENT_files})