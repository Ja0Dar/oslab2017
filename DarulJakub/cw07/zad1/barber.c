//System V
#include <sys/sem.h>
#include <stdio.h>
#include <sys/ipc.h>
#include<sys/types.h>
#include <sys/shm.h>
#include <signal.h>
#include <stdlib.h>
#include "common.h"

int get_chair_no_form_args(int argc, char **argv);

void cleanup();

void process_client(Client_t *client, int client_semaphores_id);

int get_barber_sem_if_barber_is_awake_with_timeout();
int take_nap_and_give_barber_and_waiting_room();


int shm_id, server_semgrp_id;
void *shm_addr;

int main(int argc, char **argv) {
    int number_of_chairs = get_chair_no_form_args(argc, argv);

    size_t shm_size = NUMBER_OF_INTS_IN_SHM_BEFORE_CLIENT_ARRAY * sizeof(int) + number_of_chairs * sizeof(Client_t) +
                      sizeof(Client_t);

    signal(SIGTERM, exit);
    signal(SIGINT, exit);
    atexit(cleanup);
    if ((shm_id = create_barber_shop_shm(shm_size)) < 0)printAndQuit("Cannot create SHM\n");
    if ((shm_addr = attach_shm(shm_id)) < (void *) 0)printAndQuit("Cannot attach SHM");

    barber_shop_t barber_shop = setup_barber_shop_shm_interface(shm_addr, number_of_chairs);


    if ((server_semgrp_id = create_semaphore_group(SERVER_SEM_KEY, NO_OF_SERVER_SEMAPHORES)) < 0)
        printAndQuit("Cannot create server semaphores\n");

    if (set_default_semaphore_values(server_semgrp_id, NO_OF_SERVER_SEMAPHORES, 1) < 0)
        printAndQuit("Cannot set default semaphore values\n");

    if (semaphore_wait(server_semgrp_id, IS_FACTORY_IN_PREPARATION_BOOL_SEM_NO) < 0)
        printAndQuit("Semaphore wait error\n");

    int client_factory_sem_id;
    if ((client_factory_sem_id = get_semaphore_group_id(CLIENTS_SEM_KEY)) < 0) printAndQuit("Cannot get client sem");


    int forever = 1;

    semaphore_give(server_semgrp_id, WAITING_ROOM_SEM_NO);
    semaphore_give(server_semgrp_id, BARBER_SEM_NO);
    semaphore_give(server_semgrp_id, FACTORY_SEM);
    while (forever) {
//        wait for Barber sem and barber access, take barber sem//
        while (get_barber_sem_if_barber_is_awake_with_timeout() < 0) {
            if (semaphore_take_now(server_semgrp_id, FACTORY_SEM) >= 0) {
                printf("Prev factory finished. Waiting for new to arrive.\n");
                semaphore_wait(server_semgrp_id, IS_FACTORY_IN_PREPARATION_BOOL_SEM_NO);
                client_factory_sem_id = get_semaphore_group_id(CLIENTS_SEM_KEY);
                semaphore_give(server_semgrp_id, FACTORY_SEM);// allow factory to run
            }
        }

        //Work-without-sleep-loop
        while (1) {
            process_client(barber_shop.barber_seat, client_factory_sem_id);

            if (semaphore_take(server_semgrp_id, WAITING_ROOM_SEM_NO) < 0) {
                printAndQuit("Canot take wr sem\n");
            }
//            print_barber_shop(&barber_shop);

            if (waiting_room_is_empty(&barber_shop))
                break;
            else
                move_first_client_to_barber_chair(&barber_shop);

            if (semaphore_give(server_semgrp_id, WAITING_ROOM_SEM_NO) < 0)
                printAndQuit("Cannot give waiting room sem");
        }


        //everyone cut, so go to sleep and give waiting room
        if (take_nap_and_give_barber_and_waiting_room()< 0)
            printAndQuit("Cannot go to sleep\n");
        else
            printWithTime("Barber takes a nap \n");

    }


}

int take_nap_and_give_barber_and_waiting_room() {
    static struct sembuf goodnight_buf[3] = {{BARBER_SLEEPS_SEM_NO, 1,  IPC_NOWAIT},
                                             {BARBER_SEM_NO,        -1, IPC_NOWAIT},
                                             {WAITING_ROOM_SEM_NO,  -1, IPC_NOWAIT}};//{num, op, flag}
    return semop(server_semgrp_id, goodnight_buf, 3);
}

int get_barber_sem_if_barber_is_awake_with_timeout() {
    static struct sembuf bsb[3] = {{BARBER_SLEEPS_SEM_NO, 0, 0},
                                   {BARBER_SEM_NO,        0, 0},
                                   {BARBER_SEM_NO,        1, IPC_NOWAIT}};//{num, op, flag}
    static struct timespec timeout = {2, 0};//{secs, nanosecs
    return semtimedop(server_semgrp_id, bsb, 3, &timeout);
}

void process_client(Client_t *client, int client_semaphores_id) {
    printWithTime("{%d,%d} - HAIRCUT STARTS. \n", client->pid, client->sem_no);


    //barber does his job

    if (semaphore_give(client_semaphores_id, client->sem_no) < 0)
        perror("Cannot give sem to client\n");
    printWithTime("{%d,%d} - HAIRCUT ENDS. \n", client->pid, client->sem_no);

}

void cleanup() {

    if (shmdt(shm_addr) < 0)
        perror("Couldn't detach shm");
    else
        printf("Shm detached\n");
    if (shmctl(shm_id, IPC_RMID, 0) < 0)
        perror("Couldn't remove shm.\n");
    else
        printf("Removed shm\n");
    if (semctl(server_semgrp_id, -1, IPC_RMID) < 0)
        perror("Cannot remove server semaphores");
    else
        printf("Removed server semaphores\n");
}

int get_chair_no_form_args(int argc, char **argv) {
    int res;

    if (argc != 2)
        printAndQuit("Required argument : number of barber's chairs\n");

    if (sscanf(argv[1], "%d", &res) != 1)
        printAndQuit("Number of chairs should be int.\n");

    if (res <= 0)
        printAndQuit("Number of chairs should be positive, natural.\n");
    return res;
}
