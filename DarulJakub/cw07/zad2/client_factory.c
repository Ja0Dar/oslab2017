#include <stdlib.h>
#include <sys/wait.h>
#include "common.h"

void parse_args(int argc, char **argv, int *client_no, int *cut_no);

int get_server_semaphores(struct server_semaphores *serverSemaphores);

void cleanup_parent();

void cleanup_child();

void act_like_client(int client_id, int wanted_cuts, struct server_semaphores server_Sem);

struct server_semaphores serv_sem;

//Child :
char *sem_name;
barber_shop_t *barber_shop;
sem_t *client_sem;

int main(int argc, char **argv) {
    signal(SIGINT, exit);
    signal(SIGTERM, exit);


    int number_of_clients, number_of_cuts;
    parse_args(argc, argv, &number_of_clients, &number_of_cuts);

    if (get_server_semaphores(&serv_sem) < 0)
        printAndQuit("Cannot get server semaphores\n");


    sem_post(serv_sem.factory_is_ready);
    if (semaphore_take(serv_sem.factory_sem) < 0)printAndQuit("Cannot take sem\n");
//    else printf("RECIEVED FACTORY SEM\n");

    pid_t pid;
    for (int i = 0; i < number_of_clients; i++) {
        pid = fork();
        if (pid < 0)
            printAndQuit("Cannot fork\n");
        else if (pid == 0)
            act_like_client(i, number_of_cuts, serv_sem);

    }
    atexit(cleanup_parent);

//    printf("Waiting for children\n");
    while (wait(NULL) > 0);
    exit(0);
}

void act_like_client(int client_id, int wanted_cuts, struct server_semaphores server_Sem) {
    atexit(cleanup_child);
    pid_t my_pid = getpid();
    sem_name = generate_client_semaphore_name(client_id);
    client_sem = create_semaphore(sem_name, 1);

    if (client_sem < (sem_t *) 0)printAndQuit("Client cannot get sem\n");
    barber_shop = get_and_attach_shm_for_barber_shop();
    if (barber_shop < (barber_shop_t *) 0)printAndQuit("Cannot get shm");


    Client_t this_client;
    this_client.pid = my_pid;
    this_client.sem_no = (short) client_id;

    if (semaphore_take(client_sem) < 0)
        printAndQuit("Client {%d,%d} cannot take his semaphore\n", my_pid,client_id);

//    printf("Client %d took his sem\n", client_id);

    while (wanted_cuts > 0) {

        semaphore_take(server_Sem.waiting_room_sem);

        if (!waiting_room_is_full(barber_shop)) {

            // sit directly in barbers chair
            if (waiting_room_is_empty(barber_shop) && (semaphore_take_now(server_Sem.barber_sem) > -1)) {


                barber_shop->barber_seat = this_client;
                printWithTime("{%d,%d} WAKES UP barber.\n", my_pid, client_id);

                wanted_cuts -= 1;
                semaphore_give(server_Sem.barber_sleeps_sem);
                semaphore_give(server_Sem.barber_sem);

                semaphore_wait_for_someone_to_take(server_Sem.barber_sem);

                semaphore_give(server_Sem.waiting_room_sem);

                if (semaphore_take(client_sem) < 0)
                    perror("Cannot return from barber");
                printWithTime("Client {%d,%d} returned from barber\n", my_pid, client_id);
                continue;
            }


            /// if waiting room isnt full and barber is awake /waiting room not empty :
            {
                printWithTime("Client {%d,%d} sits in waiting room\n", my_pid, client_id);

                seat_in_waiting_room(this_client, barber_shop);
                semaphore_give(server_Sem.waiting_room_sem);

                wanted_cuts -= 1;
                //wait until barber cuts your hair
                if (semaphore_take(client_sem) < 0)
                    perror("Cannot return from barber");
                printWithTime("Client {%d,%d} returned from barber\n", my_pid, client_id);
            }
        } else {
            semaphore_give(server_Sem.waiting_room_sem);
            printWithTime("waiting room full - {%d,%d} leaves\n", my_pid, client_id);

        }
    }
    printf("{%d,%d} dies\n", my_pid, client_id);
    exit(0);

}


void parse_args(int argc, char **argv, int *client_no, int *cut_no) {

    if (argc != 3)
        printAndQuit("Required 2 arguments : [number of clients]  [number of cuts]\n");

    if (sscanf(argv[1], "%d", client_no) != 1 || *client_no <= 0)
        printAndQuit("Number of clients should be int, greater than 0.\n");

    if (sscanf(argv[2], "%d", cut_no) != 1 || *cut_no <= 0)
        printAndQuit("Number of cuts should be int, greater than 0.\n");

}

void cleanup_parent() {
    semaphore_give(serv_sem.factory_sem);
    if(close_server_semaphores(serv_sem)<0)perror("Client_factory cannot close server semaphores\n");
    printf("Client factory cleanup complete\n");
}

void cleanup_child() {
    if(close_server_semaphores(serv_sem)<0)perror("Client cannot close server semaphores\n");
    if (munmap(barber_shop, sizeof(barber_shop_t)) < 0)
        perror("Cannot detach shm");
//    else printf("Detached shm\n");

    if (sem_close(client_sem) < 0)perror("Cannot close client sem\n");
//    else printf("Closed client_Sem\n");
    if (sem_unlink(sem_name) < 0)
        perror("Cannot delete client semaphore\n");
//    else printf("Deleted client sem\n");
    free(sem_name);
}


int get_server_semaphores(struct server_semaphores *serverSemaphores) {
    serverSemaphores->barber_sem = get_semaphore_handle(BARBER_SEM_NAME);
    serverSemaphores->barber_sleeps_sem = get_semaphore_handle(BARBER_SLEEPS_NAME);
    serverSemaphores->factory_sem = get_semaphore_handle(FACTORY_SEM_NAME);
    serverSemaphores->factory_is_ready = get_semaphore_handle(IS_FACTORY_IN_PREPARATION_SEM_NAME);
    serverSemaphores->waiting_room_sem = get_semaphore_handle(WAITING_ROOM_SEM_NAME);

    return check_server_semaphores(serverSemaphores);
}
