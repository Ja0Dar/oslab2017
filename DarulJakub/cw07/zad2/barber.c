//System V
#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include "common.h"


int parse_args_and_get_chair_no(int argc, char **argv);


void cleanup();

void process_client(Client_t *client);

int create_server_semaphores(struct server_semaphores *serverSemaphores);

barber_shop_t *barber_shop;
struct server_semaphores serverSemaphores;

int main(int argc, char **argv) {
    int number_of_chairs = parse_args_and_get_chair_no(argc, argv);

    signal(SIGTERM, exit);
    signal(SIGINT, exit);
    atexit(cleanup);

    int shm_id;
    if ((shm_id = create_shm(sizeof(barber_shop_t))) < 0)printAndQuit("Cannot create SHM\n");
    if ((barber_shop = attach_shm(shm_id, sizeof(barber_shop_t))) < (barber_shop_t *) 0)
        printAndQuit("Cannot attach SHM");
    barber_shop_init(barber_shop, number_of_chairs);

    if (create_server_semaphores(&serverSemaphores) < 0)printAndQuit("cannot create server semaphores\n");



    if (sem_wait(serverSemaphores.factory_is_ready) < 0)
        printAndQuit("Semaphore wait error\n");

    sem_post(serverSemaphores.waiting_room_sem);
    sem_post(serverSemaphores.barber_sem);
    sem_post(serverSemaphores.factory_sem);


    int forever = 1;
    while (forever) {


        semaphore_take(serverSemaphores.barber_sleeps_sem);
        semaphore_take(serverSemaphores.barber_sem);

        //Work-without-sleep-loop
        while (1) {
            process_client(&(barber_shop->barber_seat));

            if (semaphore_take(serverSemaphores.waiting_room_sem) < 0) {
                printAndQuit("Canot take wr sem\n");
            }
//            print_barber_shop(&barber_shop);

            if (waiting_room_is_empty(barber_shop))
                break;
            else
                move_first_client_to_barber_chair(barber_shop);

            if (semaphore_give(serverSemaphores.waiting_room_sem) < 0)
                printAndQuit("Cannot give waiting room sem");
        }


        //everyone cut, so go to sleep and give waiting room
            semaphore_give(serverSemaphores.barber_sem);
            semaphore_give(serverSemaphores.waiting_room_sem);
            printWithTime("Barber takes a nap \n");

    }

    return 1;
}

void process_client(Client_t *client) {
    printWithTime("{%d,%d} - HAIRCUT STARTS. \n", client->pid, client->sem_no);
    char *sem_name = generate_client_semaphore_name(client->sem_no);
    sem_t *cli_sem = get_semaphore_handle(sem_name);

    if (semaphore_give(cli_sem) < 0)
        perror("Cannot give sem to client\n");
    sem_close(cli_sem);
    printWithTime("{%d,%d} - HAIRCUT ENDS. \n", client->pid, client->sem_no);

}

void cleanup() {
    if (munmap(barber_shop, sizeof(barber_shop_t)) < 0)
        perror("Couldn't detach shm");
    else
        printf("Shm detached\n");
    if (shm_unlink(SHM_NAME) < 0)
        perror("Couldn't remove shm.\n");
    else
        printf("Removed shm\n");

    if (close_server_semaphores(serverSemaphores) < 0)perror("Cannot close server semaphore\n");

    if (unlink_server_semaphores() < 0)perror("Cannot delete server semaphores\n");
}

int parse_args_and_get_chair_no(int argc, char **argv) {
    int res;

    if (argc != 2)
        printAndQuit("Required argument : number of barber's chairs\n");

    if (sscanf(argv[1], "%d", &res) != 1)
        printAndQuit("Number of chairs should be int.\n");

    if (res <= 0)
        printAndQuit("Number of chairs should be positive, natural.\n");
    if(res>=MAX_CLIENT_QUEUE_LEN)
        printAndQuit("Too much chairs. Barber shop is to small. You can buy new chairs by changing MAX_CLIENT_QUEUE_LEN in common.h\n");
    return res;
}

int create_server_semaphores(struct server_semaphores *serverSemaphores) {
    //0 - busy, 1 - freee   -.-
    serverSemaphores->barber_sem = create_semaphore(BARBER_SEM_NAME, 0);
    serverSemaphores->barber_sleeps_sem = create_semaphore(BARBER_SLEEPS_NAME, 0);
    serverSemaphores->factory_sem = create_semaphore(FACTORY_SEM_NAME, 0);
    serverSemaphores->factory_is_ready = create_semaphore(IS_FACTORY_IN_PREPARATION_SEM_NAME, 0);
    serverSemaphores->waiting_room_sem = create_semaphore(WAITING_ROOM_SEM_NAME, 0);
    return check_server_semaphores(serverSemaphores);

}
