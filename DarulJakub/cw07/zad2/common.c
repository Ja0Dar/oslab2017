#include <memory.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <stdarg.h>
#include <sys/time.h>
#include <time.h>
#include <semaphore.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include "common.h"

/// PRINTS
void printAndQuit(const char *str, ...) {
    va_list args;
    va_start (args, str);
    vfprintf(stderr, str, args);
    va_end (args);

    fprintf(stderr, "\n%s\n", strerror(errno));
    exit(1);
}

void printWithTime(const char *format, ...) {
    static struct timespec ts;

    clock_gettime(CLOCK_MONOTONIC, &ts);

    printf("%li.%li ::: ", ts.tv_sec, ts.tv_nsec);
    va_list args;
    va_start (args, format);
    vprintf(format, args);
    va_end (args);
}

void print_time() {
    struct timespec ts;
    clock_gettime(CLOCK_MONOTONIC, &ts);
    printf("%li.%li", ts.tv_sec, ts.tv_nsec);
    fflush(stdout);

}

void print_client(const Client_t client) {
    printf("{pid=%i  sem_no=%d}", client.pid, client.sem_no);
}

sem_t *get_semaphore_handle(char *name) {
    return sem_open(name, DEFAULT_FLAGS);
}

sem_t *create_semaphore(char *name, unsigned int val) {
    sem_t *res = sem_open(name, CREATE_FLAGS, DEFAULT_MODE, val);
    if (res <(sem_t *) 0) {
        sem_unlink(name);
        res = sem_open(name, CREATE_FLAGS, DEFAULT_MODE, val);
    }
    return res;
}

int close_server_semaphores(struct server_semaphores ss) {
    if (sem_close(ss.factory_sem) < 0 || sem_close(ss.factory_is_ready) < 0 ||
        sem_close(ss.barber_sem) < 0 || sem_close(ss.barber_sleeps_sem) < 0 || sem_close(ss.waiting_room_sem) < 0)
        return -1;
    return 0;
}

int unlink_server_semaphores() {
    if (sem_unlink(BARBER_SEM_NAME) < 0 || sem_unlink(BARBER_SLEEPS_NAME) < 0 ||
        sem_unlink(IS_FACTORY_IN_PREPARATION_SEM_NAME) < 0 || sem_unlink(FACTORY_SEM_NAME) < 0 ||
        sem_unlink(WAITING_ROOM_SEM_NAME) < 0)
        return -1;
    return 0;
}

int check_server_semaphores(const struct server_semaphores *serverSemaphores){

    if (serverSemaphores->barber_sem < (sem_t *) 0 || serverSemaphores->barber_sleeps_sem < (sem_t *) 0 ||
        serverSemaphores->factory_sem < (sem_t *) 0 ||
        serverSemaphores->waiting_room_sem < (sem_t *) 0 ||
        serverSemaphores->factory_is_ready < (sem_t *) 0)
        return -1;
    return 0;
}

void print_barber_shop(const barber_shop_t *const barber_shop) {
    printf("=======================================\nbarber_shop_t: \n");
    printf("first_person_index= %d\n", barber_shop->first_person_index);
    printf("current_queue_length= %d\n", barber_shop->current_queue_length);
    printf("max_queue_length= %d\n", barber_shop->max_queue_length);
    printf("barber_seat: ");
    print_client(barber_shop->barber_seat);
    printf("\n");
    printf("Queue: \n");
    for (int i = barber_shop->first_person_index;
         i < barber_shop->first_person_index + barber_shop->current_queue_length; ++i)
        print_client(barber_shop->queue[i % barber_shop->max_queue_length]);
    printf("\n");
    printf("=================\n");

}


/// SHM
void *get_and_attach_shm_for_barber_shop() {
    int shm_id = shm_open(SHM_NAME, DEFAULT_FLAGS, DEFAULT_MODE);
    if (shm_id < 0)
        return (void *) -1;
    return attach_shm(shm_id, sizeof(barber_shop_t));
}

int create_shm(size_t size) {
    int res = shm_open(SHM_NAME, O_CREAT | O_EXCL | O_RDWR, DEFAULT_MODE);
    if (res < 0) {
        shm_unlink(SHM_NAME);
        res = shm_open(SHM_NAME, O_CREAT | O_EXCL | O_RDWR, DEFAULT_MODE);
    }
    if (ftruncate(res, size) < 0)printAndQuit("Cannot truncate shm\n");
    return res;
}

void barber_shop_init(barber_shop_t *barber_shop, int maxlen){
    barber_shop->first_person_index=0;
    barber_shop->current_queue_length=0;
    barber_shop->max_queue_length=maxlen;
}

void *attach_shm(int shm_id, size_t size) {
    return mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, shm_id, 0);
}


///BARBER SHOP "METHODS"
void move_first_client_to_barber_chair(barber_shop_t *barber_shop) {
    barber_shop->barber_seat = barber_shop->queue[barber_shop->first_person_index];
    barber_shop->first_person_index = (barber_shop->first_person_index + 1) % barber_shop->max_queue_length;
    barber_shop->current_queue_length -= 1;
}

int waiting_room_is_empty(const barber_shop_t *bs) {
    return bs->current_queue_length == 0;
}

int waiting_room_is_full(const barber_shop_t *bs) {
    return bs->current_queue_length >= bs->max_queue_length;
}

void seat_in_waiting_room(Client_t client, barber_shop_t *barber_shop) {
    if (waiting_room_is_full(barber_shop))
        printAndQuit("Waiting room is full you should check that earlier");
    //seat client
    int i = barber_shop->first_person_index + barber_shop->current_queue_length;
    barber_shop->queue[i % barber_shop->max_queue_length] = client;
    //increase queue_len
    barber_shop->current_queue_length += 1;

}

char *generate_client_semaphore_name(int sem_no) {
    char *res = malloc(sizeof("/client_0123456"));
    sprintf(res, "/client_%d", sem_no);
    return res;
}


void semaphore_wait_for_someone_to_take(sem_t *sem){
    int val;
    sem_getvalue(sem, &val);
    while (val != 0) {//while not taken
        sem_getvalue(sem, &val);
    }
}

