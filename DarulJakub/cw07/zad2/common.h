#ifndef ZAD1_COMMON_H

#include <unistd.h>
#include <stdio.h>
#include <semaphore.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>

#define _GNU_SOURCE
#define DEFAULT_MODE 0600
#define CREATE_FLAGS O_CREAT|O_EXCL|O_RDWR
#define DEFAULT_FLAGS O_RDWR


#define SHM_NAME "/barber_shop_shm"

#define MAX_CLIENT_QUEUE_LEN 1024

#define BARBER_SEM_NAME "/barber"
#define WAITING_ROOM_SEM_NAME "/waiting_room"
#define IS_FACTORY_IN_PREPARATION_SEM_NAME "/factory_in_preparation"
#define BARBER_SLEEPS_NAME "/barber_sleeps"
#define FACTORY_SEM_NAME "/factory"



#define semaphore_take(sem) \
    sem_wait(sem)
#define semaphore_take_now(sem) \
   sem_trywait(sem)

#define semaphore_give(sem) \
    sem_post(sem)

struct server_semaphores {
    sem_t *barber_sem, *barber_sleeps_sem, *waiting_room_sem, *factory_is_ready, *factory_sem;
};


struct Client_t {
    pid_t pid;
    short sem_no;
} typedef Client_t;

struct barber_shop_t {
    int first_person_index;
    int current_queue_length;
    int max_queue_length;
    Client_t barber_seat;
    Client_t queue[MAX_CLIENT_QUEUE_LEN];//shm[3]
} typedef barber_shop_t;

void printAndQuit(const char *str, ...);

void printWithTime(const char *format, ...);

sem_t *create_semaphore(char *name, unsigned int val);

sem_t *get_semaphore_handle(char *name);

int set_default_semaphore_values(int sem_id, int number_of_semaphores, short value);

void semaphore_wait_for_someone_to_take(sem_t *sem);
void barber_shop_init(barber_shop_t *barber_shop, int maxlen);
void print_client(const Client_t client);

void *attach_shm(int shm_id, size_t size);

int semaphore_take_with_opts(int semid, unsigned short sem_no, short wait_flags, struct timespec *ts);

int semaphore_wait_with_opts(int semid, unsigned short sem_no, short wait_flags);

//barber_shop_t get_barber_shop_interface_for_shm(void *shm_addr);
int check_server_semaphores(const struct server_semaphores *serverSemaphores);
int create_shm(size_t size);

//barber_shop_t setup_barber_shop_shm_interface(void *shm_addr, int number_of_chairs);

int waiting_room_is_full(const barber_shop_t *bs);

void seat_in_waiting_room(Client_t client, barber_shop_t *barber_shop);

char *generate_client_semaphore_name(int sem_no);

int close_server_semaphores(struct server_semaphores ss);

int unlink_server_semaphores();

void move_first_client_to_barber_chair(barber_shop_t *);

int waiting_room_is_empty(const barber_shop_t *bs);


void print_time();

void print_barber_shop(const barber_shop_t *const barber_shop);

void *get_and_attach_shm_for_barber_shop();

#define ZAD1_COMMON_H

#endif //ZAD1_COMMON_H
