//

// Created by jacko on 09.05.17.
//

#include <stdlib.h>
#include <memory.h>
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <unistd.h>
#include "common.h"



int is_mode_ok(int mode){
    switch (mode){
        case NO_ACTION:
            return 1;
        case MASK:
            return 1;
        case CATCH:
            return 1;
        default:
            return 0;
    }
}

int parse_and_check_args(int argc, char **argv,  int *thread_no, int *parent_mode,int * child_mode) {
    if (argc != 4)
        return EXIT_FAILURE;


    if (sscanf(argv[1], "%d", thread_no) != 1)
        return EXIT_FAILURE;
    if (sscanf(argv[2], "%d", parent_mode) != 1)
        return EXIT_FAILURE;
    if (sscanf(argv[3], "%d", child_mode) != 1)
        return EXIT_FAILURE;

    if(is_mode_ok(*parent_mode) && is_mode_ok(*child_mode)){
        return EXIT_SUCCESS;
    } else
        return EXIT_FAILURE;



}

void print_prompt(char *program_name) {
    printf("%s [thread_no] [parent_mode] [child_mode]\nmodes : [%d - NO_ACTION|%d - MASK |%d- CATCH", program_name,NO_ACTION,MASK,CATCH);
}


void cleaup_print_death(void *arg) {
    fprintf(stderr, "%li dies\n", *(pthread_t *) arg);
    fflush(stderr);
}

void cleanup_unlock_mutex(void *arg) {
    pthread_mutex_unlock((pthread_mutex_t *) arg);

}
void cancel_threads_except_given(pthread_t sender_tid,pthread_t* tids,int thread_no) {
    fprintf(stderr, "%li is killing\n", sender_tid);

    long int err = 0;
    for (int i = 0; i < thread_no; ++i) {
        if (tids[i] != sender_tid && tids[i] != 0) {
            err = pthread_cancel(tids[i]);
            if (err == 0) {}
            else {
                perror("Cancel error");
                printf("Error_id:%li ::: tid:%li\n", err, tids[i]);
            }
        }
    }
    return;


}