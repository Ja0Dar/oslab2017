#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include "common.h"

static void *thread_func(void *arg);

void set_signal_response(int mode);

int interactive_prompt();

int thread_no, parent_mode, child_mode;
pthread_t *tids;

int global_divisor = 1;

int main(int argc, char **argv) {
    printf("%d %d %d", NO_ACTION, MASK, CATCH);
    if (parse_and_check_args(argc, argv, &thread_no, &parent_mode, &child_mode) == EXIT_FAILURE) {
        print_prompt(argv[0]);
        exit(1);
    }

    tids = calloc((size_t) thread_no, sizeof(pthread_t));

    for (char i = 0; i < thread_no; i++) {
        if (pthread_create(tids + i, NULL, thread_func, (void *) i) != 0) {
            printf("Thread creation fails\n");
        } else {
            printf("Thread_created  %li\n", tids[i]);
        }
    }


    set_signal_response(parent_mode);
    while (interactive_prompt() == EXIT_SUCCESS);

    for (int j = 0; j < thread_no; ++j)pthread_cancel(tids[j]);

    for (int i = 0; i < thread_no; i++) {
        pthread_join(tids[i], NULL);
    }
    free(tids);
    return 0;
}

int interactive_prompt() {
    printf("Send signal to thread : \n"
                   "0:USR1\n"
                   "1:SIGTERM\n"
                   "2:SIGKILL\n"
                   "3:SIGSTOP\n"
                   "4: EXIT PROMPT\n"
                   "5 DIV BY zero\n");
    int response;
    if (scanf("%d", &response) != 1)
        exit(EXIT_SUCCESS);
    switch (response) {
        case 0:
            pthread_kill(tids[0], SIGUSR1);
            break;
        case 1:
            pthread_kill(tids[0], SIGTERM);
            break;
        case 2:
            pthread_kill(tids[0], SIGKILL);
            break;
        case 3:
            pthread_kill(tids[0], SIGSTOP);
            break;
        case 4:
            return EXIT_FAILURE;
        case 5:
            pthread_kill(tids[0], SIGUSR2);
            global_divisor=0;
            break;
        default:
            printf("Wrong resoonse\n");
    }
    return EXIT_SUCCESS;
}

void print_idis(int sig) {
    printf("pid: %d, tid: %li received signal \n", getpid(), pthread_self());
    fflush(stdout);
}

void div_by_zero(int a) {
    int i = 4;
    int y = 0;
    int x = i / y;
}

void set_signal_response(int mode) {
    sigset_t mask;
    signal(SIGUSR2, div_by_zero);
    switch (mode) {
        case NO_ACTION:
            return;
        case MASK:
            sigemptyset(&mask);
            sigaddset(&mask,SIGUSR1);
            sigaddset(&mask,SIGFPE);
            sigaddset(&mask,SIGTERM);
            sigaddset(&mask,SIGSTOP);//no block here
            sigaddset(&mask,SIGKILL);// no block here
            pthread_sigmask(SIG_BLOCK, &mask, NULL);

            break;
        case CATCH:
            signal(SIGTERM, print_idis);
            signal(SIGKILL, print_idis);//unblockable :)
            signal(SIGUSR1, print_idis);
            signal(SIGSTOP, print_idis);//unblockagbl
            signal(SIGFPE,print_idis);
            break;
        default:
            printf("Wrong mode\n");
            exit(1);

    }
}

static void *thread_func(void *arg) {
    set_signal_response(child_mode);
    pthread_t tid = pthread_self();
    pthread_cleanup_push(cleaup_print_death, &tid) ;
            int i;
            while (1) {
                pause();
//                sleep(1);
//                i=2/global_divisor;
            }


    pthread_cleanup_pop(0);
}
