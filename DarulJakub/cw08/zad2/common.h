//
// Created by jacko on 09.05.17.
//
/*
Stwórz trzy wersje programu.

 W wersji pierwszej, po odszukaniu słowa wątek anuluje asynchronicznie wszystkie pozostałe wątki i kończy pracę.

 W wersji drugiej wątek, który odszukał napis również anuluje pozostałe wątki, lecz anulowanie jest synchroniczne - punktem anulowania wątku jest zakończenie przetwarzania wczytanej ilości rekordów do danych prywatnych.

 W wersji trzeciej wszystkie wątki powinny być odłączone a warunkiem zakończenia wątku jest odczytanie wszystkich rekordów pliku.
*/
#ifndef ZAD1_COMMON_H

#define MEDIUM_BUF 255
#define RECORD_SIZE 1024

int parse_and_check_args(int argc, char **argv,  int *thread_no, int *parent_mode,int * child_mode);
void print_prompt(char* program_name);
void cleaup_print_death(void *arg);
void cleanup_unlock_mutex(void *arg);
enum masking{NO_ACTION,MASK,CATCH};


#define ZAD1_COMMON_H

#endif //ZAD1_COMMON_H
