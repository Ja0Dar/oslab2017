//
// Created by jacko on 09.05.17.
//
/*
Stwórz trzy wersje programu.

 W wersji pierwszej, po odszukaniu słowa wątek anuluje asynchronicznie wszystkie pozostałe wątki i kończy pracę.

 W wersji drugiej wątek, który odszukał napis również anuluje pozostałe wątki, lecz anulowanie jest synchroniczne - punktem anulowania wątku jest zakończenie przetwarzania wczytanej ilości rekordów do danych prywatnych.

 W wersji trzeciej wszystkie wątki powinny być odłączone a warunkiem zakończenia wątku jest odczytanie wszystkich rekordów pliku.
*/
#ifndef ZAD1_COMMON_H

#define FILE_WITH_RECORDS "../records.txt"
#define SOURCE_FOR_RECORDS "../source.txt"
#define MEDIUM_BUF 255
#define RECORD_SIZE 1024
#define TEXT_FIELD_SIZE RECORD_SIZE-sizeof(int)

int constains_word(char *haystack, const char *needle);
int parse_and_check_args(int argc, char **argv, char *filename, char *word, int *thread_no, int *blocks_to_read);
void print_prompt(char* program_name);
void cleaup_print_death(void *arg);
void cleanup_unlock_mutex(void *arg);
size_t read_bytes_sync(FILE *fp, char membuf[], size_t buf_size, pthread_mutex_t *mutex);
void cancel_threads_except_given(pthread_t sender_tid,pthread_t* tids,int thread_no);

struct record{
    int id;
    char text[RECORD_SIZE-sizeof(int)];
};

#define ZAD1_COMMON_H

#endif //ZAD1_COMMON_H
