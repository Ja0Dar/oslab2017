#include <stdio.h>
#include <stdlib.h>
#include "common.h"

int get_nr_of_records(int argc, char **argv);

//
// Created by jacko on 09.05.17.
//
int main(int argc, char **argv) {
    int rec_nr = get_nr_of_records(argc, argv);

    //todo errors?
    FILE *source = fopen(SOURCE_FOR_RECORDS, "r");
    FILE *dest = fopen(FILE_WITH_RECORDS, "w");

    size_t bufsize = 1024 - sizeof(int);

    char buf[bufsize];
    for (int i = 0; i < rec_nr; i++) {

        if(fread(buf, 1, bufsize, source)<bufsize){
            fseek(source,0L,SEEK_SET);
            if(fread(buf, 1, bufsize, source)<bufsize)
                printf("read error\n");
        }

        fwrite(&i, sizeof(int), 1, dest);//tODO - error handling
        fwrite(buf,1,bufsize,dest);


    }
    fclose(source);
    fclose(dest);
}

int get_nr_of_records(int argc, char **argv) {
    if (argc != 2) {
        printf("Should be 1 arg - number of records");
        exit(1);
    }
    int rec_nr;
    if (sscanf(argv[1], "%d", &rec_nr) != 1) {
        printf("Number of records should be int\n");
    }
    if (rec_nr <= 0) {
        printf("Record number should be positive\n");
        exit(1);
    }
    return rec_nr;
}
