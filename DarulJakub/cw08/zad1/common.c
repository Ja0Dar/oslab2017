//

// Created by jacko on 09.05.17.
//

#include <stdlib.h>
#include <memory.h>
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <asm/errno.h>
#include "common.h"


int constains_word(char *line, const char *word) {
    if (strstr(line, word) == NULL)
        return 0;
    return 1;
}

int parse_and_check_args(int argc, char **argv, char *filename, char *word, int *thread_no, int *blocks_to_read) {
    if (argc != 5)
        return EXIT_FAILURE;

    strcpy(filename, argv[1]);
    strcpy(word, argv[2]);

    if (sscanf(argv[3], "%d", thread_no) != 1)
        return EXIT_FAILURE;
    if (sscanf(argv[4], "%d", blocks_to_read) != 1)
        return EXIT_FAILURE;


    //check

    if (*blocks_to_read <= 0 || *thread_no <= 0) {
        printf("Blocks to read and thread number should be postive ints.\n");
        return EXIT_FAILURE;
    }

    if (*thread_no > 255) {
        printf("Thread_no should be <256 for my convenience\n");
        return EXIT_FAILURE;
    }

    if (access(filename, F_OK) < 0) {
        printf("File doesn't exist.\n");
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;


}

void print_prompt(char *program_name) {
    printf("%s [filename] [word] [thread_no] [records_to_read]\n", program_name);
}

size_t read_bytes_sync(FILE *fp, char membuf[], size_t buf_size, pthread_mutex_t *mutex) {
    pthread_mutex_lock(&mutex);
    size_t res = fread(membuf, 1, buf_size, fp);
    pthread_mutex_unlock(&mutex);
    return res;
}

void cleaup_print_death(void *arg) {
    fprintf(stderr, "%li dies\n", *(pthread_t *) arg);
    fflush(stderr);
}

void cleanup_unlock_mutex(void *arg) {
    pthread_mutex_unlock((pthread_mutex_t *) arg);

}
void cancel_threads_except_given(pthread_t sender_tid,pthread_t* tids,int thread_no) {
    fprintf(stderr, "%li is killing\n", sender_tid);

    long int err = 0;
    for (int i = 0; i < thread_no; ++i) {
        if (tids[i] != sender_tid && tids[i] != 0) {
            err = pthread_cancel(tids[i]);
            if (err == ESRCH)
                fprintf(stderr,"Cancelling not-alive thread ::: tid:%li\n", tids[i]);
        }
    }
    return;


}