#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <memory.h>
#include "common.h"
#include <pthread.h>

//W wersji drugiej wątek, który odszukał napis również anuluje pozostałe wątki, lecz anulowanie jest synchroniczne - punktem anulowania wątku jest zakończenie przetwarzania wczytanej ilości rekordów do danych prywatnych.
static void *thread_func(void *arg);


static pthread_mutex_t record_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t cancellation_mutex = PTHREAD_MUTEX_INITIALIZER;

FILE *fp;
char filename[MEDIUM_BUF], searched_word[MEDIUM_BUF];
int thread_no, records_to_read;
pthread_t *tids;

int word_was_found = 0;

int main(int argc, char **argv) {

    if (parse_and_check_args(argc, argv, filename, searched_word, &thread_no, &records_to_read) == EXIT_FAILURE) {
        print_prompt(argv[0]);
        exit(1);
    }


    fp = fopen(filename, "r");

    tids = calloc((size_t) thread_no, sizeof(pthread_t));

    for (char i = 0; i < thread_no; i++) {
        if (pthread_create(tids + i, NULL, thread_func, (void *) i) != 0)
            printf("Thread creation fails\n");
        else
            printf("Thread_created  %li\n", tids[i]);
    }

    for (int i = 0; i < thread_no; i++) {
        pthread_join(tids[i], NULL);
    }
    fclose(fp);
    free(tids);
    return 0;
}


static void *thread_func(void *arg) {
    pthread_t tid = pthread_self();
    pthread_cleanup_push(cleaup_print_death, &tid) ;
            if (word_was_found == 1)
                pthread_exit(EXIT_SUCCESS);


            size_t bytes_to_read = (size_t) RECORD_SIZE * records_to_read;
            char membuf[bytes_to_read];
            size_t no_of_read_bytes;
            struct record *records;
            while (1) {

                no_of_read_bytes = read_bytes_sync(fp, membuf, bytes_to_read, &record_mutex);

                if (no_of_read_bytes < bytes_to_read) {
                    printf("Too little reading - EOF\n");
//                    break;
                }
                records = (struct record *) membuf;

                pthread_testcancel(); /// RECEIVING cancels if pending
                for (int i = 0; i < records_to_read; ++i) {
                    if (constains_word(records[i].text, searched_word)) {
                        word_was_found = 1;
                        printf("Found word!:::::tid: %li, record_id: %d\n", tid, records[i].id);

                        pthread_mutex_lock(&cancellation_mutex);
                        pthread_cleanup_push(cleanup_unlock_mutex, &cancellation_mutex) ;
                                pthread_testcancel();
                                cancel_threads_except_given(tid, tids,thread_no);
                        pthread_cleanup_pop(1);//unlock mutex
                        pthread_exit(EXIT_SUCCESS);
                    }
                }
            }
            return NULL;
    pthread_cleanup_pop(0);
}


