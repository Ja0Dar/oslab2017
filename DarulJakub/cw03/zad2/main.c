#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/resource.h>
#include <memory.h>


#define SMALL_BUF 30
#define MEDIUM_BUF 255

#include "functions.h"

void processProg(char *line, unsigned int procTimeLimit, unsigned int memLimit);

void processEnv(char *);

int setHardLimit(enum __rlimit_resource resource, unsigned int limit);


void print_rusage(struct rusage *rusage);


int main(int argc, char **argv) {
    if (argc < 4) {
        printAndQuit("Too few args. Required :  [filename] [procTimeLimit] [memlimit]\n");
    }
    FILE *fp = fopen(argv[1], "r");
    unsigned int procTimeLimit, memoryLimit;
    sscanf(argv[2], "%u", &procTimeLimit);
    sscanf(argv[3], "%u", &memoryLimit);
    memoryLimit *= 1024 * 1024;// B-> kB->MB

    char *line = NULL;
    size_t len = 0;// len=0 in getline means "take everything to end of the line" 
    ssize_t read;

    if (fp == NULL)
        printAndQuit("Unable to open file");

    while ((read = getline(&line, &len, fp)) != -1) {

        if (line[0] == '#') {

            processEnv(line);
            printf("\n");
        } else if (line[0] != '-') {// comments begin with '-' 
            processProg(line, procTimeLimit, memoryLimit);
            printf("\n");
        }


    }
    free(line);

}


void processEnv(char *line) {
    char name[SMALL_BUF], value[SMALL_BUF];
    int read = (sscanf(line, "#%s %s", name, value));
    if (read < 1)
        printAndQuit("sscanf error in processEnv");
    else if (read == 1)
        if (unsetenv(name) < 0)
            printAndQuit("usetenv error in processEnv\n");
        else
            printf("===DELETE %s===\n", name);

    else if (setenv(name, value, 1/*replace*/) < 0)
        printAndQuit("setenv error in processEnv\n");
    else
        printf("===CHANGE %s to %s===\n", name, value);


}

void processProg(char *org, unsigned int procTimeLimit, unsigned int memLimit) {
    char line[MEDIUM_BUF];
    sscanf(org, "%[^\n]\n", line);
    printf("===FORK: %s===\n", line);

    char *buf[SMALL_BUF];
    for (int i = 0; i < SMALL_BUF; ++i)buf[i] = NULL;

    strSplit(strdup(line), buf, " ");
    int pid;
    if ((pid = fork()) < 0)
        printAndQuit("processProg: fork error");
    else if (pid == 0) {

        //SET limits
        if (setHardLimit(RLIMIT_CPU, procTimeLimit) < 0) {
            perror("Couldn't set hard limit for cpu.");
        };

        if (setHardLimit(RLIMIT_AS, memLimit) < 0) {
            perror("Couldn't set hard limit for virtual memory");
        }

        char *paths[SMALL_BUF];
        for (int i = 0; i < SMALL_BUF; ++i) paths[i] = NULL;

        char path[MEDIUM_BUF];
        strcpy(path, getenv("PATH"));

        strSplit(strdup((const char *) path), paths, ":");


        //Inserting current path to paths Array
        int nullI = 0;
        while (paths[nullI] != NULL)nullI++;
        paths[nullI] = malloc(MEDIUM_BUF);

	//Adding cwd to paths Array in order to use it when searching for exec's program
        getcwd(paths[nullI], sizeof(path));

        for (int i = 0; paths[i] != NULL; i++) {

            strcpy(path, paths[i]);
            strcat(path, "/");
            strcat(path, buf[0]);


            execve(path, buf, __environ);
        }

        strSplittedFree(paths);
        strSplittedFree(buf);
//        execlp("/bin/echo", "echo", "Bad fork, baad\n", NULL);
        //Why it needs to be _exit?
        printf("Couldn't find a program.\n");
        _exit(0);
    }


    struct rusage usage;
    int retState = 0;
    if (wait3(&retState, 0, &usage) < 0) {
        printAndQuit("\nWait forpid errror\n");
    }

    print_rusage(&usage);


    if (WEXITSTATUS(retState) != 0)
        printAndQuit(" Process  %s returned with %d status. \n", buf[0], WEXITSTATUS(retState));

}


int setHardLimit(enum __rlimit_resource resource, unsigned int limit) {
    struct rlimit rl;

    rl.rlim_cur = (unsigned int) 3 * limit / 4;
    rl.rlim_max = limit;

    if (setrlimit(resource, &rl) < 0)
        return -1;

    return 1;


}


void print_rusage(struct rusage *ru) {
    printf("Time:\nSys: %ld.%ld  User:%ld.%ld\n", ru->ru_stime.tv_sec, ru->ru_stime.tv_usec, ru->ru_utime.tv_sec,
           ru->ru_utime.tv_usec);
}
//void printLimit(char *string, struct rlimit rl) {
//    printf("%s   | soft: ", string);
//    if (RLIM_INFINITY == rl.rlim_cur) {
//        printf("INFINITY");
//    } else
//        printf("%li", rl.rlim_cur);
//
//    printf(" hard: ");
//    if (RLIM_INFINITY == rl.rlim_max) {
//        printf("INFINITY");
//    } else
//        printf("%li", rl.rlim_max);
//
//    printf("\n");
//}

