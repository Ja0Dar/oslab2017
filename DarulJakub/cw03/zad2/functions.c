
#include <memory.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>



void strSplit(char *str, char *res[], const char *sep) {
    int i = 0;
    while ((res[i] = strsep(&str, sep)))i++;
    res[i] = NULL;

}

void strSplittedFree(char *tab[]) {

    size_t len;
    int j;
    for (j = 0; tab[j + 1] != NULL; ++j) {
        len = strlen(tab[j]);
	//replace \0 with anything ('X' in this case')
        tab[j][len] = 'X';
    }
    free((char *) tab[0]);


}


void printAndQuit(const char *str,...) {
    va_list args;
    va_start (args, str);
    vfprintf(stderr,str,args);
    va_end (args);
    exit(1);
}
