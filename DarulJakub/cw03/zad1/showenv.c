
#include <stdio.h>
#include <stdlib.h>

int main(int argc,char ** argv){
    if (argc<1){
        printf("\nshowenv Should have at least 1 arg\n");
    }
    printf("|%s| :\n",argv[0]);
    char* env_variable;
    for (int i = 1; i < argc; ++i) {
        env_variable =getenv(argv[i]);

        if(env_variable==NULL)
            printf("Couldn't  find variable %s\n", argv[i]);
        else
            printf("%s  =   %s\n",argv[i],env_variable);
    }








}
