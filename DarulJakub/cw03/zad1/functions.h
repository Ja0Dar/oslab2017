
#ifndef ZAD1_FUNCTIONS_H


void printAndQuit(const char *,...);
void strSplit(char *str, char *res[], const char *sep);
void strSplittedFree(char *tab[]);

#define ZAD1_FUNCTIONS_H

#endif //ZAD1_FUNCTIONS_H
