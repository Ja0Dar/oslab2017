#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <memory.h>
#include <stdarg.h>
#define SMALL_BUF 30
#define MEDIUM_BUF 255

#include "functions.h"

void processEnvironmentalVariable(char *);
void executeProgram(char *line);

int main(int argc, char **argv) {
    if (argc < 2) {
        printAndQuit("Too few args. Required :  [filename]%s\n",argv[0]);
    }

    FILE *fp = fopen(argv[1], "r");

    char *line = NULL;
    size_t len = 0;// to endof line
    ssize_t read;

    if (fp == NULL)
        printAndQuit("Unable to open file");

    while ((read = getline(&line, &len, fp)) != -1) {

        if (line[0] == '#') {

            processEnvironmentalVariable(line);
        } else if (line[0] != '-') {// comments
            executeProgram(line);
        }


    }
    free(line);


}



void executeProgram(char *org) {
    char line[MEDIUM_BUF];
    sscanf(org, "%[^\n]\n", line);
    printf("===FORK: %s===\n",line);

    char *buf[SMALL_BUF];
    for (int i = 0; i < SMALL_BUF; ++i)buf[i] = NULL;

    strSplit(strdup(line), buf, " ");
    int pid;
    if ((pid = fork()) < 0)
        printAndQuit("executeProgram: fork error");
    else if (pid == 0) {

        char *paths[SMALL_BUF];
        for (int i = 0; i < SMALL_BUF; ++i) paths[i] = NULL;

        char path[MEDIUM_BUF];
        strcpy(path, getenv("PATH"));

        strSplit(strdup((const char *) path), paths, ":");


        //Inserting current path to paths
        int nullI=0;
        while (paths[nullI]!=NULL)nullI++;
        paths[nullI]=malloc(MEDIUM_BUF);
        getcwd(paths[nullI],sizeof(path));

        for (int i = 0; paths[i] != NULL; i++) {

            strcpy(path,paths[i]);
            strcat(path, "/");
            strcat(path, buf[0]);


            execve(path, buf, __environ);
        }

        strSplittedFree(paths);
        strSplittedFree(buf);
        execlp("/bin/echo","echo","Bad fork, baad\n",NULL);
//       __exit(1);
    }


    int retState=0;
    if(waitpid(pid, &retState, 0)<0){
        printAndQuit("\nWait forpid errror\n");
    } else if(retState!=0)
        printAndQuit(" Process  %s returned with %d status. \n",buf[0],WEXITSTATUS(retState));
}


void processEnvironmentalVariable(char *line) {
    char name[SMALL_BUF], value[SMALL_BUF];
    int read = (sscanf(line, "#%s %s", name, value));
    if (read < 1)
        printAndQuit("sscanf error in processEnvironmentalVariable");
    else if (read == 1)
        if (unsetenv(name) < 0)
            printAndQuit("usetenv error in processEnvironmentalVariable\n");
        else
            printf("===DELETE %s===\n", name);

    else if (setenv(name, value, 1/*replace*/) < 0)
        printAndQuit("setenv error in processEnvironmentalVariable\n");
    else
        printf("===CHANGE %s to %s===\n", name, value);


}

