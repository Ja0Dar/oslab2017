def main():
    import sys
    file="main.cpp"
    if len(sys.argv) > 1:
        writePointers(file)
    else:
        writeHeaders(file)


def trimLine(line):
    try:
        line = line[0:line.index("//")]
    except ValueError:
        pass

    try:
        line = line[0:line.index("#")]
    except ValueError:
        pass
    return line


def writeHeaders(file):
    file = open(file)
    output = open('a.txt', 'w')
    openBrackets = 0  # right bracket counter

    for line in file.readlines():

        line = trimLine(line)
        if openBrackets == 0:  # and "{" in line:
            output.write(line.replace(" {", ";").replace("{", ";"))

        openBrackets += line.count("{")
        openBrackets -= line.count("}")
    output.close()


def writePointers(file):
    file = open(file)
    output = open('a.txt', 'w')
    openBrackets = 0  # right bracket counter

    for line in file.readlines():

        line = trimLine(line)
        if openBrackets == 0:  # and "{" in line:
            output.write(line.replace(" {", ";").replace("{", ";"))

        openBrackets += line.count("{")
        openBrackets -= line.count("}")
    output.close()


if __name__ == "__main__":
    main()
else:
    print("Not main")
