


#include <time.h>
#include "functions.h"
void printWithTime(const char *format, ...) {
    static struct timespec ts;

    clock_gettime(CLOCK_MONOTONIC, &ts);

    printf("%li.%li ::: ", ts.tv_sec, ts.tv_nsec);
    va_list args;
    va_start (args, format);
    vprintf(format, args);
    va_end (args);
}

void parse_args(int argc, char **argv, int *readers_no, int *writers_no, bool *print_additional_info) {
    if (argc != 3 && argc != 4) {
        prompt(argv[0]);
        exit(1);
    }
    *readers_no = atoi(argv[1]);
    *writers_no = atoi(argv[2]);

    if(*readers_no<0 || *writers_no<0)
        printAndQuit("readers_no and writer_no shouldnt be negative\n");

    if (argc == 4) {
        if (strcmp(argv[3], "-i") == 0) {
            *print_additional_info = true;
        }else{
            prompt(argv[0]);
            exit(1);
        }
    } else
        *print_additional_info = false;


}


void prompt(const char *filename) {
    printf("Usage : %s readers_no writers_no [-i]\n", filename);
}

void printAndQuit(const char *str, ...) {
    va_list args;
    va_start (args, str);
    vfprintf(stderr, str, args);
    va_end (args);

    fprintf(stderr, "\n%s\n", strerror(errno));
    exit(1);
}


sem_t *my_semaphore_init(char *name) {
    sem_t *res = NULL;
    //sem_init(*sem, 0- threads |other - processes, value)
    if (sem_init(res, 0, 1) < 0) return NULL;

    return res;
}
/// http://stackoverflow.com/a/5386266

void ticket_lock(ticket_lock_t *ticket)
{
    unsigned long queue_me;

    pthread_mutex_lock(&ticket->mutex);
    queue_me = ticket->queue_tail++;
    while (queue_me != ticket->queue_head)
    {
        pthread_cond_wait(&ticket->cond, &ticket->mutex);
    }
    pthread_mutex_unlock(&ticket->mutex);
}

void ticket_unlock(ticket_lock_t *ticket)
{
    pthread_mutex_lock(&ticket->mutex);
    ticket->queue_head++;
    pthread_cond_broadcast(&ticket->cond);
    pthread_mutex_unlock(&ticket->mutex);
}

///
