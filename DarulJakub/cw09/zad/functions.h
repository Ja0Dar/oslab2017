//
// Created by jacko on 17.05.17.
//

#ifndef ZADA_FUNCTIONS_H

#include <fcntl.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <semaphore.h>
#include <stdbool.h>
#include <pthread.h>

#define  ARR_SIZE 3
#define  MAX_ACCESSES_PER_THREAD 10
#define semaphore_take(sem) \
    sem_wait(sem)
#define semaphore_take_now(sem) \
   sem_trywait(sem)

#define semaphore_give(sem) \
    sem_post(sem)

void printAndQuit(const char *str, ...);
void printWithTime(const char *format, ...) ;
void prompt(const char * filename);
void parse_args(int argc, char **argv, int *readers_no, int *writers_no, bool *print_additional_info) ;


///http://stackoverflow.com/a/5386266
typedef struct ticket_lock {
    pthread_cond_t cond;
    pthread_mutex_t mutex;
    unsigned long queue_head, queue_tail;
} ticket_lock_t;

#define TICKET_LOCK_INITIALIZER { PTHREAD_COND_INITIALIZER, PTHREAD_MUTEX_INITIALIZER}

void ticket_lock(ticket_lock_t *ticket);
void ticket_unlock(ticket_lock_t *ticket);


///end
#define ZADA_FUNCTIONS_H

#endif //ZADA_FUNCTIONS_H
