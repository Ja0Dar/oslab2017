cmake_minimum_required(VERSION 3.6)
project(cw09)

set(CMAKE_C_STANDARD 99)

set(SOURCE_FILES zada.c functions.c functions.h)
add_executable(zada.out ${SOURCE_FILES})
TARGET_LINK_LIBRARIES(zada.out pthread rt)

set(ZADB zadb.c functions.c functions.h)
add_executable(zadb.out ${ZADB})
TARGET_LINK_LIBRARIES(zadb.out pthread rt)
