#include <stdio.h>
#include <malloc.h>
#include <stdlib.h>
#include <stdbool.h>
#include <pthread.h>
#include "functions.h"

//a) rozwiązanie wykorzystujące do synchronizacji semafory nienazwane standardu POSIX (zgodne z typem rozwiązań problemu współbieżnego stosującego semafory)

int
generate_arrays(int *A, int *reader_count, sem_t *reader_count_sem, sem_t *access_to_A_semaphores, size_t size,
                int lower_bound,
                int higher_bound);

void *writer_stuff(void *arg);

void *reader_stuff(void *arg);

sem_t *counter_sem = NULL, *resource_sem = NULL;
int *A = NULL, *access_counter = NULL;

bool printAdditionalInfo = false;

int main(int argc, char **argv) {
    int readers_no = 10, writers_no = 10;
    parse_args(argc, argv, &readers_no, &writers_no, &printAdditionalInfo);
    A = calloc(ARR_SIZE, sizeof(int));
    access_counter = calloc(ARR_SIZE, sizeof(int));
    resource_sem = calloc(ARR_SIZE, sizeof(sem_t));
    counter_sem = calloc(ARR_SIZE, sizeof(sem_t));
    generate_arrays(A, access_counter, counter_sem, resource_sem, ARR_SIZE, 0, 1000);


    pthread_attr_t pthread_attr;
    pthread_attr_init(&pthread_attr);
    pthread_attr_setdetachstate(&pthread_attr, PTHREAD_CREATE_DETACHED);

    pthread_t trash;

    //readers
    for (int w_id = 0; w_id < readers_no; w_id++) {
        if (pthread_create(&trash, &pthread_attr, reader_stuff, (void *) 1+(rand() % 10)) != 0)
            printAndQuit("Cannot create reader\n");
    }
    for (int w_id = 0; w_id < writers_no; w_id++) {
        if (pthread_create(&trash, &pthread_attr, writer_stuff, NULL) != 0)
            printAndQuit("Cannot create writer\n");
    }

    //exit if no kids
    pthread_exit(0);
}

void *writer_stuff(void *arg) {
// pisarz raportuje na standardowym wyjściu wykonanie swojej operacji (modyfikacji tablicy),
// w wersji opisowej programu (opcja -i) wypisuje indeks i wpisaną wartość
    pthread_t tid = pthread_self();
//    int changes_no = rand() % MAX_ACCESSES_PER_THREAD;
    int random_index;
    int new_val, prev_val;

    int forever = 1;
    while (forever) {


//        for (int i = 0; i < changes_no; ++i) {
        random_index = rand() % ARR_SIZE;
        new_val = rand();

        ///lock
        if (sem_wait(&resource_sem[random_index]) < 0)printAndQuit("Sem_wait errror\n");

        prev_val = A[random_index];
        A[random_index] = new_val;

        ///unlock
        if (sem_post(&resource_sem[random_index]) < 0)printAndQuit("Sem_wait errror\n");

        if (printAdditionalInfo) {
            printWithTime("writer::%li::  index : %d   %d -> %d\n", tid, random_index, prev_val, new_val);
        } else {
            printWithTime("writer::%li::index : %d \n ", tid, random_index);
        }

//        }
    }
    pthread_exit(0);
}


void *reader_stuff(void *arg) {
    int divisor = (int) arg;
    pthread_t tid = pthread_self();

//    int read_no = rand() % MAX_ACCESSES_PER_THREAD;
    int val;
    int forever = 1;
    int divisible_count;
    while (forever) {
        divisible_count = 0;
        for (int i = 0; i < ARR_SIZE; ++i) {


            //take  counter sem
            sem_wait(&counter_sem[i]);

            access_counter[i]++;

            if (access_counter[i] == 1)
                sem_wait(&resource_sem[i]);
            //give counter sem
            sem_post(&counter_sem[i]);

            val = A[i];

            //take counter sem
            sem_wait(&counter_sem[i]);
            access_counter[i]--;

            if (access_counter[i] == 0)
                sem_post(&resource_sem[i]);

            sem_post(&counter_sem[i]);

            if (val % divisor == 0) {
                divisible_count++;
                if (printAdditionalInfo)
                    printf("reader:: %li  read %d %d ==0 \n", tid, val, divisor);
            }

            if (printAdditionalInfo)
                printWithTime("reader %li read A[%d] val : %d\n", tid, i, val);
            else
                printWithTime("reader %li read A[%d] \n", tid, i);
        }
        printWithTime("reader after arr:%li ::  %d ints divisible by %d \n", tid, divisible_count, divisor);
    }
}


int
generate_arrays(int *A, int *reader_count, sem_t *reader_count_sem, sem_t *access_to_A_semaphores, size_t size,
                int lower_bound,
                int higher_bound) {

//    A = calloc(size, sizeof(int));
//    counter_sem= calloc(size,sizeof(sem_t));
    if (A == NULL || reader_count_sem == NULL) return -1;

    for (int i = 0; i < size; ++i) {
        reader_count[i] = 0;
        A[i] = lower_bound + (int) lrand48() % (higher_bound - lower_bound);
        sem_init(reader_count_sem + i, 1, 1);// 0 - semaphore for threads (otherwise - processes)
        sem_init(access_to_A_semaphores + i, 1, 1);// 0 - semaphore for threads (otherwise - processes)
    }

    return 0;
}
