//
// Created by jacko on 19.03.17.
//
#include "main.h"
void sort_sys(struct file_details args) {
    int filedesc=open(args.filename,O_RDWR);// file exists and we can update it

    if(filedesc<0)
        printAndQuit("Cannot open file");

    char* prev=malloc(args.record_size);
    char* next=malloc(args.record_size);

    if(prev==NULL || next==NULL)
        printAndQuit("Cannot allocate memory");

    bool sorted=false;
    for(size_t i=args.number_of_records-1 ;i>0 && !sorted ;--i){
        sorted=true;
        //go to beginning
        if(lseek(filedesc,0,SEEK_SET)<0)printAndQuit("Lseek failed");


        read(filedesc,prev,args.record_size);
        for(size_t j=0; j<i;j++){

            read(filedesc,next,args.record_size);
            if(prev[0]> next[0]){
                lseek(filedesc,-2*args.record_size,SEEK_CUR);// go back two records
                write(filedesc,next,args.record_size);   //write next(smaller)
                write(filedesc,prev,args.record_size);   //write prev(bigger)
                sorted=false;


                //We want to have prev on the right side so..
                //[...,next,prev,(futurenext)..]

            } else{
                // now we have [...prev,next,future_next]
                //so in order to have prev pointing to max(prev,next) we swap pointers
                swapCharPtr(&prev,&next);
            }
        }
    }
    free(prev);
    free(next);
    close(filedesc);
}


void shuffle_sys(struct file_details args){
    int file=open(args.filename,O_RDWR);
    if(file<0)
        printAndQuit("Couldnt open file");

//    -- To shuffle an array a of n elements (indices 0..n-1):
//    for i from 0 to n−2 do
//        j ← random integer such that i ≤ j < n
//    exchange a[i] and a[j]
    char* i_val=malloc(args.record_size);
    char* j_val=malloc(args.record_size);



    long j,n=args.number_of_records;
    for (long i = 0; i < n-1; i++) {
        j= (int) (i + lrand48() % (n - i - 1));

        ///SWap
        //Read ith record to i_val
        lseek(file,i*args.record_size,SEEK_SET);
        read(file,i_val,args.record_size);

        //Read jth record to j_val
        lseek(file,j*args.record_size,SEEK_SET);
        read(file,j_val,args.record_size);

        //Write i_val to jth record
        lseek(file,j*args.record_size,SEEK_SET);
        write(file,i_val,args.record_size);

        //Write j_val to ith record
        lseek(file,i*args.record_size,SEEK_SET);
        write(file,j_val,args.record_size);

    }
    close(file);
    free(i_val);
    free(j_val);
}