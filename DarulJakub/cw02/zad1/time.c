//
// Created by jacko on 19.03.17.
//


#include "main.h"
timeTuple getTimeDiff(struct tms *prevTimes, clock_t *prevReal) {
    timeTuple res;

    struct tms now;
    times(&now);
    clock_t nowReal = clock();
    if (prevTimes->tms_stime == -1) {
        res.sys = res.real = res.user = -1.0;
    } else {
        res.real = ((double) (nowReal - *(prevReal))) / CLOCKS_PER_SEC;
        res.sys = ((double) (now.tms_stime - prevTimes->tms_stime)) / CLK;
        res.user = ((double) (now.tms_utime - prevTimes->tms_utime)) / CLK;
    }
    *prevReal = nowReal;
    *prevTimes = now;
    return res;
}
void printTimeTuple(timeTuple t) {
    printf("Real:  %f    User:   %f    Sys:     %f \n", t.real, t.user, t.sys);
}