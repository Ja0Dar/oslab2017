//
// Created by jacko on 19.03.17.
//

#ifndef ZAD1_MAIN_H

#define _XOPEN_SOURCE 500
#define CLK sysconf(_SC_CLK_TCK)

#define SYS 22
#define LIB 21
#define SHUFFLE 13
#define GENERATE 15
#define SORT 43

#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <stdbool.h>
#include <fcntl.h>
#include <unistd.h>

#include <time.h>
#include <sys/times.h>
#define RANDOM_SOURCE "/dev/urandom"


///STRUCTS


struct file_details {
    char *filename;
    size_t record_size;
    size_t number_of_records;
};
struct myArgs {
    struct file_details fd;
    short mode;
    short operation;
};


typedef struct timeTuple {
    double real, user, sys;
} timeTuple;


///DECLARATIONS

timeTuple getTimeDiff(struct tms *prevTimes, clock_t *prevReal);
void printTimeTuple(timeTuple t);

void printAndQuit(const char *str);
struct myArgs parseArgs(int argc, char **argv);
void swapCharPtr(char **a, char **b);




void sort_sys(struct file_details args);
void shuffle_sys(struct file_details args);

void generate(struct file_details args);
void sort_lib(struct file_details args);
void shuffle_lib(struct file_details args);

#define ZAD1_MAIN_H

#endif //ZAD1_MAIN_H




