#include "main.h"




void generate(struct file_details args) {
    //TODO what is the deal about safe_calloc?!


    char ** buff=calloc(args.number_of_records,args.record_size);
    FILE * randSource=fopen(RANDOM_SOURCE,"rb");
    fread(buff,args.record_size,args.number_of_records,randSource);
    fclose(randSource);

    FILE * output = fopen(args.filename,"wb");
    fwrite(buff,args.record_size,args.number_of_records,output);
    fclose(output);
    free(buff);
}

void sort_lib(struct file_details args) {
    FILE* file=fopen(args.filename,"r+");// file exists and we can update it

    char* prev=malloc(args.record_size);
    char* next=malloc(args.record_size);


    bool sorted=false;
    for(size_t i=args.number_of_records-1 ;i>0 && !sorted ;--i){
        sorted=true;
        fseek(file,0,SEEK_SET);//fseek to beg


        fread(prev,args.record_size,1,file);
        for(size_t j=0; j<i;j++){

            fread(next,args.record_size,1,file);
            if(prev[0]> next[0]){
                fseek(file,-2*args.record_size,SEEK_CUR);// go back two records
                fwrite(next,args.record_size,1,file);   //write next(smaller)
                fwrite(prev,args.record_size,1,file);   //write prev(bigger)
                sorted=false;


                //We want to have prev on the right side so..
                //[...,next,prev,(futurenext)..]

            } else{
                // now we have [...prev,next,future_next]
                //so in order to have prev pointing to max(prev,next) we swap pointers
                swapCharPtr(&prev,&next);
            }
        }
    }
    free(prev);
    free(next);
    fclose(file);
}



void shuffle_lib(struct file_details args){
    FILE* file=fopen(args.filename,"r+");


//    -- To shuffle an array a of n elements (indices 0..n-1):
//    for i from 0 to n−2 do
//        j ← random integer such that i ≤ j < n
//    exchange a[i] and a[j]
    char* i_val=malloc(args.record_size);
    char* j_val=malloc(args.record_size);

    if(j_val==NULL || i_val==NULL)
        printAndQuit("Cannot allocate memory");


    long j,n=args.number_of_records;
    for (long i = 0; i < n-1; i++) {
        j= (int) (i + lrand48() % (n - i - 1));

        ///SWap
        //Read ith record to i_val
        fseek(file,i*args.record_size,SEEK_SET);
        fread(i_val,args.record_size,1,file);

        //Read jth record to j_val
        fseek(file,j*args.record_size,SEEK_SET);
        fread(j_val,args.record_size,1,file);

        //Write i_val to jth record
        fseek(file,j*args.record_size,SEEK_SET);
        fwrite(i_val,args.record_size,1,file);

        //Write j_val to ith record
        fseek(file,i*args.record_size,SEEK_SET);
        fwrite(j_val,args.record_size,1,file);

    }
    fclose(file);
    free(i_val);
    free(j_val);
}

