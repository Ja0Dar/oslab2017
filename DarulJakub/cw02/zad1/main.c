#include "main.h"


///MAIN####################################
int main(int argc, char **argv) {


    struct myArgs args = parseArgs(argc, argv);

    void (*fun)(struct file_details);

    switch (args.operation) {
        case GENERATE:
            fun = generate;
            break;
        case SHUFFLE
            :
            switch (args.mode) {
                case SYS:
                    fun = shuffle_sys;
                    break;
                case LIB:
                    fun = shuffle_lib;
                    break;
                default:
                    printAndQuit("Something went wrong in parsing args - LIB/SYS");
            }
            break;
        case SORT:
            switch (args.mode) {
                case SYS:
                    fun = sort_sys;
                    break;
                case LIB:
                    fun = sort_lib;
                    break;
                default:
                    printAndQuit("Something went wrong in parsing args - LIB/SYS");
            }
            break;
        default:
            printAndQuit("Something went wrong in parsing args - SORT/SHUFFLE/GENERATE");

    }
    printf("%s %s record_size:%li  number_of_blocks:%li\n", argv[1]/*mode*/, argv[2]/*operation*/, args.fd.record_size,
           args.fd.number_of_records);

    struct tms prevTimes;
    clock_t prevReal;
    prevTimes.tms_stime = -1;
    timeTuple tmpTT;

    getTimeDiff(&prevTimes, &prevReal);
    (*fun)(args.fd);
    tmpTT = getTimeDiff(&prevTimes, &prevReal);
    printTimeTuple(tmpTT);

}///#####################################




///DEFINITONS
struct myArgs parseArgs(int argc, char **argv) {
    if (argc < 5) {
        printAndQuit("[mode] [operation] [Filename] [Record size]  [number of records]   required");
    }

    struct myArgs res;
    struct file_details fd;
    ///mode
    if (strcmp(argv[1], "sys") == 0)
        res.mode = SYS;
    else if (strcmp(argv[1], "lib") == 0)
        res.mode = LIB;
    else
        printAndQuit("Wrong mode. It should be [lib|sys]");


    //operation
    if (strcmp(argv[2], "generate") == 0)
        res.operation = GENERATE;
    else if (strcmp(argv[2], "shuffle") == 0)
        res.operation = SHUFFLE;
    else if (strcmp(argv[2], "sort") == 0)
        res.operation = SORT;
    else
        printAndQuit("operation should be [sort|shuffle|generate");


    fd.filename = argv[3];
    if (sscanf(argv[4], "%lu", &fd.record_size) != 1) {
        printAndQuit("Record size is not a number");
    }
    if (sscanf(argv[5], "%lu", &fd.number_of_records) != 1) {
        printAndQuit("Number of record is not a number");
    }
    res.fd = fd;
    return res;
}


void printAndQuit(const char *str) {
    printf("%s",str);
    exit(1);
}


void swapCharPtr(char **a, char **b) {
    char *tmp;
    tmp = *a;
    *a = *b;
    *b = tmp;

}
