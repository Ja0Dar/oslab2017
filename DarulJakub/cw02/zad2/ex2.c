#define _XOPEN_SOURCE 500
#include <stdio.h>
#include <memory.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

void browseTree(char *path, long long int maxSize);

void printStats(char *dirpath, char *filename, struct stat stats);

char const *sperm(mode_t mode);


int main(int argc, char **argv) {
    if (argc < 2) {
        printf("There should be at least 2 args: path, filesizeLimit");
        return 1;
    }
    long requiredFileSizeLimit;
    char path[80];


    strcpy(path, argv[1]);
    sscanf(argv[2], "%li", &requiredFileSizeLimit);
    struct stat s;
    lstat(path,&s);
    if((S_ISDIR(s.st_mode)))
        browseTree(path, requiredFileSizeLimit);
    else{
        printf("%s is not a dir.",path);
    }
}

void browseTree(char *path, long long int maxSize) {
    char prevPath[1024];
    char currentPath[1024];

    // full path
    getcwd(prevPath, sizeof(prevPath));
    chdir(path);
    getcwd(currentPath, sizeof(currentPath));

    DIR *directory = opendir(".");
    struct dirent *entry;
    struct stat stats;
    while (directory/* is not fully browsed*/) {

        if ((entry = readdir(directory)) != NULL) {

            if (strcmp(entry->d_name, "..") == 0 || strcmp(entry->d_name, ".") == 0)
                continue;//  Skip current and parent dir.

            if(lstat(entry->d_name, &stats)<0){
                printf("error : lstat couldnt get entries for %s\n",entry->d_name);
            }else{
                if (S_ISDIR(stats.st_mode))browseTree(entry->d_name, maxSize);
                else if (S_ISREG(stats.st_mode) && stats.st_size <= maxSize) {
                    printStats(currentPath, entry->d_name, stats);
                }

            }




        } else {
            if (errno == 0) {
                closedir(directory);
                break;//not found
            }
            closedir(directory);
            break; // read error
        }
    }
    chdir(prevPath);
    return;//open error
}

void printStats(char *dirpath, char *filename, struct stat stats) {
    char modTime[20];
    strftime(modTime, 20, "%Y-%m-%d %H:%M:%S", localtime(&stats.st_mtime));

    printf("%s/%s %lld %s %s\n", dirpath, filename, (long long int) stats.st_size, sperm(stats.st_mode), modTime);
}

char const *sperm(mode_t mode) {
    static char local_buff[16]; //= {0};
    int i = 0;
    // user permissions
    if ((mode & S_IRUSR) == S_IRUSR) local_buff[i] = 'r';
    else local_buff[i] = '-';
    i++;
    if ((mode & S_IWUSR) == S_IWUSR) local_buff[i] = 'w';
    else local_buff[i] = '-';
    i++;
    if ((mode & S_IXUSR) == S_IXUSR) local_buff[i] = 'x';
    else local_buff[i] = '-';
    i++;
    // group permissions
    if ((mode & S_IRGRP) == S_IRGRP) local_buff[i] = 'r';
    else local_buff[i] = '-';
    i++;
    if ((mode & S_IWGRP) == S_IWGRP) local_buff[i] = 'w';
    else local_buff[i] = '-';
    i++;
    if ((mode & S_IXGRP) == S_IXGRP) local_buff[i] = 'x';
    else local_buff[i] = '-';
    i++;
    // other permissions
    if ((mode & S_IROTH) == S_IROTH) local_buff[i] = 'r';
    else local_buff[i] = '-';
    i++;
    if ((mode & S_IWOTH) == S_IWOTH) local_buff[i] = 'w';
    else local_buff[i] = '-';
    i++;
    if ((mode & S_IXOTH) == S_IXOTH) local_buff[i] = 'x';
    else local_buff[i] = '-';
    return local_buff;
}
