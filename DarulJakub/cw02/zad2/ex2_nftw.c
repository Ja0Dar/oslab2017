#define _XOPEN_SOURCE 500


#include <stdio.h>
#include <memory.h>
#include <sys/types.h>
#include <ftw.h>
#include <sys/stat.h>
#include <unistd.h>
#include <time.h>
//#include <errno.h>
#include <libgen.h>

#include <dirent.h>

struct FTW;
void printStats(char *dirpath, char *filename, struct stat stats);
char const *sperm(mode_t mode);
void browseTree_nftw(char path[80], long limit);
int callback(const char * path, const struct stat * stats, int type, struct FTW *  ftw);

/// Global
long requiredFileSizeLimit;

int main(int argc, char **argv) {

    if (argc < 2) {
        printf("There should be at least 2 args: path, filesizeLimit");
        return 1;
    }
    char path[80];


    strcpy(path, argv[1]);
    sscanf(argv[2], "%li", &requiredFileSizeLimit);
    struct stat s;
    lstat(path, &s);
    if ((S_ISDIR(s.st_mode)))
        browseTree_nftw(path, requiredFileSizeLimit);
    else {
        printf("%s is not a dir.", path);
    }
}


void browseTree_nftw(char path[80], long limit) {
    nftw(path,callback,32/*descriptor limit*/, FTW_CHDIR|FTW_PHYS);

}


int callback(const char * path, const struct stat * stats, int type, struct FTW *  ftw){
    char cwd[1024];
    char *filename;
    filename = basename((char*)path);
    getcwd(cwd,sizeof(cwd));// get current working directory

    if(S_ISREG(stats->st_mode) && stats->st_size<=requiredFileSizeLimit)
        printStats(cwd,filename,*stats);

    return 0;
}



void printStats(char *dirpath, char *filename, struct stat stats) {
    char modTime[20];
    strftime(modTime, 20, "%Y-%m-%d %H:%M:%S", localtime(&stats.st_mtime));

    printf("%s/%s %lld %s %s\n", dirpath, filename, (long long int) stats.st_size, sperm(stats.st_mode), modTime);
}

char const *sperm(mode_t mode) {
    static char local_buff[16]; //= {0};
    int i = 0;
    // user permissions
    if ((mode & S_IRUSR) == S_IRUSR) local_buff[i] = 'r';
    else local_buff[i] = '-';
    i++;
    if ((mode & S_IWUSR) == S_IWUSR) local_buff[i] = 'w';
    else local_buff[i] = '-';
    i++;
    if ((mode & S_IXUSR) == S_IXUSR) local_buff[i] = 'x';
    else local_buff[i] = '-';
    i++;
    // group permissions
    if ((mode & S_IRGRP) == S_IRGRP) local_buff[i] = 'r';
    else local_buff[i] = '-';
    i++;
    if ((mode & S_IWGRP) == S_IWGRP) local_buff[i] = 'w';
    else local_buff[i] = '-';
    i++;
    if ((mode & S_IXGRP) == S_IXGRP) local_buff[i] = 'x';
    else local_buff[i] = '-';
    i++;
    // other permissions
    if ((mode & S_IROTH) == S_IROTH) local_buff[i] = 'r';
    else local_buff[i] = '-';
    i++;
    if ((mode & S_IWOTH) == S_IWOTH) local_buff[i] = 'w';
    else local_buff[i] = '-';
    i++;
    if ((mode & S_IXOTH) == S_IXOTH) local_buff[i] = 'x';
    else local_buff[i] = '-';
    return local_buff;
}
