
#include <sys/stat.h>
#include "main.h"
//Napisz program umożliwiający w trybie interaktywnym (tekstowym) wykonanie następujących operacji dla pliku będącego jego argumentem:
//
///ustawienie rygla do odczytu na wybrany znak pliku,
///ustawienie rygla do zapisu na wybrany znak pliku,
///Operacje 1 oraz 2 powinny być dostępne w dwóch wariantach, różniących się zachowaniem w przypadku, gdy ustawienie rygla jest niemożliwe ze względu na rygle założone przez inne procesy:
///wersja nieblokująca - próba ustawienia rygla kończy się wypisaniem informacji o błędzie
///        wersja blokująca - program czeka, aż ustawienie rygla będzie możliwe

//wyświetlenie listy zaryglowanych znaków pliku (z informacją o numerze PID procesu będącego właścicielem rygla oraz jego typie - odczyt/zapis),

///zwolnienie wybranego rygla,

///odczyt (funkcją read) wybranego znaku pliku,
///zmiana (funkcją write) wybranego znaku pliku.

//Wybór znaku we wszystkich przypadkach polega na podaniu numeru bajtu w pliku. Do ryglowania należy wykorzystać funkcję fcntl i strukturę flock. Wykonaj eksperymenty uruchamiając program jednocześnie w dwóch terminalach dla tego samego pliku. Próbuj ryglować (do odczytu lub do zapisu) te same znaki pliku i modyfikować (lub odczytywać) wybrane znaki pliku wykorzystując zarówno blokujące, jak i nieblokujące wersje operacji. Zwróć uwagę, że domyślnie ryglowanie działa w trybie rygli zalecanych (advisory), a nie rygli wymuszanych (mandatory).








int main(int argc, char **argv) {
    if (argc < 2)
        printAndQuit("No arg provided. There should be filename as an argument");

    char filename[BUFF_LENGTH];
    strcpy(filename, argv[1]);


    long int fileSize = getFileSize(filename);


    int fd=open(filename,O_RDWR);
    if (fd <= 0)
        printAndQuit("\nWrong filename");
    char op_str[BUFF_LENGTH];
    int char_no;
    char c;

    int pid,pid2;
    printHelper();

    int forever = 1;
    while (forever) {


        scanf("%s", op_str);

        switch (operationToEnum(op_str)) {

            case OP_LOCK_READ:
                if (scanf("%d", &char_no) < 1) {
                    printAndQuit("scanf error");
                }

                printLockMessage(lock_read(fd, char_no));

                break;
            case OP_LOCK_WRITE:
                if (scanf("%d", &char_no) < 1) {
                    printAndQuit("scanf error");
                }

                printLockMessage(lock_write(fd, char_no));
//                printLockMessage(setWriteLock(fd,char_no));
                break;
            case OP_LOCK_READ_WAIT:
                if (scanf("%d", &char_no) < 1) {
                    printAndQuit("scanf error");
                }
                if (lock_readw(fd, char_no)) {
                    printf("\nlocked\n");
                }
                break;
            case OP_LOCK_WRITE_WAIT:
                if (scanf("%d", &char_no) < 1) {
                    printAndQuit("scanf error");
                }
                lock_writew(fd, char_no);
//                if () {
                    printf("\nlocked\n");
//                }
                break;


            case OP_UNLOCK:
                if (scanf("%d", &char_no) < 1) {
                    printAndQuit("scanf error");
                }

                pid=getLock(fd,char_no,F_WRLCK);
                pid2=getLock(fd,char_no,F_RDLCK);
                if(pid!=0 || pid2 !=0){
                    printf("There is lock  at %d byte by pid=%d\n ",char_no,pid);
                } else{

                    if (un_lock(fd, char_no) < 0)
                        printf("\nCouldn't unlock\n");
                    else
                        printf("Unlocked\n");


                }
                break;

            case OP_SHOW:
                showLocks(fd, fileSize);
                break;

            case OP_READ:
                scanf("%d", &char_no);
                pid =getLock(fd,char_no,F_WRLCK);

                if(pid){
                   printf("There is write lock on byte %d by pid=%d\n",char_no,pid);
                } else{
                    if (readChar(fd, char_no, &c) < 0)
                        printf("Couldn't read char\n");
                    else
                        printf("Char: %c", c);
                }


                break;
            case OP_WRITE:
                scanf("%d %c", &char_no, &c);
                pid=getLock(fd,char_no,F_WRLCK);
                pid2=getLock(fd,char_no,F_RDLCK);
                if(pid!=0 || pid2 !=0){
                    printf("There is lock  at %d byte by pid=%d\n ",char_no,pid);
                } else{
                    if (writeChar(fd, char_no, c) < 0)
                        printf("Couldn't write char");
                    else
                        printf("Char written");
                }


                break;

            case OP_HELP:
                printHelper();
                break;
            case OP_QUIT:
                printAndQuit("Quitting\n");
            case OP_ERROR:
                printf("Wrong operation \n");
                break;
            default:
                printAndQuit("Something forgotten in switch. This statement should be unreachable");
        }


    }


}

void printHelper() {
    printf("\n\nOptions: \n"
                   "lr/lw/lrw/lrww bytenr\n"
                   "unlock byteNr\n"
                   "read/write byteNr\n"
                   "show\n"
                   "quit \n"
                   "help\n");

}

void printAndQuit(const char *str) {
    printf("%s", str);
    exit(1);
}

void printLockMessage(int val) {
    if (val < 0)
        printf("\nCouldnt lock\n");
    else
        printf("\n locked\n");
}

enum Operations operationToEnum(char *str) {
    if (strcmp("lr", str) == 0)
        return OP_LOCK_READ;
    if (strcmp("lrw", str) == 0)
        return OP_LOCK_READ_WAIT;

    if (strcmp("lw", str) == 0)
        return OP_LOCK_WRITE;

    if (strcmp("lww", str) == 0)
        return OP_LOCK_WRITE_WAIT;

    if (strcmp("help", str) == 0)
        return OP_HELP;


    if (strcmp("show", str) == 0)
        return OP_SHOW;

    if (strcmp("unlock", str) == 0)
        return OP_UNLOCK;

    if (strcmp("read", str) == 0)
        return OP_READ;

    if (strcmp("write", str) == 0)
        return OP_WRITE;

    if (strcmp("quit", str) == 0)
        return OP_QUIT;

    return OP_ERROR;
}


long int getFileSize(char *fname) {
    struct stat stat1;
    printf("%s", fname);
    if (stat(fname, &stat1) < 0)
        printAndQuit("getFileSize error - stat");
    return stat1.st_size;
}