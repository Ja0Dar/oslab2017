
#include "main.h"

int workWithLock(int file_desc, int char_no, int cmd, short type) {
    struct flock flock1;

    flock1.l_whence = SEEK_SET;
    flock1.l_start = char_no;
    flock1.l_len = 1;
    flock1.l_type = type;

    return fcntl(file_desc, cmd, &flock1);
}

int getLock(int fd, long char_no, short lock_type) {
    struct flock flock1;
    flock1.l_whence = SEEK_SET;
    flock1.l_start = char_no;
    flock1.l_type = lock_type;
    flock1.l_len = 1;


    if (fcntl(fd, F_GETLK, &flock1) < 0)
        printAndQuit("getLock - cntl error");

    if (flock1.l_type == F_UNLCK)
        return 0; // no lock here
    else
        return flock1.l_pid;

}

void showLocks(int fd, long filesize) {
    int res;
    for (long i = 0; i < filesize; ++i) {
        res = getLock(fd, i, F_RDLCK);
        if (res)
            printf("%li READ LOCK  pid=%d\n", i, res);

        res = getLock(fd, i, F_WRLCK);
        if (res)
            printf("%li WRITE LOCK pid=%d\n", i, res);

    }
}

int readChar(int fp, int char_no, char *res) {
    lseek(fp, char_no, SEEK_SET);
    return (read(fp, res, 1) < 0 ? -1 : 1);


}

int writeChar(int fd, int char_no, char c) {
    lseek(fd, char_no, SEEK_SET);
    return (write(fd, &c, 1) < 0 ? -1 : 1);
}



