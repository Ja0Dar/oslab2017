//
// Created by jacko on 20.03.17.
//

#ifndef ZAD3_MAIN_H

#define _XOPEN_SOURCE 500
#include <fcntl.h>
#include <sys/types.h>
#include <unistd.h>
#include<stdio.h>
#include <stdlib.h>
#include <memory.h>
//#include <stdbool.h>
#define BUFF_LENGTH 1024
#define lock_read(fd, char_no) \
    workWithLock(fd,char_no,F_SETLK,F_RDLCK)

#define lock_write(fd, char_no) \
    workWithLock(fd,char_no,F_SETLK,F_WRLCK)

#define un_lock(fd, char_no) \
    workWithLock(fd,char_no,F_SETLK,F_UNLCK)

#define lock_readw(fd, char_no) \
    workWithLock(fd,char_no,F_SETLKW,F_RDLCK)

#define lock_writew(fd, char_no) \
    workWithLock(fd,char_no,F_SETLKW,F_WRLCK)

#define has_read_lock (fd,char_no) \
    getLock(fd,char_no,F_RDLCK)

#define has_write_lock (fd,char_no) \
    getLock(fd,char_no,F_WRLCK)

enum Operations {
    OP_LOCK_READ,OP_LOCK_WRITE, OP_LOCK_READ_WAIT,OP_LOCK_WRITE_WAIT, OP_SHOW, OP_UNLOCK, OP_READ, OP_WRITE, OP_ERROR, OP_QUIT,OP_HELP
};

int writeChar(int fd, int char_no,char c);
int readChar(int fp,int char_no, char* res);
int workWithLock(int file_desc, int char_no, int cmd, short type);
long int getFileSize(char*);
void showLocks(int fd, long filesize);

void printHelper();


void printLockMessage(int val);

void printAndQuit(const char *str);

enum Operations operationToEnum(char *str);

#define ZAD3_MAIN_H

#endif //ZAD3_MAIN_H
