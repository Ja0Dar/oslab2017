//Należy napisać interpreter poleceń obsługujący operator pipe - "|". Interpreter czyta kolejne linie z wejścia standardowego. Każda linia ma format
//
//prog1 arg1 ... argn1 | prog2 arg1 ... argn2 | ... | progN arg1 ... argnN
//
//        Dla takiej linii interpreter powinien uruchomić wszystkie N poleceń w osobnych procesach, zapewniając przy użyciu potoków nienazwanych oraz funkcji dup2, by wyjście standardowe procesu k było przekierowane do wejścia standardowego procesu (k+1). Można założyć ograniczenie górne na ilość obsługiwanych argumentów (co najmniej 3) oraz ilość połączonych komend w pojedynczym poleceniu (co najmniej 20). Interpreter powinien umożliwiać uruchamianie programów znajdujących się w katalogu na liście w zmiennej PATH bez podawania pełnej ścieżki. Po uruchomieniu ciągu programów składających się na pojeczyne polecenie (linijkę) interpreter powinien oczekiwać na zakończenie wszystkich tych programów.

//Uwaga: należy użyć pipe/fork/exec, nie popen
#include <stdio.h>
#include <unistd.h>
#include <wait.h>
#include "functions.h"

#define READ 0
#define WRITE 1

void create_child_and_bind_IO(const char *command_to_exec, int *prev_pipe, int *next_pipe);

char **get_and_split_line_with_pipes(FILE *stream);

int main(int argc, char **argv) {
    char **commands_to_execute=NULL;

    while ((commands_to_execute= get_and_split_line_with_pipes(stdin)) != NULL) {


        if (commands_to_execute[0] == NULL)
            printAndQuit("Please print good line\n");
        int *pipes[BUF];
        for (int i = 0; i < BUF; i++)pipes[i] = NULL;


        //pipes[i] - pipe between commands_to_execute[i-1] and command_to_execute[i]
        pipes[0] = malloc(2 * sizeof(int *));
        pipe(pipes[0]);
        create_child_and_bind_IO(commands_to_execute[0], NULL, pipes[0]);
        int i;
        for (i=1; commands_to_execute[i] != NULL; i++) {
            pipes[i] = malloc(2 * sizeof(int *));
            pipe(pipes[i]);

            create_child_and_bind_IO(commands_to_execute[i], pipes[i - 1], pipes[i]);
        }


        char buf[3 * MEDIUM_BUF] = "\n\n";
        read(pipes[i - 1][READ], &buf, 3 * MEDIUM_BUF);
        printf("%s",buf);
    }
}

char ** get_and_split_line_with_pipes(FILE *stream) {
    char *line_with_newline=NULL;
    size_t tmp_size=0;

    if(getline(&line_with_newline,&tmp_size , stream) <0){
        free(line_with_newline);// man says "the buffer should be freed even if getline failed
        return NULL;
    }

    char **res=malloc(BUF*sizeof(char*));
    char * line=malloc(MEDIUM_BUF*sizeof(char));
    sscanf(line_with_newline, "%[^\n]\n", line);
    free(line_with_newline);
    strSplit(line, res, "|");
    trim_empty_strings_from_C_string_array(res);
    return res;
}

void create_child_and_bind_IO(const char *command_to_exec, int *prev_pipe, int *next_pipe) {
    pid_t pid;

    if ((pid = fork()) < 0) {
        printAndQuit("Fork error");
    } else if (pid == 0) {
        //child
        if (prev_pipe != NULL)
            dup2(prev_pipe[READ], STDIN_FILENO);
        if (next_pipe != NULL)
            dup2(next_pipe[WRITE], STDOUT_FILENO);

        exec_in_PATH_or_current_dir(command_to_exec);
    } else {
        wait(NULL);

        if (prev_pipe!=NULL)
            close(prev_pipe[READ]);
        if(next_pipe!=NULL)
            close(next_pipe[WRITE]);

    }


}








