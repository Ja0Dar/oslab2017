//
// Created by jacko on 08.04.17.
//

#ifndef ZAD1_FUNCTIONS_H
#include <memory.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#define BUF 30
#define SMALL_BUF 30
#define MEDIUM_BUF 255

char **get_split_path_with_current_dir();
void printAndQuit(const char *str,...);

void print_Cstring_array(char ** A);
void strSplit(char *source, char *result[], const char *separator);
void strSplittedFree(char *tab[]);
void exec_in_PATH_or_current_dir(const char *argv_string);
void trim_empty_strings_from_C_string_array(char **A);

void swap_string_pointers(char **a, char** b);


#define ZAD1_FUNCTIONS_H

#endif //ZAD1_FUNCTIONS_H
