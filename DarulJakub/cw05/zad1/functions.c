#include <printf.h>
#include "functions.h"


void strSplit(char *source, char *result[], const char *separator) {
    int i = 0;
    while ((result[i] = strsep(&source, separator)))i++;
    result[i] = NULL;
}

void strSplittedFree(char *tab[]) {

    size_t len;
    int j;
    for (j = 0; tab[j + 1] != NULL; ++j) {
        len = strlen(tab[j]);
        //replace \0 with anything ('X' in this case')
        tab[j][len] = 'X';
    }
    free((char *) tab[0]);


}

char **get_split_path_with_current_dir() {
    char **split_PATH = malloc(SMALL_BUF * sizeof(char *));
    char path[MEDIUM_BUF];
    for (int i = 0; i < SMALL_BUF; ++i) split_PATH[i] = NULL;
    strcpy(path, getenv("PATH"));

    strSplit(strdup((const char *) path), split_PATH, ":"); //     "/bin/:/usr/.../"   => ["/bin/","/usr/", ...]

    //Inserting current path to split_PATH
    int index = 0;
    while (split_PATH[index] != NULL)index++;
    split_PATH[index] = malloc(MEDIUM_BUF);
    getcwd(split_PATH[index], MEDIUM_BUF);


    return split_PATH;


}

//Todo - rename
void exec_in_PATH_or_current_dir(const char *argv_string) {
    char copied_argv_string[MEDIUM_BUF];
    strcpy(copied_argv_string,argv_string);

    char **argv_to_execute=malloc(sizeof(char*)*MEDIUM_BUF);
    strSplit(copied_argv_string, argv_to_execute," ");

    trim_empty_strings_from_C_string_array(argv_to_execute);


    char **split_PATH;
    split_PATH = get_split_path_with_current_dir();

    char possible_path_to_program[MEDIUM_BUF];
    for (int i = 0; split_PATH[i] != NULL; i++) {

        strcpy(possible_path_to_program, split_PATH[i]);
        strcat(possible_path_to_program, "/");
        strcat(possible_path_to_program, argv_to_execute[0]);


        execve(possible_path_to_program, argv_to_execute, __environ);
    }

    strSplittedFree(split_PATH);
    strSplittedFree(argv_to_execute);
    execlp("/bin/echo", "echo", "Bad fork, baad\n", NULL);
}





void trim_empty_strings_from_C_string_array(char **A) {
    int end_of_A=0;
    while (A[end_of_A]!=NULL)end_of_A++;
    if (end_of_A<=0)return;

    for (int k =end_of_A-1; k>=0 ; k--)
        if (strcmp("",A[k])==0){
//            free(*A[k]); //TODO:   is that ok?
            A[k]=NULL;
        }

    int null_finder=0,str_finder=0;

    while(null_finder<end_of_A &&str_finder<end_of_A){
        while(null_finder<end_of_A && A[null_finder]!=NULL)null_finder++;

        if(str_finder<=null_finder)str_finder=null_finder+1;
        while (str_finder< end_of_A && A[str_finder]==NULL)str_finder++;
        swap_string_pointers(&A[null_finder],&A[str_finder]);

    }





}


void swap_string_pointers(char **a, char **b) {
    char* tmp;
    tmp=*a;
    *a=*b;
    *b=tmp;
}


void print_Cstring_array(char **A) {
    printf("[");
    for (int i = 0; A[i] != NULL; i++)
        printf(" \"%s\",",A[i]);
    printf("]\n");
}


void printAndQuit(const char *str, ...) {
    va_list args;
    va_start (args, str);
    vfprintf(stderr,str,args);
    va_end (args);
    exit(1);
}
