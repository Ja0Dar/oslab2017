#include <printf.h>
#include <errno.h>
#include "functions.h"


double random_double(double min, double max){ return min+drand48()*(max-min);}


void printAndQuit(const char *str, ...) {
    va_list args;
    va_start (args, str);
    vfprintf(stderr,str,args);
    va_end (args);

    fprintf(stderr,"\n%s\n",strerror(errno));
    exit(1);
}


void free_square_matrix(int ***Matrix, int Matrix_size) {
    for (int i = 0; i < Matrix_size; ++i)
        free((*Matrix)[i]);
    free(*Matrix);
    *Matrix = NULL;
}

void allocate_square_matrix(int ***T, const int size) {
    *T = malloc(sizeof(int *) * size);
//    return;
    for (int i = 0; i < size; i++)
        (*T)[i] = malloc(sizeof(int) * size);

}

void initialize_square_matrix_with_zeros(int **T, const int size) {
    for (int i = 0; i < size; i++)
        for (int j = 0; j < size; ++j) T[i][j] = 0;

}

int map_double_to_int(double x_double, double minDouble, double maxDouble, int minInt, int maxInt) {
    return (int) (maxInt - minInt) * (x_double - minDouble) / (maxDouble - minDouble);
}
