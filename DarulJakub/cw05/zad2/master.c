#include <stdio.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "functions.h"

void master_arg_error_and_Quit();

void parse_args(int argc, char **argv, char *fifo_name, int *R);

void map_slaves_fifo_output_onto_T(const char *fifo_name, int *const *T, int T_size);

void save_T_to_file(int *const *T, int T_size);

void delegate_computations_to_slaves_writing_res_to_fifo(const char *fifo_name);

void draw_plot_from_file_using_gnuplot(int R);

#define  SLAVE_NO 30
#define POINTS_PER_SLAVE_STR "10000"
#define MAX_ITERS_STR "30" //K
#define SLAVE_PATH "./slave.out"
#define OUTPUT_FILENAME "data"

int main(int argc, char **argv) {
    char fifo_name[MEDIUM_BUF];
    int R;
    parse_args(argc, argv, fifo_name, &R);

    int **T = NULL;
    allocate_square_matrix(&T, R);
    initialize_square_matrix_with_zeros(T, R);


    if (mkfifo(fifo_name, 0666) < 0) // 0666 - permissions
        printAndQuit("Couldn't create fifo\n");

    delegate_computations_to_slaves_writing_res_to_fifo(fifo_name);

    map_slaves_fifo_output_onto_T(fifo_name, T, R);

    if (remove(fifo_name) < 0)
        printAndQuit("Couldn't remove fifo\n");

    save_T_to_file(T, R);

    free_square_matrix(&T, R);


    draw_plot_from_file_using_gnuplot(R);
    if (remove(OUTPUT_FILENAME) < 0)
        printf("Could not remove '" OUTPUT_FILENAME "'.\n");
    return 0;
}

void draw_plot_from_file_using_gnuplot(int matrix_size) {
    FILE *gnuplot;
    if ((gnuplot = popen("gnuplot", "w")) == NULL)
        printAndQuit("Couldn't open gnuplot");
    fprintf(gnuplot, "set view map\n");
    fprintf(gnuplot, "set xrange[0:%d]\n", matrix_size);
    fprintf(gnuplot, "plot '" OUTPUT_FILENAME "' with image\n");
    fflush(gnuplot);
    getchar();
    pclose(gnuplot);
}



void delegate_computations_to_slaves_writing_res_to_fifo(const char *fifo_name) {
    for (int i = 0; i < SLAVE_NO; i++) {
        int x = fork();
        if (x == 0) {
            if (execlp(SLAVE_PATH, SLAVE_PATH, fifo_name, POINTS_PER_SLAVE_STR, MAX_ITERS_STR, (char *) NULL) < 0)
                printAndQuit("EXEC ERROR");
        }
    }
}

void save_T_to_file(int *const *T, int T_size) {
    int data;
    if ((data = open(OUTPUT_FILENAME, O_WRONLY | O_CREAT | O_TRUNC,
                     S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)) < 0)
        printAndQuit("Couldn't create file 'data'");

    char line[SMALL_BUF];
    for (int j = 0; j < T_size; ++j) {
        for (int i = 0; i < T_size; ++i) {
            sprintf(line, "%d %d %d\n", j, i, T[j][i]);
            write(data, line, strlen(line));
        }
    }
    close(data);
}

void map_slaves_fifo_output_onto_T(const char *fifo_name, int *const *T, int T_size) {
    char line[MEDIUM_BUF];
    double re, im;
    int iters;
    int x, y;
    int fd = open(fifo_name, O_RDONLY);
    if (fd < 0)
        printAndQuit("Couldn't open fifo\n");

    while (read(fd, line, MEDIUM_BUF) > 0) {
        if (sscanf(line, "%lf %lfi %d", &re, &im, &iters) != 3)
            printAndQuit("Wrong line format in fifo ('%s')\n", fifo_name);
        x = map_double_to_int(re, MIN_RE, MAX_RE, 0, T_size);
        y = map_double_to_int(im, MIN_IM, MAX_IM, 0, T_size);
        if (x < T_size && y < T_size)
            T[x][y] = iters;
    }
    close(fd);
}




void parse_args(int argc, char **argv, char *fifo_name, int *R) {
    if (argc != 3)
        master_arg_error_and_Quit();

    sscanf(argv[1], "%s", fifo_name);

    if (sscanf(argv[2], "%d", R) < 1)
        master_arg_error_and_Quit();

}

void master_arg_error_and_Quit() {
    printAndQuit("Master: wrong args. should be [fifo_name] [R-size of array representing area D]");
}


