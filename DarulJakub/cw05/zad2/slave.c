//Slave przyjmuje trzy argumenty -
//      ścieżkę do potoku nazwanego i
//      liczby całkowite
//              N oraz K.
//
// Wykonuje następujące akcje:
// otwiera potok nazwany
//generuje N losowych punktów w obszarze D, dla każdego punktu c oblicza wartość iters(c) oraz zapisuje do potoku jedną linijkę zawierającą część rzeczywistą oraz urojoną punktu c oraz obliczoną wartość iters(c)
//zamyka potok nazwany

#include <stdio.h>
#include <complex.h>
#include <fcntl.h>
#include <sys/time.h>
#include <time.h>
#include "functions.h"


void slave_arg_error_end_quit();

int iters(double complex c, int K);

double complex f_c(double complex c, double complex z);

void parse_args(int argc, char **argv, char *fifo_path, int *N, int *K);

void generate_points_and_pass(int supply, int K, int fifo_fd);


int main(int argc, char **argv) {
    char fifo_path[MEDIUM_BUF];
    int number_of_points_to_supply, K;
    parse_args(argc, argv, fifo_path, &number_of_points_to_supply, &K);

    srand48((unsigned int) getpid() * time(NULL));

    int fifo_fd = open(fifo_path, O_WRONLY);
    if (fifo_fd < 0)
        printAndQuit("Slave couldn't open fifo");

    generate_points_and_pass(number_of_points_to_supply, K, fifo_fd);
    close(fifo_fd);
}


void generate_points_and_pass(int number_of_points, int K, int fifo_fd) {
    double re, im;
    char buf[MEDIUM_BUF];
    double complex c;
    for (int i = 0; i < number_of_points; i++) {
        re = random_double(MIN_RE, MAX_RE);
        im = random_double(MIN_IM, MAX_IM);
        c = re + im * I;
        sprintf(buf, "%f %fi %d\n", re, im, iters(c, K));
        write(fifo_fd, buf, MEDIUM_BUF);

    }
}


double complex f_c(double complex c, double complex z) {
    return cpow(z, 2.0) + c;

}

void parse_args(int argc, char **argv, char *fifo_path, int *N, int *K) {
    if (argc != 4)
        slave_arg_error_end_quit();

    sscanf(argv[1], "%s", fifo_path);

    if (sscanf(argv[2], "%d", N) < 1)
        slave_arg_error_end_quit();
    if (sscanf(argv[3], "%d", K) < 1)
        slave_arg_error_end_quit();

}

void slave_arg_error_end_quit(){
    printAndQuit("Wrong args. should be [fifo_path] [N - number of points ] [K -]\n");
}

int iters(double complex c, int K) {
    double complex z = 0.0;
    int i;
    for (i = 0; i < K && cabs(z) < 2.0; i++)
        z = f_c(c, z);
    return i;
}




