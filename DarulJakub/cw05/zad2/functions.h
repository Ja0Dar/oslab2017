//
// Created by jacko on 08.04.17.
//

#ifndef ZAD1_FUNCTIONS_H

#include <memory.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <complex.h>
#include <math.h>

#define SMALL_BUF 60
#define MEDIUM_BUF 255

#define MIN_RE -2
#define MAX_RE 1
#define MIN_IM -1
#define MAX_IM 1

double random_double(double min, double max);

void printAndQuit(const char *str, ...);

void allocate_square_matrix(int ***T, const int size);

int map_double_to_int(double x_double, double minDouble, double maxDouble, int minInt, int maxInt);

void free_square_matrix(int ***Matrix, int Matrix_size);

void initialize_square_matrix_with_zeros(int **T, const int size);


#define ZAD1_FUNCTIONS_H

#endif //ZAD1_FUNCTIONS_H
