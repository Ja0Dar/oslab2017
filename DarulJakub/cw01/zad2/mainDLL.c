#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <sys/times.h> //times
#include "library.h"

#define DLL

#ifdef DLL

#include <dlfcn.h>

#endif


#define CLK sysconf(_SC_CLK_TCK) // tics per sec

typedef struct timeTuple {
    double real, user, sys;
} timeTuple;

typedef struct timeAVG {
    timeTuple times;
    int count;
} timeAVG;

timeAVG newTimeAVG() {
    timeAVG t;
    t.count = 0;
    t.times.sys = t.times.real = t.times.user = 0;
    return t;
}

void addTimeAVGTo(timeAVG *tavg, const timeTuple added) {
    timeTuple *tt = &tavg->times;
    tt->user += added.user;
    tt->real += added.real;
    tt->sys += added.sys;
    tavg->count += 1;
}


timeTuple getTimeDiff(struct tms *prevTimes, clock_t *prevReal) {
    timeTuple res;

    struct tms now;
    times(&now);
    clock_t nowReal = clock();
    if (prevTimes->tms_stime == -1) {
        res.sys = res.real = res.user = -1.0;
    } else {
        res.real = ((double) (nowReal - *(prevReal))) / CLOCKS_PER_SEC;
        res.sys = ((double) (now.tms_stime - prevTimes->tms_stime)) / CLK;
        res.user = ((double) (now.tms_utime - prevTimes->tms_utime)) / CLK;
    }
    *prevReal = nowReal;
    *prevTimes = now;
    return res;
}

timeTuple getAvgTime(timeAVG t) {
    timeTuple res;
    res.user = t.times.user / t.count;
    res.real = t.times.real / t.count;
    res.sys = t.times.sys / t.count;
    return res;
}

void printTimeTuple(timeTuple t) {
    printf("Real:  %f    User:   %f    Sys:     %f \n", t.real, t.user, t.sys);
}


void deepestLeftLeafUtil(TreeNode *root, int lvl, int *maxlvl,
                         int isLeft, TreeNode **resPtr) {
    // Base case
    if (root == NULL)
        return;

    // Update result if this node is left leaf and its level is more
    // than the maxl level of the current result
    if (isLeft > 0 && !root->left && !root->right && lvl > *maxlvl) {
        *resPtr = root;
        *maxlvl = lvl;
        return;
    }

    // Recur for left and right subtrees
    deepestLeftLeafUtil(root->left, lvl + 1, maxlvl, 1, resPtr);
    deepestLeftLeafUtil(root->right, lvl + 1, maxlvl, 0, resPtr);
}

// A wrapper over deepestLeftLeafUtil().
TreeNode *deepestLeftLeaf(TreeNode *root) {
    int maxlevel = 0;
    TreeNode *result = NULL;
    deepestLeftLeafUtil(root, 0, &maxlevel, 0, &result);
    return result;
}


int main() {
#ifdef DLL


    void *libhandle = dlopen("./libshared.so", RTLD_LAZY);
    PhoneBookRecord
    (*createPhoneBookRecord)(const char *firstname, const char *lastname, const char *birthdate, const char *email,
                             long phone, const char *address)=dlsym(libhandle,"createPhoneBookRecord");
//    void (*printPhonebookRecord)(PhoneBookRecord pbr);
    int (*firstnameComparator)(PhoneBookRecord pb1, PhoneBookRecord pb2)=dlsym(libhandle,"firstnameComparator");
//    int (*lastnameComparator)(PhoneBookRecord pb1, PhoneBookRecord pb2);
//    int (*birthdateComparator)(PhoneBookRecord pb1, PhoneBookRecord pb2);
    int (*phoneComparator)(PhoneBookRecord pb1, PhoneBookRecord pb2)=dlsym(libhandle,"phoneComparator");
//    int (*addressComparator)(PhoneBookRecord pb1, PhoneBookRecord pb2);
//    int (*emailComparator)(PhoneBookRecord pb1, PhoneBookRecord pb2);
//    int (*PhoneBookRecordEqual)(PhoneBookRecord p1, PhoneBookRecord p2);
    void (*deletePhoneBookRecord)(PhoneBookRecord pbr)=dlsym(libhandle,"deletePhoneBookRecord");
//    lNode* (*createlNode)(PhoneBookRecord pbr);
//    void (*concatlNode)(lNode *prev, lNode *next);
//    int (*length)(ListWithGuards l);
    ListWithGuards (*createList)(int (*comparator)(PhoneBookRecord, PhoneBookRecord))=dlsym(libhandle,"createList");
//    void (*deleteList)(ListWithGuards *l);
    void (*insertPBRIntoList)(ListWithGuards l, PhoneBookRecord pbr)=dlsym(libhandle,"insertPBRIntoList");
//    void (*insertNodeIntoList)(ListWithGuards l, lNode *ln);
    void (*deleteNodeFromList)(lNode *ln)=dlsym(libhandle,"deleteNodeFromList");
//    ListWithGuards (*forcedSortList)(ListWithGuards l, int (*comparator)(PhoneBookRecord, PhoneBookRecord));
    ListWithGuards (*sortList)(ListWithGuards l, int (*comparator)(PhoneBookRecord, PhoneBookRecord))=dlsym(libhandle,"sortList");
    ListExcerpt
    (*findInList)(ListWithGuards l, PhoneBookRecord val, int (*comparator)(PhoneBookRecord, PhoneBookRecord))=dlsym(libhandle,"findInList");
//    void (*printListPart)(lNode *startNode, lNode *endNode);
//    void (*printList)(ListWithGuards l)=dlsym(libhandle,"printList");
//    void (*bindLeftTreeNode)(TreeNode *parent, TreeNode *left);
//    void (*bindRightTreeNode)(TreeNode *parent, TreeNode *right);
    TreeNode *(*minNodeInSubtree)(TreeNode *n)=dlsym(libhandle,"minNodeInSubtree");
//    TreeNode *(*maxNodeInSubtree)(TreeNode *n);
//    TreeNode *(*successor)(TreeNode *n);
//    TreeNode *(*pedecessor)(TreeNode *n);
    Tree (*createTree)(int (*comparator)(PhoneBookRecord, PhoneBookRecord))=dlsym(libhandle,"createTree");
//    TreeNode *(*createTreeNode)(PhoneBookRecord pbr);
//    void (*insertNodeIntoTree)(Tree *t, TreeNode *currentRoot, TreeNode *inserted);
    void (*insertPBRIntoTree)(Tree *t, PhoneBookRecord pbr)=dlsym(libhandle,"insertPBRIntoTree");
//    Tree (*forcedSortTree)(Tree *t, int(*comparator)(PhoneBookRecord,PhoneBookRecord));
    Tree (*sortTree)(Tree *t, int(*comparator)(PhoneBookRecord, PhoneBookRecord))=dlsym(libhandle,"sortTree");
//    void (*deleteSubTree)(TreeNode *tn);
//    void (*deleteTree)(Tree *t);
    void (*deleteTreeNode)(Tree *t, TreeNode *tn)=dlsym(libhandle,"deleteTreeNode");
    TreeNode *(*findFirstInSubTree)(Tree t, TreeNode *tn, PhoneBookRecord pbr)=dlsym(libhandle,"findFirstInSubTree");
//    void (*printTree)(Tree t)=dlsym(libhandle,"printTree");



#endif



    //FOR checking Time
    struct tms prevTimes, prevTimesForSmallMeasurments;
    clock_t prevReal, prevRealForSmallMeasurments;
    prevTimes.tms_stime = -1;
    prevTimesForSmallMeasurments.tms_stime = -1;
    printf("Start.\n");

    timeTuple tmpTT;

/////////////////////////Creating Phonebook list;


    int (*comparator)(PhoneBookRecord, PhoneBookRecord)=firstnameComparator;
    ListWithGuards l = createList(comparator);
    char line[1024];
    long phone;
    char firstname[30], lastname[30], birthdate[30], email[30], address[40];
    PhoneBookRecord pbr;
    getTimeDiff(&prevTimes, &prevReal); //Reset time
    FILE *stream = fopen("names.csv", "r");

    timeAVG listTimeAVG = newTimeAVG();
    while (fgets(line, 1024, stream)) {
        sscanf(line, "%[^ ] %[^|]|%[^|]|%[^|]|%li|%[0-9a-zA-Z.#,- ]",
               firstname,
               lastname,
               birthdate,
               email,
               &phone,
               address);
        pbr = createPhoneBookRecord(firstname, lastname, birthdate, email, phone, address);
        getTimeDiff(&prevTimesForSmallMeasurments, &prevRealForSmallMeasurments);  // reset clock
        insertPBRIntoList(l, pbr);
        addTimeAVGTo(&listTimeAVG,
                     getTimeDiff(&prevTimesForSmallMeasurments, &prevRealForSmallMeasurments));  // measure

    }
    fclose(stream);

    printf("Created Phonebook -List with 1000 values - read from csv\n");
    printTimeTuple(getTimeDiff(&prevTimes, &prevReal));
    printf("\n\n");


    printf("Average time for inserting element into list: \n");
    printTimeTuple(getAvgTime(listTimeAVG));
    printf("\n\n");
/////CREATING TREE
    Tree t = createTree(comparator);//firstname comparator
    getTimeDiff(&prevTimes, &prevReal); // Reset Time
    stream = fopen("names.csv", "r");


    timeAVG treeTimeAVG = newTimeAVG();
    while (fgets(line, 1024, stream)) {
        sscanf(line, "%[^ ] %[^|]|%[^|]|%[^|]|%li|%[0-9a-zA-Z. #-]",
               firstname,
               lastname,
               birthdate,
               email,
               &phone,
               address);
        pbr = createPhoneBookRecord(firstname, lastname, birthdate, email, phone, address);
        getTimeDiff(&prevTimesForSmallMeasurments, &prevRealForSmallMeasurments);
        insertPBRIntoTree(&t, pbr);
        addTimeAVGTo(&treeTimeAVG,
                     getTimeDiff(&prevTimesForSmallMeasurments, &prevRealForSmallMeasurments));
    }
    fclose(stream);

    printf("Created Phonebook -BST with 1000 values - read from csv\n");
    printTimeTuple(getTimeDiff(&prevTimes, &prevReal));
    printf("\n\n");

    printf("Average time for inserting element into tree: \n");
    printTimeTuple(getAvgTime(treeTimeAVG));
    printf("\n\n");



//////DELETE FROM LIST
    getTimeDiff(&prevTimes, &prevReal);
    deleteNodeFromList(l.firstGuard->next);
    tmpTT = getTimeDiff(&prevTimes, &prevReal);
    printf("Deleting from list with given list node ( without searching)\n");
    printTimeTuple(tmpTT);
    printf("\n\n");

////DELETE FROM TREE
    getTimeDiff(&prevTimes, &prevReal);
    deleteTreeNode(&t, t.root);
    tmpTT = getTimeDiff(&prevTimes, &prevReal);
    printf("Deleting from tree- root\n");
    printTimeTuple(tmpTT);
    printf("\n\n");


    TreeNode *leaf = minNodeInSubtree(t.root);// geting LEAF
    getTimeDiff(&prevTimes, &prevReal);//reset time
//    return 1;
    deleteTreeNode(&t, leaf);
    tmpTT = getTimeDiff(&prevTimes, &prevReal);
    printf("Deleting  from tree - leaf \n");
    printTimeTuple(tmpTT);
    printf("\n\n");


    ///FIND IN LIST

    pbr = createPhoneBookRecord("Leah", "", "", "", 0, "");
    getTimeDiff(&prevTimes, &prevReal);
    findInList(l, pbr,
               l.comparator);//  it returns part of list with searched values, but I don't need it, so I  can skip ret value
    tmpTT = getTimeDiff(&prevTimes, &prevReal);
    printf("Pesimistic finding in List \n");
    printTimeTuple(tmpTT);
    printf("\n\n");

//TODO UNCOMENT IF IT WORKS
    deletePhoneBookRecord(pbr);
    pbr = createPhoneBookRecord("Ala", "", "", "", 0, "");
    getTimeDiff(&prevTimes, &prevReal);
    findInList(l, pbr,
               l.comparator);//  it returns part of list with searched values, but I don't need it, so I  can skip ret value
    tmpTT = getTimeDiff(&prevTimes, &prevReal);
    printf("Optimistic finding in List \n");
    printTimeTuple(tmpTT);
    printf("\n\n");
    deletePhoneBookRecord(pbr);


    ///FIND IN tree

    ///Optimistic searching in tree
    getTimeDiff(&prevTimes, &prevReal);
    findFirstInSubTree(t, t.root,
                       t.root->value);//  it returns part of list with searched values, but I don't need it, so I  can skip ret value
    tmpTT = getTimeDiff(&prevTimes, &prevReal);
    printf("Optimistic searching  in Tree \n");
    printTimeTuple(tmpTT);
    printf("\n\n");

    ///Pesimistic searching tree
    TreeNode *tn = deepestLeftLeaf(t.root);
//    printPhonebookRecord(tn->value);
    getTimeDiff(&prevTimes, &prevReal);


    if (findFirstInSubTree(t, t.root, tn->value) == NULL) {
        printf("\n===========Found null=========\n");
    }
    tmpTT = getTimeDiff(&prevTimes, &prevReal);

    printf("Pesimistic searching in tree - deepest node (left node)\n");
    printTimeTuple(tmpTT);
    printf("\n\n");





    ///SORTING LIST
    getTimeDiff(&prevTimes, &prevReal);
    l = sortList(l, phoneComparator);
    tmpTT = getTimeDiff(&prevTimes, &prevReal);
    printf("List sorting \n");
    printTimeTuple(tmpTT);
    printf("\n\n");



    /// SORTING TREE
    getTimeDiff(&prevTimes, &prevReal);
    t = sortTree(&t, comparator);
    tmpTT = getTimeDiff(&prevTimes, &prevReal);
    printf("Tree sorting \n");
    printTimeTuple(tmpTT);
    printf("\n\n");

//    printList(l);

#ifdef DLL
    dlclose(libhandle);
#endif
//    printList(l);
    return 0;
}
