#ifndef PBLIB
#define PBLIB

#define _GNU_SOURCE
#include <string.h>
#include <stdio.h>
#include <stdlib.h>


///PHONEBOOK RECORD
typedef struct PhoneBookRecord {
    //nonconstant pointer to constant char
    char *firstname;
    char *lastname;
    char *birthdate;
    char *email;
    long phone;
    char *address;
} PhoneBookRecord;


/// LIST STUFF
typedef struct lNode {
    struct lNode *prev;
    struct lNode *next;
    PhoneBookRecord value;
} lNode;

typedef struct ListWithGuards {
    int (*comparator)(PhoneBookRecord, PhoneBookRecord);

    lNode *firstGuard;
    lNode *lastGuard;
} ListWithGuards;

typedef struct ListExcerpt {
    lNode *first;
    lNode *last;

    int (*comparator)(PhoneBookRecord, PhoneBookRecord);

} ListExcerpt;


// Do i have to use it?
typedef struct Tree {
    int (*comparator)(PhoneBookRecord, PhoneBookRecord);

    struct TreeNode *root;
} Tree;

typedef struct TreeNode {
    struct TreeNode *left, *right, *parent;
    PhoneBookRecord value;
} TreeNode;








PhoneBookRecord
createPhoneBookRecord(const char *firstname, const char *lastname, const char *birthdate, const char *email,
                      long phone, const char *address);


void printPhonebookRecord(PhoneBookRecord pbr);


int firstnameComparator(PhoneBookRecord pb1, PhoneBookRecord pb2);

int lastnameComparator(PhoneBookRecord pb1, PhoneBookRecord pb2);

int birthdateComparator(PhoneBookRecord pb1, PhoneBookRecord pb2);

int phoneComparator(PhoneBookRecord pb1, PhoneBookRecord pb2);


int addressComparator(PhoneBookRecord pb1, PhoneBookRecord pb2);

int emailComparator(PhoneBookRecord pb1, PhoneBookRecord pb2);


int PhoneBookRecordEqual(PhoneBookRecord p1, PhoneBookRecord p2);
void deletePhoneBookRecord(PhoneBookRecord pbr);

lNode *createlNode(PhoneBookRecord pbr);


void concatlNode(lNode *prev, lNode *next);


int length(ListWithGuards l);


ListWithGuards createList(int (*comparator)(PhoneBookRecord, PhoneBookRecord));

void deleteList(ListWithGuards *l);

void
insertPBRIntoList(ListWithGuards l, PhoneBookRecord pbr);
void insertNodeIntoList(ListWithGuards l, lNode *ln);


void deleteNodeFromList(lNode *ln);

ListWithGuards forcedSortList(ListWithGuards l, int (*comparator)(PhoneBookRecord, PhoneBookRecord));

ListWithGuards sortList(ListWithGuards l, int (*comparator)(PhoneBookRecord, PhoneBookRecord));

ListExcerpt
findInList(ListWithGuards l, PhoneBookRecord val, int (*comparator)(PhoneBookRecord, PhoneBookRecord));

void printListPart(lNode *startNode, lNode *endNode);

void printList(ListWithGuards l);


void bindLeftTreeNode(TreeNode *parent, TreeNode *left);

void bindRightTreeNode(TreeNode *parent, TreeNode *right);


TreeNode *minNodeInSubtree(TreeNode *n);

TreeNode *maxNodeInSubtree(TreeNode *n);

TreeNode *successor(TreeNode *n);

TreeNode *pedecessor(TreeNode *n);



Tree createTree(int (*comparator)(PhoneBookRecord, PhoneBookRecord));


TreeNode *createTreeNode(PhoneBookRecord pbr);

void insertNodeIntoTree(Tree *t, TreeNode *currentRoot, TreeNode *inserted);

void insertPBRIntoTree(Tree *t, PhoneBookRecord pbr);


Tree forcedSortTree(Tree *t, int(*comparator)(PhoneBookRecord,PhoneBookRecord));

Tree sortTree(Tree *t, int(*comparator)(PhoneBookRecord,PhoneBookRecord));

void deleteSubTree(TreeNode *tn);

void deleteTree(Tree *t);

void deleteTreeNode(Tree *t, TreeNode *tn);

TreeNode *findFirstInSubTree(Tree t, TreeNode *tn, PhoneBookRecord pbr);

void printTree(Tree t);

#endif