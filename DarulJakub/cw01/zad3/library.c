#include "library.h"



///PHONEBOOK

PhoneBookRecord
createPhoneBookRecord(const char *firstname, const char *lastname, const char *birthdate, const char *email,
                      long phone, const char *address) {
    PhoneBookRecord res;
    res.firstname = strdup(firstname);
    res.lastname = strdup(lastname);
    res.birthdate = strdup(birthdate);
    res.phone = phone;
    res.address = strdup(address);
    res.email = strdup(email);
    return res;
}

void deletePhoneBookRecord(PhoneBookRecord pbr) {
    free(pbr.firstname);
    free(pbr.lastname);
    free(pbr.birthdate);
    free(pbr.address);
    free(pbr.email);

}

void printPhonebookRecord(PhoneBookRecord pbr) {
    printf("Name: %s LName: %s  Bdate: %s mail:%s phone: %li address: %s", pbr.firstname, pbr.lastname, pbr.birthdate,
           pbr.email, pbr.phone, pbr.address);

    printf("\n");

}
/// PHHONEBOOK RECORD COMPARATORS

///strcmp(s1,s2)= {s1<s2 : -1
///                s1==s2: 0
///                s1>s2 : 1


///   " Is  pb1 > pb2?  yes =1 no,less = -1 equal=0
int firstnameComparator(PhoneBookRecord pb1, PhoneBookRecord pb2) {
    return strcmp(pb1.firstname, pb2.firstname);
}

int lastnameComparator(PhoneBookRecord pb1, PhoneBookRecord pb2) {
    return strcmp(pb1.lastname, pb2.lastname);
}

int birthdateComparator(PhoneBookRecord pb1, PhoneBookRecord pb2) {
    return strcmp(pb1.birthdate, pb2.birthdate);
}

int phoneComparator(PhoneBookRecord pb1, PhoneBookRecord pb2) {
    if (pb1.phone == pb2.phone) {
        return 0;
    }

    if (pb1.phone > pb2.phone) {
        return 1;
    } else {
        return -1;
    }
}


int addressComparator(PhoneBookRecord pb1, PhoneBookRecord pb2) {
    return strcmp(pb1.address, pb2.address);
}

int emailComparator(PhoneBookRecord pb1, PhoneBookRecord pb2) {
    return strcmp(pb1.email, pb2.email);
}


int PhoneBookRecordEqual(PhoneBookRecord p1, PhoneBookRecord p2) {
    if (    firstnameComparator(p1, p1) != 0 ||
            lastnameComparator(p1, p1) != 0 ||
            birthdateComparator(p1, p1) != 0 ||
            phoneComparator(p1, p1) != 0 ||
            addressComparator(p1, p1) != 0 ||
            emailComparator(p1, p1) != 0)
        return 0;

    return 1;
}


/// L-NODE :
lNode *createlNode(PhoneBookRecord pbr) {
    lNode *res = (lNode *) calloc(1, sizeof(lNode));
    res->prev = NULL;
    res->next = NULL;
    res->value = pbr;
    return res;
}


void concatlNode(lNode *prev, lNode *next) {
    prev->next = next;
    next->prev = prev;
}


/////////////////////////////////////////////////////////////////// LIST
int length(ListWithGuards l) {
    lNode *fst = l.firstGuard->next;
    int len = 0;//because there is always a jump between first and last node ( guards)
    while (fst->next != NULL) {
        fst = fst->next;
        len++;
    }
    return len;
}


ListWithGuards createList(int (*comparator)(PhoneBookRecord, PhoneBookRecord)) {
    ListWithGuards res;
    res.comparator = comparator;
    PhoneBookRecord a = createPhoneBookRecord("First", "Guard", "XXX", "XXX", 000, "xxx"), b = createPhoneBookRecord(
            "Last", "Guard", "XXX", "XXX", 000, "xxx");// PBR's just for debugging
    res.firstGuard = createlNode(a);
    res.lastGuard = createlNode(b);
    res.firstGuard->prev = res.lastGuard->next = NULL;
    concatlNode(res.firstGuard, res.lastGuard);
    return res;
}

void deleteList(ListWithGuards *l) {
    lNode *r = l->firstGuard->next, *p = NULL;
    while (r != l->lastGuard) {
        p = r;
        r = r->next;
        deletePhoneBookRecord(p->value);
        free(p);
    }
    deletePhoneBookRecord(l->firstGuard->value);
    deletePhoneBookRecord(l->lastGuard->value);
    free(l->firstGuard);
    free(l->lastGuard);
    l->firstGuard = l->lastGuard = NULL;
    l->comparator = NULL;

}

void
insertPBRIntoList(ListWithGuards l, PhoneBookRecord pbr) {//,int (*PBRComparator)(PhoneBookRecord,PhoneBookRecord)){

    lNode *r = l.firstGuard;
    while (r->next->next != NULL && (*l.comparator)(pbr, r->next->value) > 0) {
        r = r->next;
    }

    // Now  lNode(pbr) should be between r and r->next;
    lNode *next, *newNode = createlNode(pbr);
    next = r->next;
    concatlNode(r, newNode);
    concatlNode(newNode, next);

}


void insertNodeIntoList(ListWithGuards l, lNode *ln) {
    lNode *r = l.firstGuard;
    while (r->next->next != NULL && (*l.comparator)(ln->value, r->next->value) > 0) {
        r = r->next;
    }
    // Now  lNode(pbr) should be between r and r->next;
    lNode *next;// * newNode = createlNode(pbr);
    next = r->next;
    concatlNode(r, ln);
    concatlNode(ln, next);

}


void deleteNodeFromList(lNode *ln) {
    if (ln->prev == NULL) {
        printf("Cannot delete firstGuard in list");
    } else if (ln->next == NULL) {
        printf("Cannot delete lastGuard in list");
    } else {
        concatlNode(ln->prev, ln->next);
        deletePhoneBookRecord(ln->value);
        free(ln);
    }
}

ListWithGuards forcedSortList(ListWithGuards l, int (*comparator)(PhoneBookRecord, PhoneBookRecord)) {
    ListWithGuards res = createList(comparator);
    lNode *r, *prev;
    r = l.firstGuard->next;
    while (r->next != NULL) {
        prev = r;
        r = r->next;
        insertNodeIntoList(res, prev);

    }
    deletePhoneBookRecord(l.firstGuard->value);
    deletePhoneBookRecord(l.lastGuard->value);
    free(l.firstGuard);
    free(l.lastGuard);
    return res;
}

ListWithGuards sortList(ListWithGuards l, int (*comparator)(PhoneBookRecord, PhoneBookRecord)) {
    return comparator != l.comparator ? forcedSortList(l, comparator) : l;
}

ListExcerpt
findInList(ListWithGuards l, PhoneBookRecord val, int (*comparator)(PhoneBookRecord, PhoneBookRecord)) {
    ListExcerpt res; res.first=res.last=NULL;
    res.comparator = comparator;
    if (l.comparator != comparator) {
        printf("findInList : phonebook is not sorted correcly. please sort it with comparator before trying to find sth with that comparator");
        return res;
    }

    comparator=l.comparator;
    //EMpty list:
    if (l.firstGuard->next == l.lastGuard) {
//        res.first = res.last = NULL;
        return res;
    }

    lNode *first = l.firstGuard->next;

    while ((comparator)(val, first->value) > 0 && first->next != NULL) {// if priovided val > what we have
        first = first->next;                  //go ahead
    }


    if ((*comparator)(val, first->value) < 0 || first->next == NULL ||
        first->prev == NULL) {  // if provided val <what we have or >  max or <min
        res.first = res.last = NULL;
    } else {// so now first describes first record with value
        lNode *last = first;
        while ((*comparator)(last->next->value, val) == 0 && last->next != l.lastGuard) {
            last = last->next;

        }

        res.first = first;
        res.last = last;

    }
    return res;


}

void printListPart(lNode *startNode, lNode *endNode) {
    if (startNode == NULL) {
        printf("empty part of list");
    }
    //PRints startnode (inclusive) -> endnode (exclusive)
    lNode *r = startNode;
    while (r->next != NULL && r != endNode) {
        printPhonebookRecord(r->value);
        r = r->next;
    }


}

void printList(ListWithGuards l) {
    printListPart(l.firstGuard->next, l.lastGuard);
}




///////////////////////////////////////////////////////////////////////TREES




void bindLeftTreeNode(TreeNode *parent, TreeNode *left) {
    parent->left = left;
    left->parent = parent;
}

void bindRightTreeNode(TreeNode *parent, TreeNode *right) {
    parent->right = right;
    right->parent = parent;
}


TreeNode *minNodeInSubtree(TreeNode *n) {
    while (n->left != NULL) {
        n = n->left;
    }
    return n;

}

TreeNode *maxNodeInSubtree(TreeNode *n) {
    while (n->right != NULL) {
        n = n->right;
    }
    return n;
}

TreeNode *successor(TreeNode *n) {
    // Either minium of right subtree
    if (n->right != NULL)
        return minNodeInSubtree(n->right);

    // or go up left until you go up-right :)
    TreeNode *p = n->parent;
    while (p != NULL && n == p->right) {
        n = p;
        p = p->parent;
    }
    return p;
}

TreeNode *pedecessor(TreeNode *n) {

    if (n->left != NULL) {
        return maxNodeInSubtree(n->left);
    }


    TreeNode *p = n->parent;
    while (p != NULL && n == p->left) {
        n = p;
        p = p->parent;
    }
    return p;
}


Tree createTree(int (*comparator)(PhoneBookRecord, PhoneBookRecord)) {
    Tree res;
    res.comparator = comparator;
    res.root = NULL;
    return res;
}


TreeNode *createTreeNode(PhoneBookRecord pbr) {
    TreeNode *tn = (TreeNode *) calloc(1, sizeof(TreeNode));
    tn->value = pbr;
    tn->left = tn->right = tn->parent = NULL;
    return tn;
}

void insertNodeIntoTree(Tree *t, TreeNode *currentRoot, TreeNode *inserted) {
    // greater values go -> right
    // le - > go left
    if (currentRoot == NULL) {// && t->root == NULL) {
        t->root = inserted;
        return;
    }

    if (t->comparator(currentRoot->value, inserted->value) > 0) {// if inserted <current
        if (currentRoot->left == NULL)
            bindLeftTreeNode(currentRoot, inserted);
//           currentRoot->left=inserted;
        else
            insertNodeIntoTree(t, currentRoot->left, inserted);
    } else {// if inserted >= currentroot
        if (currentRoot->right == NULL)
            bindRightTreeNode(currentRoot, inserted);
            //currentRoot->right=inserted;
        else
            insertNodeIntoTree(t, currentRoot->right, inserted);

    }
}

void insertPBRIntoTree(Tree *t, PhoneBookRecord pbr) {
    TreeNode *inserted = createTreeNode(pbr);
    insertNodeIntoTree(t, t->root, inserted);
}


Tree forcedSortTree(Tree *t, int(*comparator)(PhoneBookRecord, PhoneBookRecord)) {
    Tree res;
    res.comparator = comparator;
    res.root = NULL;
    TreeNode *tn;
    tn = minNodeInSubtree(t->root);
    // easy, but not the fastest implementation .

    while (tn != NULL) {
        insertPBRIntoTree(&res, tn->value);
        tn = successor(tn);
    }
    deleteTree(t);

    return res;
//    insertNodeIntoTree(&res,res.root,tn);
}

Tree sortTree(Tree *t, int(*comparator)(PhoneBookRecord, PhoneBookRecord)) {
    if (t->comparator != comparator) {
        return forcedSortTree(t, comparator);
    } else {
        return *t;
    }
}

void deleteSubTree(TreeNode *tn) {
    if (tn == NULL)
        return;

    deleteSubTree(tn->left);
    deleteSubTree(tn->right);
    deletePhoneBookRecord(tn->value);
    free(tn);

}

void deleteTree(Tree *t) {
    deleteSubTree(t->root);
    t->comparator = NULL;
    t->root = NULL;
}

void deleteTreeNode(Tree *t, TreeNode *tn) {

    TreeNode *p, *childToPreserve;
    p = tn->parent;

    if (tn->left == NULL && tn->right == NULL) {// O children

        if (p == NULL)//root case
            t->root = NULL;
        else {
            if (p->right == tn)
                p->right = NULL;
            else
                p->left = NULL;

        }
        deletePhoneBookRecord(tn->value);
        free(tn);
    } else if ((tn->left == NULL) != (tn->right == NULL)) {// One child

        if (tn->left == NULL)
            childToPreserve = tn->right;
        else
            childToPreserve = tn->left;


        if (p == NULL) {//root case
            childToPreserve->parent = NULL;
            t->root = childToPreserve;
        } else {
            if (p->right == tn)
                bindRightTreeNode(p, childToPreserve);
            else
                bindLeftTreeNode(p, childToPreserve);
        }
        deletePhoneBookRecord(tn->value);
        free(tn);
    } else {//Both children
        /// safe for root
        //Find min of right subtree
        TreeNode *minR = minNodeInSubtree(tn->right);
        PhoneBookRecord tmp;
        tmp=tn->value;
        tn->value = minR->value;
        minR->value=tmp;
        deleteTreeNode(t, minR);

    }


}

TreeNode *findFirstInSubTree(Tree t, TreeNode *tn, PhoneBookRecord pbr) {
    if (tn==NULL){
        return tn;
    }
    int comp = t.comparator(tn->value, pbr);
    if (comp == 0) {//THE SAME AND :
        if (tn->left == NULL) // NO LEFT
            return tn;

        if (t.comparator(tn->left->value, tn->value) == 0)  // LEFT IS EQUAL
            return findFirstInSubTree(t, tn->left, pbr);
        else    //LEFT is smaller
            return tn;
    } else if (comp > 0)  // inserted < currentnode
        return findFirstInSubTree(t, tn->left, pbr);
      else
        return findFirstInSubTree(t, tn->right, pbr);
    //return NULL;
}

void printTree(Tree t) {
    TreeNode *r = minNodeInSubtree(t.root);
    while (r != NULL) {
        printPhonebookRecord(r->value);
        r = successor(r);
    }

}


